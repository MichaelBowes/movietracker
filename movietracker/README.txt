Change of profiles:
To change the profile to production (use of an external database) change the following values:

In "src.main.java.movietracker.configuration.ApplicationInitializer" change the line:
"container.setInitParameter("spring.profiles.active", "development");"
to: "container.setInitParameter("spring.profiles.active", "production");".

In "src.main.java.movietracker.configuration.DatabaseConfiguration" change the line:
"@PropertySource(value = { "classpath:database-dev.properties" })" to:
"@PropertySource(value = { "classpath:database-production.properties" })"

For database url, username and password access the database settings under the location:
"src.main.resources.database-production.properties"
