package movietracker.rest;

/**
 * @author Maximilian Kostial
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import movietracker.model.Movie;
import movietracker.services.MovieService;

@RestController
@RequestMapping(value="/rest/movie")
public class MovieRestController {
	
	@Autowired
	MovieService movieService;
	
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public Movie getMovieById(@PathVariable long id){
		return movieService.findMovieById(id);
	}

}
