package movietracker.rest;
/**
 * @author Maximilian Kostial
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import movietracker.model.Movie;
import movietracker.services.ActorService;

@RestController
@RequestMapping(value="/rest/actor")
public class ActorRestController {
	
	@Autowired
	ActorService actorService;
	
	@RequestMapping(value="/movies", method = RequestMethod.GET)
	public List<Movie> getAllMoviesByActor(@RequestParam String firstName, 
			                   @RequestParam String lastName){
		for (Movie movie : actorService.findActorByName(firstName, lastName).getMovies())
			System.out.println(" Title : " + movie.getTitle());
		return actorService.findActorByName(firstName, lastName).getMovies();
	}

}
