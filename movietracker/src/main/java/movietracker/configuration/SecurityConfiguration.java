package movietracker.configuration;
/**
 * @author Maximilian Kostial
 */
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.core.userdetails.UserDetailsService;




@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

	@Autowired
	DataSource dataSource;
	
    @Autowired 
    UserDetailsService userDetailsService;

	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	    http.authorizeRequests()
	           .antMatchers("/","/home/**", "/login", "/resources/**",
	        		        "/categories", "/categories/**", "/tracker/**", 
	        		        "/movie/**", "/registration/**", "/rest/**")
	           .permitAll()
	           .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
	           .anyRequest()
	           .authenticated()
	    .and()
           .formLogin()
           .loginPage("/home")
           .loginProcessingUrl("/home")
        .and()
           .logout()
           .logoutSuccessUrl("/")
           .logoutUrl("/signout");
	    
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}

}
