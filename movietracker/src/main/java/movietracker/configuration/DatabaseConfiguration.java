package movietracker.configuration;

/**
 * @author Maximilian Kostial
 */

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@ComponentScan( basePackages = {"movietracker.database"})
@PropertySource(value = { "classpath:database-dev.properties" })
public class DatabaseConfiguration {

	 @Autowired
	 private Environment environment;
	 @Autowired
	 private DataSource dataSource;
	 
	  
	/**
	 * This method returns an H2 embedded datasource when the profile development is activated.
	 * 
	 * The Profile can be activated in the onStartup method via 
	 *<blockquote> {@code container.setInitParameter("spring.profiles.active", "development");}</blockquote> or as
	 * JVM System Parameter <blockquote>{@code -Dspring.profiles.active=development}</blockquote>
	 * 
	 * @return the embedded datasource
	 */
	  
	@Bean(name= "dataSource")
	@Profile("development")
	public DataSource getEmbeddedDataSource(){		
		
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		EmbeddedDatabase dataSource = builder
			.setType(EmbeddedDatabaseType.H2) 
			.addScript("movietracker.sql")
			.addScript("data-for-db.sql")
			.build();
		return dataSource;
		
	}
	
	/**
	 * This method returns an MySQL datasource when the profile production is activated.
	 * 
	 * The Profile can be activated in the onStartup method in the {@code ApplicationConfiguration.class} via 
	 * <blockquote>{@code container.setInitParameter("spring.profiles.active", "production");}</blockquote> or as
	 * JVM System Parameter <blockquote>{@code -Dspring.profiles.active=production}</blockquote>
	 *
	 * @return the embedded datasource
	 */
	@Bean(name= "dataSource")
	@Profile("production")
	public DataSource getMysqlDataSource(){
	       DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
	        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
	        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
	        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
	        return dataSource;
	}
	
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(this.dataSource);
        sessionFactory.setPackagesToScan(new String[] { "movietracker.model" });
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
     }
    
    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
       HibernateTransactionManager txManager = new HibernateTransactionManager();
       txManager.setSessionFactory(sessionFactory); 
       return txManager;
    }
  
    
    
    @SuppressWarnings("serial")
	private Properties hibernateProperties(){
    	return new Properties(){
    		{   		
    		put("hibernate.dialect", environment.getProperty("hibernate.dialect"));
    		put("hibernate.show_sql", environment.getProperty("hibernate.show_sql"));
    		put("hibernate.format_sql", environment.getProperty("hibernate.format_sql"));
    		}
    	};
    }
}
