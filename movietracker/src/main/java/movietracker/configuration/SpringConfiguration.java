package movietracker.configuration;
/**
 * @author Maximilian Kostial
 */

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.ServletWebArgumentResolverAdapter;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"movietracker.controller","movietracker.rest"})
public class SpringConfiguration extends WebMvcConfigurerAdapter implements ApplicationContextAware{
 
	private ApplicationContext applicationContext;
	
		@Override
		public void addResourceHandlers(ResourceHandlerRegistry registry) {
			registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		}
		
		
		@Bean
		public SpringResourceTemplateResolver templateResolver() {
			
			SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
			templateResolver.setApplicationContext(this.applicationContext);
		    templateResolver.setPrefix("/WEB-INF/fragments/");
		    templateResolver.setSuffix(".html");
		    templateResolver.setTemplateMode("HTML5");
		 
		    return templateResolver;
		}
		 
		@Bean
		public SpringTemplateEngine templateEngine() {
		    SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		    templateEngine.setTemplateResolver(templateResolver());
		    templateEngine.addDialect(new SpringSecurityDialect());
		 
		    return templateEngine;
		}
		@Bean
		public ThymeleafViewResolver viewResolver() {
		    ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		    viewResolver.setTemplateEngine(templateEngine());
		    return viewResolver;
		}
	    
	    @Override
	    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
	    	configurer.enable();
	    }
	    
	    // This resolver is for the Pagination
	    @Override
	    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers){  	
	    	PageableArgumentResolver resolver = new PageableArgumentResolver();
	    	resolver.setFallbackPagable(new PageRequest(1,10));    	
	    	argumentResolvers.add(new ServletWebArgumentResolverAdapter(resolver));
	    }

		@Override
		public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
			this.applicationContext = applicationContext;
			
		}
	

}
