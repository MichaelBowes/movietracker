package movietracker.util;

import java.util.List;
import java.util.regex.Pattern;

import movietracker.model.Movie;

public class SearchUtil {

	/*
	 * takes a String and an movie object and returns 
	 * true of the string is contained in the title of the movie obj.
	 */
	public boolean containsActor(String name,Movie movie){
		long count = movie.getActors().stream()
						.filter(a -> Pattern.compile(Pattern.quote(name),
													Pattern.CASE_INSENSITIVE).matcher(a.getFirstName()).find()
													||Pattern.compile(Pattern.quote(name),
													Pattern.CASE_INSENSITIVE).matcher(a.getLastName()).find())
						.count();
		if(count > 0){
			return true;
		}	
		return false;
	}
	
	public boolean containsCategory(List<String> categories, Movie movie){
		if(categories == null){
			return true;
		}else{
				long count = movie.getCategories().stream()
						.filter(c -> categories.contains(c.getCategoryName())).count();
			
			if(count == categories.size()){
				return true;
			}else{
				return false;
			}
		}
	}
}
