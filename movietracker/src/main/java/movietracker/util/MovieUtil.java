package movietracker.util;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import movietracker.model.MovieUser;
import movietracker.services.MovieUserService;

/**
 * This Class contains methods for changing values in MovieUser or other
 * objects associated with Movies.
 * @author Bloodypizza
 *
 */
@Component
public class MovieUtil {
	

	@Autowired
	private MovieUserService movieUserService;
	

	/**
	 * This method searches for the MovieUser with the given movieID and userID.<br>
	 * If no matching MovieUser is found it will create a new MovieUser and return it.
	 * @param userID
	 * @param movieID
	 * @return MovieUser with the corresponding movie and user IDs.
	 */
	public MovieUser getMovieUser(Long userID,Long movieID){
		
		System.out.println("User: " + userID + " Movie: " + movieID);
		MovieUser movieUser = movieUserService.findMovieUserById(userID, movieID);

		if(movieUser == null){
			MovieUser movieuser = MovieUser.newBuilder()
					.withMovieID(movieID)
					.withUserID(userID)
					.withSeen(false)
					.withWatchlist(false)
					.build();
			
			movieUserService.create(movieuser);
			movieUserService.updateMovieUser(movieuser);
			return movieuser;
		}else{
			return movieUser;
		}	
	}
	
	/**
	 * Changes the isSeen parameter in the respective MovieUser to true if it was false, and to false if it was true.
	 * 
	 * @param userID
	 * @param MovieID
	 */
	public void triggerSeenMovie(Long userID,Long MovieID){
		MovieUser movieUser = getMovieUser(MovieID,userID);
		
		if(movieUser.isSeen()){
			movieUser.setSeen(false);
			movieUserService.updateMovieUser(movieUser);
		}else if(!movieUser.isSeen()){
			movieUser.setSeen(true);
			movieUser.setDateSeen(Calendar.getInstance().getTime());//change to calendar?
			movieUserService.updateMovieUser(movieUser);
		}
	}
	/**
	 * Changes the isSeen parameter in the respective MovieUser to true if it was false, and to false if it was true.
	 * @param movieUser
	 */
	public void triggerSeenMovie(MovieUser movieUser){
		if(movieUser.isSeen()){
			movieUser.setSeen(false);
			movieUserService.updateMovieUser(movieUser);
		}else if(!movieUser.isSeen()){
			movieUser.setSeen(true);
			movieUser.setDateSeen(Calendar.getInstance().getTime());//change to calendar?
			movieUserService.updateMovieUser(movieUser);
		}
	}
	
	/**
	 * Sets the watchlist variable to false and returns true.<br>
	 * If the variable was already false it will return false.
	 * @param userID
	 * @param MovieID
	 * @return True if the removal was successful, and false if not.
	 */
	public boolean removeFromWatchlist(Long userID,Long MovieID){
		MovieUser movieUser = getMovieUser(MovieID,userID);
		if(movieUser.isWatchlist()){
			movieUser.setWatchlist(false);
			movieUser.setDateSeen(null);
			movieUserService.updateMovieUser(movieUser);
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Sets the watchlist variable to false and returns true.<br>
	 * If the variable was already false it will return false.
	 * @param movieUser
	 * @return True if the removal was successful, and false if not.
	 */
	public boolean removeFromWatchlist(MovieUser movieUser){
		if(movieUser.isWatchlist()){
			movieUser.setWatchlist(false);
			movieUser.setDateSeen(null);
			movieUserService.updateMovieUser(movieUser);
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Sets the watchlist variable to true and returns true.<br>
	 * If the variable was already true it will return false. 
	 * 
	 * @param userID
	 * @param MovieID
	 * @return True if the addition was successful, and false if not.
	 */
	public boolean addToWatchList(Long userID,Long MovieID){
		MovieUser movieUser = getMovieUser(MovieID,userID);
		if(!movieUser.isWatchlist()){
			movieUser.setWatchlist(true);
			movieUser.setDateWatchlist(Calendar.getInstance().getTime());
			movieUserService.updateMovieUser(movieUser);
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Sets the watchlist variable to true and returns true.<br>
	 * If the variable was already true it will return false. 
	 * 
	 * @param movieUser
	 * @return True if the addition was successful, and false if not.
	 */
	public boolean addToWatchList(MovieUser movieUser){
		if(!movieUser.isWatchlist()){
			movieUser.setWatchlist(true);
			movieUser.setDateWatchlist(Calendar.getInstance().getTime());
			movieUserService.updateMovieUser(movieUser);
			return true;
		}else{
			return false;
		}
	}
	
}
