package movietracker.controller;
/**
 * @author Maximilian Kostial
 */


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import movietracker.model.Movie;
import movietracker.model.MovieUser;
import movietracker.model.User;
import movietracker.model.WatchList;
import movietracker.services.MovieUserService;
import movietracker.services.UserService;

@Controller
@RequestMapping(value="/registration")
public class RegistrationController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private MovieUserService movieUserService;
	@Autowired
	private WatchList watchList;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String getRegistrationView(@ModelAttribute User user, Model model){
		return "register";
	}
	@RequestMapping(method = RequestMethod.POST)
	public String registerUser(@ModelAttribute User user, final RedirectAttributes redirectAttributes){
		   User userFromDatabase = userService.findUserByUserName(user.getUserName());
           boolean userNotInDatabase = userFromDatabase.getUserName()== null ? true : false;

            if (userNotInDatabase) {
	        user.setAdmin(false);
			userService.addUser(user);		
			// Add the Movies in the Watchlist to the Users Watchlist
			for (Movie movie : watchList.getMovies()){
				MovieUser movieUser = MovieUser.newBuilder()
						.withUserID(user.getUserID())
						.withMovieID(movie.getMovieID())
						.withWatchlist(true)
						.build();
				movieUserService.updateMovieUser(movieUser);
			}
			return "redirect:/home";	       
            }
            redirectAttributes.addFlashAttribute("user", user);
		return "redirect:/registration?error";
	}

}
