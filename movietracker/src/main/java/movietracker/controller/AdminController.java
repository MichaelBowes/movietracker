package movietracker.controller;

import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import movietracker.model.Actor;
import movietracker.model.Category;
import movietracker.model.Movie;
import movietracker.model.MovieDetails;
import movietracker.model.Status;
import movietracker.model.User;
import movietracker.model.Director;
import movietracker.services.ActorService;
import movietracker.services.CategoryService;
import movietracker.services.DirectorService;
import movietracker.services.MovieDetailsService;
import movietracker.services.MovieService;

/**
 * @author Michael Bowes
 */

@Controller
@RequestMapping()
public class AdminController {

	@Autowired
	private User user;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private MovieDetailsService movieDetailsService;
	@Autowired
	private MovieService movieService;
	@Autowired
	private ActorService actorService;
	@Autowired
	private DirectorService directorService;
	

	@ModelAttribute("movieDetailsList")
	public List<MovieDetails> moviesDetailsList() {
		return movieDetailsService.findAllMoviesDetails();
	}
	
	@RequestMapping(value = "/admin")
	public String getAdminView(Model model) {
		List<Movie> tempMovies = movieService.findAllMovies();
		List<Movie> movieList = new ArrayList<>();

		for (Movie m : tempMovies) {
			if (m.getStatus() == Status.APPROVED) {
				movieList.add(m);
			}
		}

		model.addAttribute("mode", "normal");
		model.addAttribute("overview", 2);
		model.addAttribute("user", this.user);
		model.addAttribute("director", new Director());
		model.addAttribute("actor", new Actor());
		model.addAttribute("category", new Category());
		model.addAttribute("movieList", movieList);
		model.addAttribute("actorList", actorService.findAllActors());
		model.addAttribute("directorList", directorService.findAllDirectors());

		return "admin";
	}
	
	// MovieDetails Added
	@RequestMapping(value = "/admin/moviedetails/add/{id}", method = RequestMethod.POST)
	public String addMovieDetails(@PathVariable("id") Long id) {
       MovieDetails movieDetails = movieDetailsService.findMovieDetailsById(id);
       Movie movie = new Movie();
       movie.setTitle(movieDetails.getTitle());
       movie.setAgeLimitation(movieDetails.getAgeLimitation());
       movie.setDescription(movieDetails.getDescription());
       movie.setDuration(movieDetails.getDuration());
       movie.setReleaseDate(movieDetails.getReleaseDate());
       movie.setCreatedBy(movieDetails.getCreatedBy());
       movie.setStatus(movieDetails.getStatus());
       movie.setDirectorID(movieDetails.getDirectorID());
       
       if (movieDetails.getMovieID() > 0) {
           movie.setMovieID(movieDetails.getMovieID());
           movieService.updateMovie(movie);
       } else {
    	   movieService.addMovie(movie);         
       }
       movieDetailsService.deleteMovieDetails(id);
		return "redirect:/admin/";
	}
	
	// MovieDetails Delete
	@RequestMapping(value = "/admin/moviedetails/delete/{id}", method = RequestMethod.POST)
	public String deleteMovieDetails(@PathVariable("id") Long id) {
               movieDetailsService.deleteMovieDetails(id);    
		return "redirect:/admin/";
	}

	// Actors added by the administrator.
	@RequestMapping(value = "/admin/movies", method = RequestMethod.POST)
	public String checkMovies(Model model, @RequestParam Object click) {
		model.addAttribute("overview", 1);
		System.out.println("Object");
		Actor actor = new Actor();
		actorService.addActor(actor);
		actorService.updateActor(actor);

		return "redirect:/admin/";
	}

	// Actors added by the administrator.
	@RequestMapping(value = "/admin", method = RequestMethod.POST)
	public String checkCategories(Model model, @RequestParam String categoryAccept) {
		model.addAttribute("overview", 2);
		System.out.println(categoryAccept);
		Actor actor = new Actor();
		actorService.addActor(actor);
		actorService.updateActor(actor);

		return "redirect:/admin/";
	}

	// Actors added by the administrator.
	@RequestMapping(value = "/admin/actors", method = RequestMethod.POST)
	public String addActors(@ModelAttribute Actor actor, Model model) {
		actorService.addActor(actor);

		return "redirect:/admin/";
	}

	// Directors added by the administrator.
	@RequestMapping(value = "/admin/directors", method = RequestMethod.POST)
	public String addDirectors(@ModelAttribute Director director, Model model) {
		directorService.addDirector(director);

		return "redirect:/admin/";
	}

	@RequestMapping(value = "/admin/director/update/{id}")
	public String startEditingDirector(@PathVariable(value = "id") Long id, Model model) {
		model.addAttribute("mode", "editingDirector");
		model.addAttribute("user", this.user);
		model.addAttribute("director", directorService.findDirectorById(id));
		return "admin";
	}

	@RequestMapping(value = "/admin/director/delete/{id}", method = RequestMethod.POST)
	public String deleteDirector(@PathVariable(value = "id") Long id, Model model) {
		directorService.deleteDirectorById(id);

		return "redirect:/admin/";
	}

	@RequestMapping(value = "/admin/director/update", method = RequestMethod.POST)
	public String updateDirector(@ModelAttribute Director director, Model model) {
		directorService.updateDirector(director);

		return "redirect:/admin/";
	}

	@RequestMapping(value = "/admin/actor/update/{id}")
	public String startEditingActor(@PathVariable(value = "id") Long id, Model model) {
		model.addAttribute("mode", "editingActor");
		model.addAttribute("user", this.user);
		model.addAttribute("actor", actorService.findActorById(id));
		return "admin";
	}

	@RequestMapping(value = "/admin/actor/delete/{id}", method = RequestMethod.POST)
	public String deleteActor(@PathVariable(value = "id") Long id, Model model) {
		actorService.deleteActorById(id);

		return "redirect:/admin/";
	}

	@RequestMapping(value = "/admin/actor/update", method = RequestMethod.POST)
	public String updateActor(@ModelAttribute Actor actor, Model model) {
		Actor actorInDB = actorService.findActorById(actor.getActorID());
		actor.setMovies(actorInDB.getMovies());
		actorService.updateActor(actor);

		return "redirect:/admin/";
	}

	@RequestMapping(value = "/admin/updateRanks")
	public String updateRanks() {
		movieService.updateRanks();
		return "redirect:/admin/";
	}

	// Category Not Approved list
	@ModelAttribute("categoryNotApprovedList")
	public List<Category> getCatNotAppList() {
		List<Category> result = new ArrayList<>();
		for (Category cat : categoryService.findAllCategories()) {
			if (!cat.isApproved())
				result.add(cat);
		}
		return result;
	}

	// Category Approved list
	@ModelAttribute("categoryApprovedList")
	public List<Category> getCatAppList() {
		List<Category> result = new ArrayList<>();
		for (Category cat : categoryService.findAllCategories()) {
			if (cat.isApproved())
				result.add(cat);
		}
		return result;
	}

	@RequestMapping(value = "/admin/category/update/{id}")
	public String startEditingCategory(@PathVariable(value = "id") Long id, Model model) {
		model.addAttribute("mode", "editingCategory");
		model.addAttribute("user", this.user);
		model.addAttribute("category", categoryService.findCategoryById(id));
		return "admin";
	}

	@RequestMapping(value = "/admin/category/delete/{id}", method = RequestMethod.POST)
	public String deleteCategory(@PathVariable(value = "id") Long id, Model model) {
		categoryService.deleteCategory(id);
		return "redirect:/admin/";
	}

	@RequestMapping(value = "/admin/category/update", method = RequestMethod.POST)
	public String updateCategory(@ModelAttribute Category category, Model model) {
		// Category actorInDB =
		// categoryService.findCategoryById(category.getId());
		// actor.setMovies(actorInDB.getMovies());
		Category categoryInDB = categoryService.findCategoryById(category.getId());
		category.setMovies(categoryInDB.getMovies());
		category.setApproved(true);
		categoryService.updateCategory(category);
		return "redirect:/admin/";
	}

	@RequestMapping(value = "/admin/category/approve/{id}", method = RequestMethod.POST)
	public String approveCategory(@PathVariable(value = "id") Long id, Model model) {
		Category category = categoryService.findCategoryById(id);
		category.setApproved(true);
		categoryService.updateCategory(category);
		return "redirect:/admin/";
	}

	// Category list
	@ModelAttribute("moviesSeen")
	public List<Movie> moviesSeen() {
		return movieService.findAllMoviesSeenByUser(this.user, true);
	}

}
