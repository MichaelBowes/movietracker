package movietracker.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import movietracker.model.Movie;
import movietracker.model.User;
import movietracker.model.Category;
import movietracker.model.Comment;
import movietracker.model.Vote;
import movietracker.services.CommentService;
import movietracker.services.MovieService;
import movietracker.services.UserService;
import movietracker.services.VoteService;

/**
 * @author Michael Bowes
 */

@Controller
@RequestMapping(value="/account")
public class AccountController {

	@Autowired
	private UserService userService;
	@Autowired
	private VoteService voteService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private MovieService movieService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public String getAccountView(@PathVariable Long id, Model model){
		long userID = userService.getUserForSession().getUserID();
		if (id != userID)
			return "redirect:/account/" + userID;
		User user = userService.findUserById(id);
		
		List<Movie> commentedMovies = new ArrayList<>();
		for (Comment c : commentService.findAllComments()){
			if (id == c.getMovieID()){
				commentedMovies.add(movieService.findMovieById(c.getMovieID()));
			}
		}
		
		List<Vote> voteList = voteService.selectAllVotesOfAUser(user);
		
		List<Comment> commentList = new ArrayList<>();
		for (Comment c : commentService.findAllComments()){
			if (id == c.getUserID()){
					commentList.add(c);
				}
		}
				
		List<Movie> recommendations = new ArrayList<>();
		List<Movie> allMovies = movieService.findAllMovies();
		sortByRank(allMovies);
		
		List<Movie> moviesUserVotedOn = new ArrayList<>();
		List<Movie> moviesVotedList = new ArrayList<>();
		List<Movie> moviesNotSeen = movieService.findAllMoviesSeenByUser(user, false);
		for (Vote v : voteList){
			for (Movie m : movieService.findAllMovies()){
				if (m.getMovieID() == v.getMovieID()){
					if(!containsMovie(moviesUserVotedOn, m)) moviesUserVotedOn.add(m);
					if(!containsMovie(moviesVotedList, m)) moviesVotedList.add(m);
					}
			}
		}
		model.addAttribute("voteMovies", moviesVotedList);
		model.addAttribute("voteList", voteList);
		sortByRank(moviesNotSeen);
		sortByUsersVote(moviesUserVotedOn, voteList);
		List<Category> topVotedCategories = new ArrayList<Category>();
		for (Movie m : moviesUserVotedOn){
			for (Category c : m.getCategories()){
				if (!containsCategory(topVotedCategories, c)) topVotedCategories.add(c);
			}
		}
		for (Category c : topVotedCategories){
			for (Movie m : moviesNotSeen){				
				if (recommendations.size() == 5) break;
				if (containsCategory(m.getCategories(), c) && !containsMovie(recommendations, m)) recommendations.add(m);
			}
		}
		
		int i = 0;
		while (recommendations.size() < 5 || i > 10) {
			if (moviesNotSeen.size() > i) 
				if (!containsMovie(recommendations, moviesNotSeen.get(i))) recommendations.add(moviesNotSeen.get(i));
			i++;
		}
		
		model.addAttribute("commentMovies", commentedMovies);
		model.addAttribute("user", user);
		model.addAttribute("movieList", recommendations);
		model.addAttribute("commentList", commentList);
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(user, true));
		
		return "account";
	}
	
	@RequestMapping(value="/{id}/name", method = RequestMethod.POST)
	public String ChangeName(@PathVariable Long id, Model model, @RequestParam String name){
		
		User user = userService.findUserById(id);
		user.setUserName(name);
		
		userService.updateUser(user);
		
		return "redirect:/account/" + id;
	}
	
	@RequestMapping(value="/{id}/pass", method = RequestMethod.POST)
	public String ChangePass(@PathVariable Long id, Model model, @RequestParam String pass){
		
		User user = userService.findUserById(id);
		user.setPassword(pass);
		
		userService.updateUser(user);
		
		return "redirect:/account/" + id;
	}
	
	@RequestMapping(value="/{id}/email", method = RequestMethod.POST)
	public String ChangeEmail(@PathVariable Long id, Model model, @RequestParam String email){
		
		User user = userService.findUserById(id);
		
		if( (email.contains(".")) && (email.contains("@"))){
			user.setUserEmail(email);
		}
		else {
			String warning = "not a valid email address";
			model.addAttribute("warning", warning);
			}
		
		userService.updateUser(user);
		
		return "redirect:/account/" + id;
	}
	
	@ModelAttribute("movie")
	public List<Movie> getMovies(){
		
		List<Movie> movielist = movieService.findAllMovies();
		return movielist;
	}
	
	private static List<Movie> sortByRank(List<Movie> movies){
		Collections.sort(movies, new Comparator<Movie>(){
		     public int compare(Movie m1, Movie m2){
		         if(m1.getRank() == m2.getRank()) return 0;
		         return m1.getRank() > m2.getRank() ? -1 : 1;
		     }
		});
		return movies;
	}
	
	private static List<Movie> sortByUsersVote(List<Movie> movies, List<Vote> votes){
		Collections.sort(movies, new Comparator<Movie>(){
			public int compare(Movie m1, Movie m2){
				Vote v1 = voteFromList(votes, m1);
				Vote v2 = voteFromList(votes, m2);
				if (v1 == null && v2 == null) return 0;
				if (v1 != null && v2 == null) return -1;
				if (v1 == null && v2 != null) return 1;
				if (v1.getRank() == v2.getRank()) return 0;
				return v1.getRank() > v2.getRank() ? -1 : 1;
			}
		});
		return movies;
	}
	
	private static Vote voteFromList(List<Vote> votes, Movie movie) {
	    for (Vote v : votes) {
	        if (v.getMovieID() == movie.getMovieID()) {
	            return v;
	        }
	    }
	    return null;
	}
	
	private static boolean containsCategory(List<Category> list, Category cat) {
		for (Category c : list) {
			if (c.getId() == cat.getId()) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean containsMovie(List<Movie> list, Movie movie) {
	    for (Movie m : list) {
	        if (m.getMovieID() == movie.getMovieID()) {
	            return true;
	        }
	    }
	    return false;
	}
}
