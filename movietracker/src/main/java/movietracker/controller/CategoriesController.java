package movietracker.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import movietracker.model.Category;
import movietracker.model.User;
import movietracker.services.CategoryService;
import movietracker.services.MovieService;

/**
 * @author Michael Bowes
 * @author Stanislav Lokhtin
 */

@Controller
@RequestMapping()
public class CategoriesController {
	
	/*private MovieService ms = new MovieService(new NamedParameterJdbcTemplate(new EmbeddedDatabaseBuilder()
    		.setType(EmbeddedDatabaseType.H2).addScripts("movietracker.sql", "data-for-db.sql").build()));*/

	@Autowired
	private User user;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private MovieService movieService;
	
	@RequestMapping(value = "/categories")
	public String getCategoriesView(Model model) {
		model.addAttribute("overviewOn", true);
		model.addAttribute("user", this.user);
		model.addAttribute("movieList", movieService.findAllMovies());
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(this.user, true));
		return "categories";
	}
	
	@RequestMapping(value="/categories/{id}", method = RequestMethod.GET)
	public String getCategoryView(@PathVariable(value="id") Long id, Model model){
		Category cat = categoryService.findCategoryById(id);
		model.addAttribute("category", cat);
		model.addAttribute("overviewOn", false);
		model.addAttribute("user", this.user);
		model.addAttribute("movieList", cat.getMovies());
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(this.user, true));
		return "categories";
	}
	
	// Categories added by users that have not yet been accepted.
	@RequestMapping(value = "/categories", method = RequestMethod.POST)
	public String unconfirmedCategories(Model model, @RequestParam String name) {
		if(name.length() > 0){
			Category userCategory = new Category(name, false); //this.user.isAdmin());
			categoryService.addCategory(userCategory);
		}
		
		return "redirect:/categories/";
	}
	
	// Category list
	@ModelAttribute("categoryList")
	public List<Category> getCatList(){
		List<Category> result = new ArrayList<>();
		for (Category cat : categoryService.findAllCategories()) {
			if (cat.isApproved()) result.add(cat);
		}
		return result;
	}
	
}