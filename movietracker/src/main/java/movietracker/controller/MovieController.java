package movietracker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import movietracker.model.Actor;
import movietracker.model.Category;
import movietracker.model.Comment;
import movietracker.model.Movie;
import movietracker.model.MovieUser;
import movietracker.model.User;
import movietracker.model.Vote;
import movietracker.services.CommentService;
import movietracker.services.DirectorService;
import movietracker.services.MovieService;
import movietracker.services.MovieUserService;
import movietracker.services.UserService;
import movietracker.services.VoteService;


/**
 * 
 * @author Nicolas Bruch
 * Controller for the movie pages
 */

@Controller
@RequestMapping(value="/movie")
public class MovieController {

	@Autowired
	private MovieService movieService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MovieUserService movieUserService;
	
	@Autowired
	private DirectorService directorService;
	
	@Autowired
	private User user;
	
	@Autowired
	private VoteService voteService;

	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public String moviePage(@PathVariable long id, Model model, @ModelAttribute Comment comment){
		
		Movie movie = movieService.findMovieById(id);
		
		if(user.getUserID()>0){
			MovieUser movieUser = getMovieUser(user.getUserID(),id);
			model.addAttribute("movieUser",movieUser);
		}
		
			
		Set<Actor> actors = new HashSet<>();
		if (movie.getActors().size() > 0) actors.addAll(movie.getActors());
		
		Set<Category> categories = new HashSet<>();
		if (movie.getCategories().size() > 0) categories.addAll(movie.getCategories());
		
		List<Comment> comments = movie.getComments();
		
		List<List<String>> commentlist = new ArrayList<>();
		
		//sorting the comments
		comments.stream().sorted(new Comparator<Comment>(){
			public int compare(Comment comment1,Comment comment2){
				if(comment1.getId()>comment2.getId()){
					return 1;
				}else if(comment1.getId()<comment2.getId()){
					return -1;
				}else{
					return 0;
				}
			}
		});
		
		/*
		 * Comments are added to an multidimensional arraylist.
		 * Has to be done because comments don't contain the username
		 * and looking through the table for every user would be more expensive
		 * on the performance.
		 * 
		 * Index:
		 * 0: username
		 * 1: content
		 * 2: date
		 * 3: userID
		 * 4: commentID
		 */
        comments.stream().forEach(c -> {
				List<String> data = new ArrayList<>();
				data.add(userService.findUserById(c.getUserID()).getUsername());
				data.add(c.getText());
				data.add(c.getCreatedAt().getTime().toString());
				data.add(Long.toString(c.getUserID()));
				data.add(Long.toString(c.getId()));	
				if(!commentlist.contains(data)){
					commentlist.add(data);
				}			
        });
        
    	
		model.addAttribute("user", user);
		model.addAttribute("commentlist",commentlist);
		model.addAttribute("director",directorService.findDirectorById(movie.getDirectorID()));
		model.addAttribute("actorlist",actors);
		model.addAttribute("moviecategories",categories);
		model.addAttribute("movie",movie);
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(this.user, true));
		return "movie";
	}
	
	/**
	 *  Changes the movieUservalue to true if it was false and to false if it was true.
	 */
	@RequestMapping(value="/{id}/mark", method = RequestMethod.POST)
	public String markAsWatched(@PathVariable Long id, Model model){
		
		MovieUser movieUser = getMovieUser(user.getUserID(),id);
		
		if(movieUser.isSeen()){
			movieUser.setSeen(false);
			movieUserService.updateMovieUser(movieUser);
		}else if(!movieUser.isSeen()){
			movieUser.setSeen(true);
			movieUser.setDateSeen(Calendar.getInstance().getTime());//change to calendar?
			movieUserService.updateMovieUser(movieUser);
		}		
		
		return "redirect:/movie/" + id;
	}
	
	/**
	 *  Adds a Vote to the movie or updates the existing vote.
	 */
	@RequestMapping(value="/{id}/vote", method= RequestMethod.POST)
	public String voteForMovie(@PathVariable Long id, @ModelAttribute Vote vote, Model model){

		List<Vote> votes = voteService.findAllVotes()
				.stream()
				.filter(v -> v.getMovieID() == id)
				.filter(v -> v.getUserID() == user.getUserID())
				.collect(Collectors.toList());
		if(votes.size() == 0){
			voteService.addVote(new Vote(vote.getRank(),user.getUserID(),id));
			movieService.findMovieById(id).setNumberOfVotes(movieService.findMovieById(id).getNumberOfVotes() + 1);
		}else if(votes.size()==1){
			votes.get(0).setRank(vote.getRank());
			voteService.updateVote(votes.get(0));	
		}else{
			votes.parallelStream().forEach(v -> voteService.deleteVote(v.getId()));
			voteForMovie(id,vote,model);
		}
		movieService.updateRanks();
		
		movieService.updateMovie(movieService.findMovieById(id));
		
		return "redirect:/movie/" + id;
	}
	
	/**
	 * Deletes a comment from an existing movie.
	 * 
	 */
	@RequestMapping(value="/{id}/delete", method = RequestMethod.POST)
	public String deleteComment(@PathVariable Long id, @RequestParam("commentID") Long commentID, Model model){
		Movie movie = movieService.findMovieById(id);
		movie.getComments().remove(movie.getComments().stream().filter(c -> c.getId()==commentID).findFirst().get());
		commentService.deleteComment(commentID);
		movieService.updateMovie(movie);
		return "redirect:/movie/" + id;
	}
	
	/**
	 * Deletes a movie via its ID.
	 */
	@RequestMapping(value="/{id}/deleteMovie")
	public String deleteMovie(@PathVariable Long id, Model model){
		movieService.deleteMovie(id);
		return "redirect:/home";
	}
	
	/**
	 * 
	 */
	@RequestMapping(value="/{id}/submit", method = RequestMethod.POST)
	public String submitComment(@PathVariable Long id, @ModelAttribute Comment comment, Model model){
		if(comment.getText() != null && comment.getText().length() >= 10 && comment.getText().length() <= 1000){			
			Comment userComment = new Comment(user.getUserID(), id, comment.getText(), Calendar.getInstance());
			Movie movie = movieService.findMovieById(id);
		
			commentService.addComment(userComment);
			movie.getComments().add(userComment);		
			movieService.updateMovie(movieService.findMovieById(id));
			movieService.updateMovie(movie);
		}	
		return "redirect:/movie/" + id;
	}
	
	
	/**
	 * This method searches for the MovieUser with the given movieID and userID.<br>
	 * If no matching MovieUser is found it will create a new MovieUser and return it.
	 * @param userID
	 * @param movieID
	 * @return MovieUser with the corresponding movie and user IDs.
	 */
	private MovieUser getMovieUser(Long userID,Long movieID){
	
		MovieUser movieUser = movieUserService.findMovieUserById(userID, movieID);

		if(movieUser == null){
			MovieUser movieuser = MovieUser.newBuilder()
					.withMovieID(movieID)
					.withUserID(userID)
					.withSeen(false)
					.withWatchlist(false)
					.build();
			
			movieUserService.create(movieuser);
			movieUserService.updateMovieUser(movieuser);
			return movieuser;
		}else{
			return movieUser;
		}	
	}
	
}
