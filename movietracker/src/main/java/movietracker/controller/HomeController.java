package movietracker.controller;


/** @author Maximilian Kostial
 * 
 */


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import movietracker.model.Movie;
import movietracker.model.MovieUser;
import movietracker.model.User;
import movietracker.model.WatchList;
import movietracker.services.MovieService;
import movietracker.services.MovieUserService;
import movietracker.services.UserService;




@Controller
@RequestMapping(value={"/", "/home"})
public class HomeController{
	
	@Autowired
	private User user; 
	@Autowired 
	private UserService userService;	
	@Autowired
	private MovieService movieService;
	@Autowired
	private MovieUserService movieUserService;
	@Autowired
	private WatchList watchList;
	
	@RequestMapping(method = RequestMethod.GET)
	 public String getHomeView(Model model){
		if (user.getUsername() == null)
		   userService.setUserForSession();
		
		// Update the Movie to the Watchlist if the Watchlist is not empty
		// and the ID of the User for this Session is greater than 0
		long minID = 0;
		if (!this.watchList.getMovies().isEmpty() && minID < user.getUserID() )
			for (Movie movie : watchList.getMovies()){
				MovieUser movieUser = MovieUser.newBuilder()
						.withUserID(user.getUserID())
						.withMovieID(movie.getMovieID())
						.withWatchlist(true)
						.build();
				movieUserService.updateMovieUser(movieUser);
			}

		if (this.user.getUsername() != null) {
	    	     // Finds all MovieUsers for the User of this Session
	    	     List<MovieUser> movieUsers = movieUserService.findMovieUserByUserId(this.user.getUserID());		
	         	// Builds a Watchlist for the User of this Session
    	    	List<Movie> watchListFromDB = new ArrayList<Movie>();				
                // Adds the Movie to the Watchlist if isWatchlist is true in MovieUser
		    for (MovieUser movieUser : movieUsers) {
	    	   if (movieUser.isWatchlist() == true) {
	        		long id = movieUser.getMovieID(); 
	        		Movie movie = movieService.findMovieById(id);
        			watchListFromDB.add(movie);
	        	}	
	    	}
		// Adds the Watchlist to the Model when logged in
		model.addAttribute("watchList", watchListFromDB);
		} else {
			// Adds the Watchlist to the Model when anonymous
			model.addAttribute("watchList", watchList.getMovies());
		}
		
		model.addAttribute("movies", movieService.findAllMovies());
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(this.user, true));
		model.addAttribute("user", this.user);
		return "home";
	}
	@RequestMapping(value="/watchlist/remove/{movieId}", method = RequestMethod.POST)
	 public String removeMovieFromWatchlist(@PathVariable long movieId, Model model){
		
		if (this.user.getUsername() != null) {
		 // Builds a MovieUser with isWatchlist false
		MovieUser movieUser = MovieUser.newBuilder()
				.withUserID(user.getUserID())
				.withMovieID(movieId)
				.withWatchlist(false)
				.build();
		movieUserService.updateMovieUser(movieUser);
		} else {
			Movie movie = movieService.findMovieById(movieId);
			watchList.getMovies().remove(movie);
		}
		return "redirect:/home";
	}
	
	@RequestMapping(value="/watchlist/add/{movieId}", method = RequestMethod.POST)
	 public String addMovieToWatchlist(@PathVariable long movieId, Model model){
		// Only if the User Name is not null the MovieUser in the DB is updated
		if (this.user.getUsername() != null) {
	    // Builds a MovieUser with isWatchlist true 
		MovieUser movieUser = MovieUser.newBuilder()
					.withMovieID(movieId)
					.withUserID(user.getUserID())
					.withWatchlist(true)
					.build();
		movieUserService.updateMovieUser(movieUser);
		} else {
			// 
			Movie movie = movieService.findMovieById(movieId);
			if (!watchList.getMovies().contains(movie))
			   watchList.getMovies().add(movie);
		}
		return "redirect:/home";
	}
	
	@RequestMapping
	public String search(@RequestParam("search")String movie){
		
		System.out.println(movie);
		return "redirect:/home";		
	}
}
