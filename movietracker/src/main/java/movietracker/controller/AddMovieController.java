package movietracker.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import movietracker.model.Actor;
import movietracker.model.Category;
import movietracker.model.Director;
import movietracker.model.Movie;
import movietracker.model.MovieDetails;
import movietracker.model.Status;
import movietracker.model.User;
import movietracker.services.ActorService;
import movietracker.services.CategoryService;
import movietracker.services.DirectorService;
import movietracker.services.MovieDetailsService;
import movietracker.services.MovieService;

@Controller

public class AddMovieController {

	@Autowired
	private MovieDetailsService movieDetailsService;

	@Autowired
	private MovieService movieService;

	@Autowired
	private DirectorService directorService;

	@Autowired
	private ActorService actorService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	User user;

	@RequestMapping(value = "/movie/addmovie")
	public String getAddMovieView(@ModelAttribute MovieDetails movieDetails, Model model) {

		model.addAttribute("user", this.user);
		model.addAttribute("movie", new Movie());
		model.addAttribute("editing", false);
		model.addAttribute("directorList", directorService.findAllDirectors());
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(this.user, true));
		return "addmovie";
	}

	@RequestMapping(value = "/movie/{id}/edit")
	public String editMovieView(@PathVariable(value = "id") Long id, Model model) {
		model.addAttribute("user", this.user);
		model.addAttribute("editing", true);
		Movie movie = movieService.findMovieById(id);
		List<Category> notAssignedCategories = new ArrayList<Category>();
		for (Category c : categoryService.findAllCategories()) {
			boolean movieHas = false;
			for (Category moviec : movie.getCategories()) {
				if (moviec.getId() == c.getId()) movieHas = true;
			}
			if (!movieHas) notAssignedCategories.add(c);
		}
		List<Actor> notAssignedActors = new ArrayList<Actor>();
		for (Actor a : actorService.findAllActors()) {
			if (!movie.getActors().contains(a))
				notAssignedActors.add(a);
		}
		model.addAttribute("movie", movie);
		model.addAttribute("directorList", directorService.findAllDirectors());
		model.addAttribute("actorList", notAssignedActors);
		model.addAttribute("categoryList", notAssignedCategories);
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(this.user, true));
		return "addmovie";
	}

	@RequestMapping(value = "/movie/addmovie/submit", method = RequestMethod.POST)
	public String submitMovie(@ModelAttribute Movie movie, Model model) {

		List<String> movieNameList = new ArrayList<>();
		List<Movie> movies = movieService.findAllMovies();
		movies.stream().forEach(m -> movieNameList.add(m.getTitle()));
		boolean editing = false;
		for (Movie m : movies) {
			if (m.getMovieID() == movie.getMovieID())
				editing = true;
		}
		Long movieID = movie.getMovieID();
		if (editing) {
			if(user.isAdmin()){
				Movie newMovie = movieService.findMovieById(movie.getMovieID());
				newMovie.setTitle(movie.getTitle());
				newMovie.setAgeLimitation(movie.getAgeLimitation());
				newMovie.setReleaseDate(movie.getReleaseDate());
				newMovie.setDescription(movie.getDescription());
				newMovie.setDuration(movie.getDuration());
				newMovie.setDirectorID(movie.getDirectorID());
				movieService.updateMovie(newMovie);
				movieID = newMovie.getMovieID();
			}else{
				MovieDetails movieDetails = new MovieDetails();
				movieDetails.setAgeLimitation(movie.getAgeLimitation());
				movieDetails.setCreatedBy(user.getUsername());
				movieDetails.setDescription(movie.getDescription());
				movieDetails.setDuration(movie.getDuration());
				Director director = directorService.findDirectorById(movie.getDirectorID());
				movieDetails.setDirectorName(director.getFirstName() + " " + director.getLastName());
				movieDetails.setMovieID(movie.getMovieID());
				movieDetails.setTitle(movie.getTitle());
				movieDetailsService.updateMovieDetails(movieDetails);
			}
		} else {
			Movie newMovie = new Movie();
			newMovie.setTitle(movie.getTitle());
			newMovie.setAgeLimitation(movie.getAgeLimitation());
			newMovie.setReleaseDate(movie.getReleaseDate());
			newMovie.setDescription(movie.getDescription());
			newMovie.setDuration(movie.getDuration());
			newMovie.setDirectorID(movie.getDirectorID());
			newMovie.setCreatedBy(this.user.getUsername());
			newMovie.setRank(0);
			newMovie.setNumberOfVotes(0);
			newMovie.setStatus(Status.PENDING);
			if (user.isAdmin()) {
				newMovie.setStatus(Status.APPROVED);
			}
			if (!movieNameList.contains(newMovie.getTitle())) {
				movieService.addMovie(newMovie);
			}
			movieID = newMovie.getMovieID();
		}
		
		return "redirect:/movie/" + movieID + "/edit";
	}

	@RequestMapping(value = "/movie/{id}/addcategory/{categoryID}")
	public String addCategory(@PathVariable(value = "categoryID") Long categoryID, @PathVariable(value = "id") Long id,
			Model model) {

		
		Movie movie = movieService.findMovieById(id);
		List<Category> categories = movie.getCategories();
		categories.add(categoryService.findCategoryById(categoryID));
		movie.setCategories(categories);
		movieService.updateMovie(movie);
		
		
		return "redirect:/movie/" + movie.getMovieID() + "/edit";
	}

	@RequestMapping(value = "/movie/{id}/removecategory/{categoryID}")
	public String removeCategory(@PathVariable(value = "categoryID") Long categoryID,
			@PathVariable(value = "id") Long id, Model model) {

		Movie movie = movieService.findMovieById(id);
		List<Category> categories = new ArrayList<Category>();
		for (Category c : movie.getCategories()) {
			if (c.getId() != categoryID)
				categories.add(c);
		}
		movie.setCategories(categories);
		movieService.updateMovie(movie);
		return "redirect:/movie/" + movie.getMovieID() + "/edit";
	}

	@RequestMapping(value = "/movie/{id}/addactor/{actorID}")
	public String addActor(@PathVariable(value = "actorID") Long actorID, @PathVariable(value = "id") Long id,
			Model model) {

		Movie movie = movieService.findMovieById(id);
		List<Actor> actors = movie.getActors();
		actors.add(actorService.findActorById(actorID));
		movie.setActors(actors);
		movieService.updateMovie(movie);
		return "redirect:/movie/" + movie.getMovieID() + "/edit";
	}

	@RequestMapping(value = "/movie/{id}/removeactor/{actorID}")
	public String removeActor(@PathVariable(value = "actorID") Long actorID,
			@PathVariable(value = "id") Long id, Model model) {

		Movie movie = movieService.findMovieById(id);
		List<Actor> actors = new ArrayList<Actor>();
		for (Actor a : movie.getActors()) {
			if (a.getActorID() != actorID)
				actors.add(a);
		}
		movie.setActors(actors);
		movieService.updateMovie(movie);
		return "redirect:/movie/" + movie.getMovieID() + "/edit";
	}

}
