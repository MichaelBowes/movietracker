package movietracker.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import movietracker.model.Movie;
import movietracker.model.MovieSearch;
import movietracker.model.Status;
import movietracker.model.User;
import movietracker.services.CategoryService;
import movietracker.services.MovieService;
import movietracker.util.SearchUtil;

@Controller
@RequestMapping(value="/tracker")
public class TrackerController {

	@Autowired
	private User user; 
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private MovieService movieService;

	
	@RequestMapping()
	public String getTrackerView(@ModelAttribute MovieSearch movieSearch,Model model){
		model.addAttribute("user", this.user);
		model.addAttribute("allCategories",categoryService.findAllCategories());
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(this.user, true));
		return "tracker";
	}
    
	
	@RequestMapping(value="/search", method = RequestMethod.POST)
	public String searchMovie(@ModelAttribute MovieSearch movieSearch, Model model){
			
		SearchUtil search = new SearchUtil();
		
		List<Movie> foundMovies = new ArrayList<>();
		
		foundMovies = movieService.findAllMovies().stream()
					.filter(m -> m.getStatus() == Status.APPROVED)
					.filter(m -> Pattern.compile(Pattern.quote(movieSearch.getMovieTitle()),Pattern.CASE_INSENSITIVE).matcher(m.getTitle()).find())
					.filter(m -> search.containsActor(movieSearch.getActorName(), m))
					.filter(m -> m.getAgeLimitation() <= movieSearch.getAgeRating())
					.filter(m -> (Integer.parseInt(m.getReleaseDate().substring(m.getReleaseDate().length() - 4)) > movieSearch.getFrom())
								&&(Integer.parseInt(m.getReleaseDate().substring(m.getReleaseDate().length() - 4)) < movieSearch.getUntil()))
					.filter(m -> ((Float.compare(m.getRank(),movieSearch.getRank())) == 0) || (Float.compare(m.getRank(),movieSearch.getRank()) > 0))
					.filter(m -> search.containsCategory(movieSearch.getCategoryList(),m))
					.collect(Collectors.toList());
		
		model.addAttribute("user", this.user);	
		model.addAttribute("allCategories",categoryService.findAllCategories());
		model.addAttribute("foundMovies", foundMovies);
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(this.user, true));
		return "tracker";
	}
	
	@RequestMapping(value="/search", method = RequestMethod.GET)
	public String searchMovie(@RequestParam("search") String search, Model model){
			
		
		List<Movie> foundMovies = new ArrayList<>();
		
		foundMovies = movieService.findAllMovies().stream()
					.filter(m -> m.getStatus() == Status.APPROVED)
					.filter(m -> Pattern.compile(Pattern.quote(search),Pattern.CASE_INSENSITIVE).matcher(m.getTitle()).find())
					.collect(Collectors.toList());
		
		model.addAttribute("movieSearch", new MovieSearch());	
		model.addAttribute("user", this.user);	
		model.addAttribute("allCategories",categoryService.findAllCategories());
		model.addAttribute("foundMovies", foundMovies);
		model.addAttribute("moviesSeen", movieService.findAllMoviesSeenByUser(this.user, true));
		return "tracker";
	}
	

	
}
