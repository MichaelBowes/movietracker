package movietracker.database;
/**
 * @author Maximilian Kostial
 */
import java.util.List;

import movietracker.model.Actor;

public interface ActorDAO {
	
	public void insert(Actor actor);
	
	public Actor select(long id);
	
	public Actor selectByName(String firstName, String lastName);
	
	public List<Actor> selectAll();
	
	public void delete(long id);
	
	public void update(Actor actor);
	

}
