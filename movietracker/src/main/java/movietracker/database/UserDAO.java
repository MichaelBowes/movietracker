package movietracker.database;

import movietracker.model.*;
import java.util.List;

import javax.persistence.NoResultException;

/**
 * 
 * Data Access Object Interface for {@code User}.
 * 
 * @author Michael Bowes
 * 
 */
public interface UserDAO {
	
	public void insert(User user);
	
	public User selectUserById(long id);
	
	public User selectUserByName(String name) throws NoResultException ;
	
	public List<User> selectAll();
	
	public void delete(long id);
	
	public void update(User user);
	
}
