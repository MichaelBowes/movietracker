package movietracker.database;

import java.util.List;

import movietracker.model.Category;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;

@Transactional
@Repository("categoryDAO")
public class CategoryDAOImpl implements CategoryDAO {

	private static final Logger logger = LoggerFactory.logger(CategoryDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void insert(Category category) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(category);
		logger.info("Category " + category.getCategoryName() + " was successfully added.");
	}

	@Override
	public Category select(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Category c = (Category) session.get(Category.class, id);
		if (c != null) logger.info("Category with ID: " + c.getId() + " selected.");
		else logger.info("Category with ID: " + id + " was NOT found.");
		return c;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Category> selectAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Category> categoryList = session.createQuery("from Category").list();
		for(Category c : categoryList){
			logger.info("Category List::" + c);
		}
		return categoryList;
	}

	@Override
	public void delete(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Category c = (Category) session.load(Category.class, id);
		String tempName = select(id).getCategoryName();
		if(null != c){
			session.delete(c);
		}
		logger.info("Category" + tempName +  " was succsessfully deleted." );
	}

	@Override
	public void update(Category category) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(category);
		logger.info("Category " + category.getCategoryName() + " updated.");
	}

}
