package movietracker.database;

import movietracker.model.MovieUser;
import movietracker.model.MovieUserPrimaryKey;
import movietracker.model.User;

import java.util.List;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;

/**
 * 
 * @author Stanislav Lokhtin
 *
 */


@Repository("movieUserDAO")
public class MovieUserDAOImpl implements MovieUserDAO {

	private static final Logger logger = LoggerFactory.logger(MovieDAOImpl.class); 

	@Autowired
	private SessionFactory sessionFactory;
    
    @Autowired
	UserDAO userDAO;

	@Override
	@Transactional
	public MovieUser insert(MovieUser movieUser) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(movieUser);
		logger.info("MovieUser " + movieUser.toString() + " was successfully added.");
		return movieUser;
	}

	@Override
	@Transactional
	public MovieUser select(long userID, long movieID) {
		Session session = this.sessionFactory.getCurrentSession();
		MovieUser c = (MovieUser) session.get(MovieUser.class, new MovieUserPrimaryKey(movieID, userID) );
		if (c != null) logger.info("MovieUser: " + c.toString() + " selected.");
		else logger.info("MovieUser with IDs: " + movieID + " " + userID + " was NOT found.");
		return c;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<MovieUser> selectAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<MovieUser> movieUserList = session.createQuery("from MovieUser").list();  
		for (MovieUser c : movieUserList) {
			logger.info("MovieUser List:" + c.toString());
		}
		return movieUserList;
		
	}

	@Override
	@Transactional
	public void delete(long userID, long movieID) {
		Session session = this.sessionFactory.getCurrentSession();
		MovieUser c = (MovieUser) session.get(MovieUser.class, new MovieUserPrimaryKey(movieID, userID));
	
		if(c != null){
			session.delete(c);
			logger.info("MovieUser"  + movieID + " " + userID +  " was succsessfully deleted." );
		}
		
	}
	

	@Override
	@Transactional
	public void deleteForAllUsers(long movieID) {
		Session session = this.sessionFactory.getCurrentSession();
		List<User> users = userDAO.selectAll();
		for(User user: users) {
			MovieUser c = (MovieUser) session.get(MovieUser.class, new MovieUserPrimaryKey(movieID, user.getUserID()));
			if(c != null){
				session.delete(c);
				logger.info("MovieUser"  + movieID + " " + user.getUserID() +  " was succsessfully deleted." );
			}
		}
	}

	@Override
	@Transactional
	public void update(MovieUser movieUser) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(movieUser);
		logger.info("MovieUser " + movieUser.toString() + " updated.");
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<MovieUser> selectByUserId(long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MovieUser.class);
		criteria.add(Restrictions.eq("userID", id));
		List<MovieUser> movieUsers = criteria.list();
		for (MovieUser c : movieUsers) {
			logger.info("MovieUsers :" + c.toString());
		}
			
		return movieUsers;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<MovieUser> selectByMovieId(long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MovieUser.class);
		criteria.add(Restrictions.eq("movieID", id));
		List<MovieUser> movieUsers = criteria.list();
		for (MovieUser c : movieUsers) {
			logger.info("MovieUsers :" + c.toString());
		}
			
		return movieUsers;
	}
	
	
}
