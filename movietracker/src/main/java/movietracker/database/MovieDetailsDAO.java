package movietracker.database;
/**
* @author Michael Bowes
* 
*/
import java.util.List;
import movietracker.model.MovieDetails;
/**
 * 
 * Data Access Object Interface for {@code MovieDetails}.
 * 
 * 
 */
public interface MovieDetailsDAO {
	public void insert(MovieDetails movieDetails);
	public MovieDetails select(long id);
	public List<MovieDetails> selectAll();
	public void delete(long id);
	public void update(MovieDetails movieDetails);
	List<MovieDetails> selectAllByMovieID(long id);
}
