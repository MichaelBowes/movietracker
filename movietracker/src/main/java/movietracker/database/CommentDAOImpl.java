package movietracker.database;

import movietracker.model.Comment;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;

/**
 * 
 * @author Stanislav Lokhtin
 *
 */

@Transactional
@Repository("commentDAO")
public class CommentDAOImpl implements CommentDAO {

	private static final Logger logger = LoggerFactory.logger(MovieDAOImpl.class); 

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void insert(Comment comment) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(comment);
		logger.info("Comment " + comment.toString() + " was successfully added.");
	}

	@Override
	public Comment select(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Comment c = (Comment) session.get(Comment.class, new Long(id));
		if (c != null) logger.info("Comment: " + c.toString() + " selected.");
		else logger.info("Comment with ID: " + id + " was NOT found.");
		return c;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> selectAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Comment> commentList = session.createQuery("from Comment").list();  
		for (Comment c : commentList) {
			logger.info("Comment List:" + c.toString());
		}
		return commentList;
		
	}

	@Override
	public void delete(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Comment c = (Comment) session.get(Comment.class, id);
		String tempName = select(id).getText();
		if(null != c){
			session.delete(c);
		}
		logger.info("Comment" + tempName +  " was succsessfully deleted." );
	}

	@Override
	public void update(Comment comment) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(comment);
		logger.info("Comment " + comment.toString() + " updated.");
	}
	
}
