package movietracker.database;
/**
* @author Michael Bowes
* 
*/
import movietracker.model.*;
import java.util.List;

/**
 * 
 * Data Access Object Interface for {@code Movie}.
 * 
 * 
 */
public interface MovieDAO {

	public Movie insert(Movie movie);
	public Movie select(long id);
	public List<Movie> selectAll();
	public void delete(long id);
	public void update(Movie movie);
}
