package movietracker.database;

import java.util.List;
import movietracker.model.*;

public interface MovieUserDAO {
	public MovieUser insert(MovieUser movieUser);
	public MovieUser select(long userID, long movieID);
	public List<MovieUser> selectByUserId(long id);
	public List<MovieUser> selectAll();
	public void delete(long userID, long movieID);

	/**
	 * This methods calls the saveOrUpdate Method on the {@code CurrentSession} for the given {@code MovieUser}.
	 * <br> That is if the MovieUser exists already in the Database the MovieUser is updated, else it is 
	 *  created a new MovieUser.
	 * @param movieUser
	 */
	public void update(MovieUser movieUser);
	List<MovieUser> selectByMovieId(long id);
	void deleteForAllUsers(long movieID);
}
