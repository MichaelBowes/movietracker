package movietracker.database;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import movietracker.model.User;

/**
 * 
 * Data Access Object Implementation for {@code User}.
 * Implements {@code UserDAO} as interface.
 * 
 * @author Michael Bowes
 * 
 */
@Repository("userDAO")
@Transactional
public class UserDAOImpl implements UserDAO {

	private static final Logger logger = LoggerFactory.logger(UserDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	
	private Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}

	@Override
	public void insert(User user) {
      this.getSession().persist(user);
      logger.info("The user was added successfully : "+ user);
	}

	@Override
	public User selectUserById(long id) {
       User user = (User)this.getSession().get(User.class, id);
       logger.info("The User was successfully found : " + user);
       return user;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> selectAll() {
	List<User> userList = this.getSession().createQuery("from User").list();
	for (User user : userList)
	    logger.info(user);
	return userList;

	}

	@Override
	public void delete(long id) {
      User user = this.getSession().get(User.class, new Long(id));
      if (user != null){
    	  this.getSession().delete(user);
    	  logger.info("User was successfully deleted : " + user);
      } else {
    	  throw new IllegalArgumentException("The user was not found in the database!");
      }
	}

	@Override
	public void update(User user) {
      this.getSession().update(user);
	  logger.info("User was successfully updated : " + user);

	}

	@Override
	public User selectUserByName(String name) throws NoResultException {
		Criteria criteria = this.getSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("userName", name));
		criteria.setMaxResults(1);
	if(criteria.list().size() == 1) {
		return (User)criteria.uniqueResult();
	} else if (criteria.list().isEmpty()) {
		throw new NoResultException("User not found.");
	} else {
		throw new NonUniqueResultException(criteria.list().size());
	  }
	}


}
