package movietracker.database;
/**
* @author Michael Bowes
* 
*/
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import movietracker.model.Comment;
import movietracker.model.Movie;
import movietracker.model.MovieDetails;
import movietracker.model.MovieUser;
import movietracker.model.Vote;

/**
 * 
 * Data Access Object Implementation for {@code Movie}.
 * Implements {@code MovieDAO} as interface.
 * 
 * 
 */
@Repository("movieDAO")
public class MovieDAOImpl implements MovieDAO {

	private static final Logger logger = LoggerFactory.logger(MovieDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	MovieDetailsDAO movieDetailsDAO;

	@Autowired
	MovieUserDAO movieUserDAO;
	
	@Autowired
	CommentDAO commentDAO;
	
	@Autowired
	VoteDAO voteDAO;
	
	@Override
	@Transactional
	public Movie insert(Movie movie) {

		Session session = this.sessionFactory.getCurrentSession();
		session.save(movie);
		session.flush();
		logger.info("Movie" + movie.getTitle().toString() + "was successfully added.");
		return movie;

	}

	@Override
	@Transactional
	public Movie select(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Movie m = (Movie) session.get(Movie.class, id);
		if (m!=null)
			logger.info("Movie with ID: " + m.getMovieID() + " selected.");
		else
			logger.info("Movie with ID: " + id + " was NOT found.");
		return m;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Movie> selectAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Movie> movieList = session.createQuery("from Movie").list();   
		for (Movie m : movieList) {
			logger.info("Movie List:" + m.getTitle());
		}
		return movieList;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional
	public void delete(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<MovieDetails> details = movieDetailsDAO.selectAllByMovieID(id);
		for (MovieDetails md : details) {
			movieDetailsDAO.delete(md.getDetailsID());
		}
		List<MovieUser> movieUsers = movieUserDAO.selectByMovieId(id);
		for (MovieUser mu : movieUsers) {
			movieUserDAO.deleteForAllUsers(mu.getMovieID());
		}
		movieUsers = movieUserDAO.selectByMovieId(id);
		Movie m = (Movie) session.get(Movie.class, id);
		List<Comment> comments = m.getComments();
		if (m == null) return;
		
		for (Comment c : comments) {
			commentDAO.delete(c.getId());
		}
		List<Vote> votes = voteDAO.selectAllVotesOfAMovie(m);
		
		for (Vote v : votes) {
			voteDAO.delete(v.getId());
		}
		session.delete(m);
		
		logger.info("Movie was successfully deleted.");
	}

	@Override
	@Transactional
	public void update(Movie movie) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(movie);
		logger.info("Movie was successfully updated --- ID: " + movie.getMovieID());
	}

}
