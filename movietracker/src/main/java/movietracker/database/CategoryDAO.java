package movietracker.database;

import java.util.List;
import movietracker.model.*;

public interface CategoryDAO {
	public void insert(Category category);
	public Category select(long id);
	public List<Category> selectAll();
	public void delete(long id);
	public void update(Category caregory);
}
