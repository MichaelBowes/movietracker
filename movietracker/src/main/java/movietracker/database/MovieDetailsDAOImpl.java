package movietracker.database;
/**
* @author Michael Bowes
* 
*/
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import movietracker.model.MovieDetails;
/**
 * 
 * Data Access Object Implementation for {@code MovieDetails}.
 * Implements {@code MovieDetailsDAOImpl} as interface.
 * 
 * 
 */
@Repository("movieDetailsDAO")
@Transactional
public class MovieDetailsDAOImpl implements MovieDetailsDAO {

private static final Logger logger = LoggerFactory.logger(MovieDetailsDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void insert(MovieDetails movieDetails) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(movieDetails);
		logger.info("Movie" + movieDetails.getTitle() + "was successfully added.");
	}

	@Override
	public MovieDetails select(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		MovieDetails md = (MovieDetails) session.get(MovieDetails.class, id);
		if (md!=null)
			logger.info("MovieDetails with ID: " + md.getDetailsID() + " selected.");
		else
			logger.info("MovieDetails with ID: " + id + " was NOT found.");
		return md;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MovieDetails> selectAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<MovieDetails> movieDetailsList = session.createQuery("from MovieDetails").list();   
		for (MovieDetails md : movieDetailsList) {
			logger.info("MovieDetails List:" + md);
		}
		return movieDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MovieDetails> selectAllByMovieID(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<MovieDetails> movieDetailsList = session.createQuery("FROM MovieDetails MD WHERE MD.MovieID = " + id).list();   
		for (MovieDetails md : movieDetailsList) {
			logger.info("MovieDetails List:" + md);
		}
		return movieDetailsList;
	}

	@Override
	public void delete(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		MovieDetails md = (MovieDetails) session.get(MovieDetails.class, id);
		if (null != md) {
			session.delete(md);
		}
		logger.info("MovieDetails was successfully deleted.");
	}

	@Override
	public void update(MovieDetails movieDetails) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(movieDetails);
		logger.info("MovieDetails was successfully updated --- ID: " + movieDetails.getDetailsID());
	}
	
}
