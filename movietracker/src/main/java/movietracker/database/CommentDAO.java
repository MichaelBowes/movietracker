package movietracker.database;

import java.util.List;
import movietracker.model.*;

public interface CommentDAO {
	public void insert(Comment comment);
	public Comment select(long id);
	public List<Comment> selectAll();
	public void delete(long id);
	public void update(Comment comment);
}
