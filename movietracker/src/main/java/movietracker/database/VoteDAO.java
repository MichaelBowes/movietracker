package movietracker.database;

import java.util.List;

import movietracker.model.Movie;
import movietracker.model.User;
import movietracker.model.Vote;

public interface VoteDAO {
	public void insert(Vote category);
	public Vote select(long id);
	public List<Vote> selectAll();
	public void delete(long id);
	public void update(Vote caregory);
	public List<Vote> selectAllVotesOfAUser(User user);
	public List<Vote> selectAllVotesOfAMovie(Movie movie);
}
