package movietracker.database;
/**
 * @author Maximilian Kostial
 */
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import movietracker.model.Actor;

@Transactional
@Repository("actorDAO")
public class ActorDAOImpl implements ActorDAO {
	
	private static final Logger logger = LoggerFactory.logger(ActorDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}

	@Override
	public void insert(Actor actor) {
		this.getSession().persist(actor);
		logger.info("The actor was added successfully : " + actor);
		
	}

	@Override
	public Actor select(long id) {
		Actor actor = this.getSession().get(Actor.class, id);
		logger.info("The actor was successfully found : " + actor);
		return actor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Actor> selectAll() {
		List<Actor> actorList = this.getSession().createQuery("from Actor").list();
		for (Actor actor : actorList)
		   logger.info("The Actor : " + actor);
		return actorList;
	}

	@Override
	public void delete(long id) {
		Actor actor = this.getSession().get(Actor.class, id);
		if (actor != null) {
			this.getSession().delete(actor);
		    logger.info("The actor " + actor + " was deleted successfully.");
		} else {
	    	  throw new IllegalArgumentException("The actor was not found in the database!");
		}
	}

	@Override
	public void update(Actor actor) {
		this.getSession().update(actor);
		logger.info("The actor was updated successfully : " + actor);
		
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public Actor selectByName(String firstName, String lastName) {
		Criteria criteria = this.getSession().createCriteria(Actor.class);
		criteria.add(Restrictions.eq("firstName", firstName));
		criteria.add(Restrictions.eq("lastName", lastName));
		Actor actor = null;
	for (Actor actorFromDB : (List<Actor>)criteria.list())
	  if(actorFromDB.getFirstName().equals(firstName)&& actorFromDB.getLastName().equals(lastName)) 
	          actor = actorFromDB;	    
    	 else if (criteria.list().isEmpty()) 
    		throw new NoResultException("Actor not found.");
	return actor;	  
	}



}
