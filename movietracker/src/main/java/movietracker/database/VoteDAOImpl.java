package movietracker.database;

import java.util.ArrayList;
import java.util.List;

import movietracker.model.Movie;
import movietracker.model.User;
import movietracker.model.Vote;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;


@Transactional
@Repository("voteDAO")
public class VoteDAOImpl implements VoteDAO {

	private static final Logger logger = LoggerFactory.logger(VoteDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void insert(Vote vote) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(vote);
		logger.info("Vote was successfully added.");
	}

	@Override
	public Vote select(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Vote v = (Vote) session.get(Vote.class, id);
		if (v != null) logger.info("Vote: " + v.toString() + " selected.");
		else logger.info("Vote with ID: " + id + " was NOT found.");
		return v;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Vote> selectAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Vote> voteList = session.createQuery("from Vote").list();  
		for (Vote v : voteList) {
			logger.info("Vote List:" + v.toString());
		}
		return voteList;
	}

	@Override
	public void delete(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Vote v = (Vote) session.load(Vote.class, id);
		if(null != v){
			session.delete(v);
		}
		logger.info("Vote " + id + " was successfully deleted.");
	}

	@Override
	public void update(Vote vote) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(vote);
		logger.info("Vote was successfully updated --- ID: " + vote.getId());
	}

	@Override
	public List<Vote> selectAllVotesOfAUser(User user) {
		List<Vote> voteList = this.selectAll();  
		List<Vote> result = new ArrayList<Vote>();
		for (Vote v : voteList) {
			if (v.getUserID() == user.getUserID()) result.add(v);
		}
		return result;
	}

	@Override
	public List<Vote> selectAllVotesOfAMovie(Movie movie) {
		List<Vote> voteList = this.selectAll();  
		List<Vote> result = new ArrayList<Vote>();
		for (Vote v : voteList) {
			if (v.getMovieID() == movie.getMovieID()) result.add(v);
		}
		return result;
	}

}
