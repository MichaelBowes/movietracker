package movietracker.database;
/**
 * @author Maximilian Kostial
 */
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import movietracker.model.Director;
import movietracker.model.Movie;
import movietracker.model.MovieDetails;

@Transactional
@Repository("directorDAO")
public class DirectorDAOImpl implements DirectorDAO {
	
	private static final Logger logger = LoggerFactory.logger(DirectorDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private MovieDAO movieDAO;

	private Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}
	
	@Override
	public void insert(Director director) {
		this.getSession().persist(director);
		logger.info("The director was successfully added : " + director);
	}

	@Override
	public Director select(long id) {
		Director director = this.getSession().get(Director.class, id);
		logger.info("The director : " + director + " was found.");
		return director;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Director> selectAll() {
		List<Director> directorList = this.getSession().createQuery("from Director").list();
		for (Director director : directorList)
			logger.info(director);
		return directorList;
	}

	@Override
	public void delete(long id) {
		Director director = this.getSession().get(Director.class, id);
		List<Movie> movies = movieDAO.selectAll();
		for (Movie m : movies) {
			if (m.getDirectorID() == id) movieDAO.delete(m.getMovieID());
		}
		if (director != null){
			this.getSession().delete(director);
			logger.info("The director : " + director + " was deleted successfully.");
		} else {
			throw new IllegalArgumentException("The director was not found in the database!");
		}		
	}

	@Override
	public void update(Director director) {
		this.getSession().update(director);
		logger.info("The director was updated successfully : " + director);		
	}

}
