package movietracker.database;
/**
 * @author Maximilian Kostial
 */
import java.util.List;

import movietracker.model.Director;

public interface DirectorDAO {
	
	public void insert(Director director);
	
	public Director select(long id);
	
	public List<Director> selectAll();
	
	public void delete(long id);
	
	public void update(Director director);
	

}
