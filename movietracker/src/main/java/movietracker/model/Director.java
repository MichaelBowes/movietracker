package movietracker.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Maximilian Kostial 
 */

@Entity
@Table(name="director")
public class Director extends Person {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long directorID;

	public Director(){
		
	}
	public Director(Builder builder){
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
	}
	public long getDirectorID() {
		return directorID;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setDirectorID(long directorID) {
		this.directorID = directorID;
	}
	
	public static Builder newBuilder(){
		return new Builder();	
	}
	
	public static class Builder{
		private String firstName;	
		private String lastName;
		
		private Builder(){
			
		}
		public Builder withFirstName(String firstName){
			this.firstName = firstName;
			return this;
		}
		public Builder withLastName(String lastName){
			this.lastName = lastName;
			return this;
		}
		public Director build(){
			return new Director(this);
		}
	}

	@Override
	public String toString() {
		return "Director [directorID=" + directorID +
				", firstName=" + firstName +
				", lastName=" + lastName + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (directorID ^ (directorID >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Director other = (Director) obj;
		if (directorID != other.directorID)
			return false;
		return true;
	}	
}
