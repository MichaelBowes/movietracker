package movietracker.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author Maximilian Kostial 
 */
@Entity
@Table(name="actor")
public class Actor extends Person {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long actorID;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthDate;
	
	private String biography;
	@ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST
    })
	@JoinTable(name = "movieactors", 
		joinColumns =  @JoinColumn(name = "actorID") ,
		inverseJoinColumns =  @JoinColumn(name = "movieID"))
	@JsonManagedReference 
	private List<Movie> movies;

	public Actor(){
		
	}
	public Actor(Builder builder){
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.birthDate = builder.birthDate;
		this.biography = builder.biography;
	}
	
	public long getActorID() {
		return actorID;
	}

	public void setActorID(long actorID) {
		this.actorID = actorID;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}
	
	public List<Movie> getMovies() {
		return movies;
	}
	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}
	public static Builder newBuilder(){
		return new Builder();	
	}
	
	public static class Builder{
		private String firstName;	
		private String lastName;
		private Date birthDate;
		private String biography;
		
		private Builder(){
			
		}

		public Builder withFirstName(String firstName){
			this.firstName = firstName;
			return this;
		}
		public Builder withLastName(String lastName){
			this.lastName = lastName;
			return this;
		}
		public Builder withBirthDate(Date birthDate){
			this.birthDate = birthDate;
			return this;
		}
		public Builder withBiography(String biography){
			this.biography = biography;
			return this;
		}
		public Actor build(){
			return new Actor(this);
		}
	}
	

	@Override
	public String toString() {
		return "Actor [actorID=" + actorID + 
				", firstName=" + firstName +
				", lastName=" + lastName +
				", birthDate=" + birthDate + 
				", biography=" + biography + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (actorID ^ (actorID >>> 32));
		result = prime * result + ((biography == null) ? 0 : biography.hashCode());
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Actor other = (Actor) obj;
		if (actorID != other.actorID)
			return false;
		if (biography == null) {
			if (other.biography != null)
				return false;
		} else if (!biography.equals(other.biography))
			return false;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		return true;
	}
	
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}
	
}
