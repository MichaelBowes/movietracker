package movietracker.model;


/**
* @author Michael Bowes
* 
*/
import java.util.List;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
@Table(name = "movie")
public class Movie {

	/**
	 * 
	 * Creates a new {@code Movie}.
	 * 
	 * @param movieID
	 *            the {@code Movie}'s MovieID. Mustn't be null or empty.
	 * 
	 * @param description
	 *            the {@code Movie}'s description. Mustn't be null or empty.
	 * 
	 * @param title
	 *            the {@code Movie}'s title. Mustn't be null or empty.
	 * 
	 * @param duration
	 *            the {@code Movie}'s duration. Mustn't be null or empty.
	 * 
	 * @param ageLimitation
	 *            the {@code Movie}'s ageLimitation. Mustn't be null or empty.
	 * 
	 * @param releaseDate
	 *            the {@code Movie}'s releaseDate. Mustn't be null or empty.
	 * 
	 * @param directorID
	 *            the {@code Movie}'s directorID. Mustn't be null or empty.
	 * 
	 * @param createdBy
	 *            the {@code Movie}'s createdBy. Mustn't be null or empty.
	 * 
	 * @param status
	 *            the {@code Movie}'s status. Mustn't be null or empty.
	 * 
	 * @param rank
	 *            the {@code Movie}'s rank. Mustn't be null or empty.
	 * 
	 * @param numberOfVotes
	 *            the {@code Movie}'s numberOfVotes. Mustn't be null or empty.
	 * 
	 */

	private long movieID;
	private String description;				// required for creation
	private String title;					// required for creation
	private String duration;				// required for creation
	private int ageLimitation;				// required for creation
	private String releaseDate;				// required for creation
	private long directorID;				// required for creation
	private String createdBy;
	private Status status;
	private float rank;
	private int numberOfVotes;
	private List<Category> categories = new ArrayList<Category>();			// required for creation
	private List<Actor> actors = new ArrayList<Actor>();					// required for creation
	private List<Comment> comments = new ArrayList<Comment>();
	

	public Movie(long movieID, String description, String title, String duration, int ageLimitation, String releaseDate,
			long directorID, String createdBy, Status status, float rank, int numberOfVotes) {
		this.movieID = movieID;
		this.description = description;
		this.title = title;
		this.duration = duration;
		this.ageLimitation = ageLimitation;
		this.releaseDate = releaseDate;
		this.directorID = directorID;
		this.createdBy = createdBy;
		this.status = status;
		this.rank = rank;
		this.numberOfVotes = numberOfVotes;
	}

	public Movie() {
	}

	@Id
	@Column(name="movieID", nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getMovieID() {
		return movieID;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	@Column(name="title")
	public String getTitle() {
		return title;
	}

	@Column(name="duration")
	public String getDuration() {
		return duration;
	}

	@Column(name="ageLimitation")
	public int getAgeLimitation() {
		return ageLimitation;
	}

	@Column(name="releaseDate")
	public String getReleaseDate() {
		return releaseDate;
	}

	@Column(name="directorID")
	public long getDirectorID() {
		return directorID;
	}

	@Column(name="createdBy")
	public String getCreatedBy() {
		return createdBy;
	}
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public Status getStatus() {
		return status;
	}

	@Column(name="rank")
	public float getRank() {
		return rank;
	}

	@Column(name="numberOfVotes")
	public int getNumberOfVotes() {
		return numberOfVotes;
	}
	@Fetch(FetchMode.SUBSELECT)
	@ManyToMany(fetch = FetchType.EAGER, cascade = {
                CascadeType.DETACH,
                CascadeType.MERGE,
                CascadeType.REFRESH,
                CascadeType.PERSIST
        })
	@JoinTable(name = "moviecategories", 
			joinColumns =  @JoinColumn(name = "movieID") ,
			inverseJoinColumns =  @JoinColumn(name = "categoryID"))
	@JsonManagedReference
	public List<Category> getCategories(){
		return categories;
	}

	public void setMovieID(long movieID) {
		this.movieID = movieID;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setAgeLimitation(int ageLimitation) {
		this.ageLimitation = ageLimitation;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public void setDirectorID(long directorID) {
		this.directorID = directorID;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setRank(float rank) {
		this.rank = rank;
	}

	public void setNumberOfVotes(int numberOfVotes) {
		this.numberOfVotes = numberOfVotes;
	}
	
	public void setCategories(List<Category> categories){
		this.categories = categories;
	}
	@Fetch(FetchMode.SUBSELECT)
	@ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST
    })
	@JoinTable(name = "movieactors", 
		joinColumns =  @JoinColumn(name = "movieID") ,
		inverseJoinColumns =  @JoinColumn(name = "actorID"))
	@JsonBackReference
	public List<Actor> getActors(){
		return actors;
	}
	

	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "moviecomments", 
	joinColumns = { @JoinColumn(name = "movieID") }, 
	inverseJoinColumns = { @JoinColumn(name = "commentID") })
	@JsonManagedReference
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (movieID ^ (movieID >>> 32));
		result = prime * result + ((actors == null) ? 0 : actors.hashCode());
		result = prime * result + ageLimitation;
		result = prime * result + ((categories == null) ? 0 : categories.hashCode());
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (directorID ^ (directorID >>> 32));
		result = prime * result + ((duration == null) ? 0 : duration.hashCode());
		result = prime * result + numberOfVotes;
		result = prime * result + Float.floatToIntBits(rank);
		result = prime * result + ((releaseDate == null) ? 0 : releaseDate.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (movieID != other.movieID)
			return false;
		if (actors == null) {
			if (other.actors != null)
				return false;
		} else if (!actors.equals(other.actors))
			return false;
		if (ageLimitation != other.ageLimitation)
			return false;
		if (categories == null) {
			if (other.categories != null)
				return false;
		} else if (!categories.equals(other.categories))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (directorID != other.directorID)
			return false;
		if (duration == null) {
			if (other.duration != null)
				return false;
		} else if (!duration.equals(other.duration))
			return false;
		if (numberOfVotes != other.numberOfVotes)
			return false;
		if (Float.floatToIntBits(rank) != Float.floatToIntBits(other.rank))
			return false;
		if (releaseDate == null) {
			if (other.releaseDate != null)
				return false;
		} else if (!releaseDate.equals(other.releaseDate))
			return false;
		if (status != other.status)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	public void setActors(List<Actor> actors){
		this.actors = actors;
	}
	
}
