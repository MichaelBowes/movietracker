package movietracker.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Stanislav Lokhtin
 *
 */

@Entity
@Table(name = "comment")
public class Comment {

	@Id
    @Column(name="commentID", nullable=false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name="userID")
	private long userID;
    @Column(name="movieID")
	private long movieID;
    @Column(name="text")
    private String text;
    @Column(name="createdAt")
    private Calendar createdAt;	

	public Comment(long userID, long movieID, String text, Calendar createdAt) {
		this.userID = userID;
		this.movieID = movieID;
		this.text = text;
		this.createdAt = createdAt;
	}

	public Comment() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public long getMovieID() {
		return movieID;
	}

	public void setMovieID(long movieID) {
		this.movieID = movieID;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Calendar getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Calendar createdAt) {
		this.createdAt = createdAt;
	}
	
	 @Override
	  public String toString() {
	    return "Comment [id=" + id + ", userID=" + userID + ", movieID=" + movieID + ", text=" + text + ", createdAt=" + createdAt + "]";
	  }

}