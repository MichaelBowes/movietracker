
package movietracker.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Nicolas Bruch
 *
 */
@Entity
@Table(name = "vote")
public class Vote {
	
	@Id
	@Column(name="voteID", nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name="rating")
	private int rank;
	@Column(name="userID")
	private long userID;
	@Column(name="movieID")
	private long movieID;


	public Vote (){
		
	}
	public Vote(int rank, long userID, long movieID){
		this.rank = rank;
		this.userID = userID;
		this.movieID = movieID;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public long getMovieID() {
		return movieID;
	}

	public void setMovieID(long movieID) {
		this.movieID = movieID;
	}
	
	@Override
	public String toString(){
		return String.valueOf(this.id);
	}

}