package movietracker.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class WatchList {
	
	private final List<Movie> movies;

	public WatchList(){
		this.movies = new ArrayList<Movie>();
	}
	
	public List<Movie> getMovies() {
		return movies;
	}
		

}
