package movietracker.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * 
 * @author Stanislav Lokhtin
 *
 */

@SuppressWarnings("serial")
@Entity
@IdClass(MovieUserPrimaryKey.class)
@Table(name="movieUser")
public class MovieUser implements Serializable {

	/**
	 * 
	 * Creates a new {@code Watch list}.
	 * 
	 * @param watchListID
	 *            the {@code Movie}'s watch list ID. Mustn't be null or empty.
	 * 
	 * @param userID
	 *            the {@code Movie}'s user ID. Mustn't be null or empty.
	 * 
	 * @param movieID
	 *            the {@code Movie}'s movie ID. Mustn't be null or empty.
	 * 
	 * @param seen
	 *            the {@code Movie}'s seen status. Mustn't be null or empty.
	 * 
	 * @param watchlist
	 *            the {@code Movie}'s watch list status. Mustn't be null or empty.
	 * 
	 * @param dateWatchlist
	 *            the {@code Movie}'s watch list date. Mustn't be null or empty.
	 * 
	 * @author Michael Bowes
	 * 
	 */
	@Id
	@Column(name="movieID")
	private long movieID;
	@Id
    @Column(name="userID")
	private long userID;
    @Column(name="seen")
    private boolean seen;
    @Column(name="dateSeen")
    private Date dateSeen;	
    @Column(name="watchlist")
    private boolean watchlist;
    @Column(name="dateWatchlist")
    private Date dateWatchlist;	

	/**
	 * Constructor for the {@code movieUser}.
	 * 
	 * creates a new {@code movieUser} and sets the attributes therein.
	 */
    public MovieUser(long movieID, long userID, boolean seen, Date dateSeen, boolean watchlist, Date dateWatchlist) {
		this.movieID = movieID;
		this.userID = userID;
		this.seen = seen;
		this.dateSeen = dateSeen;
		this.watchlist = watchlist;
		this.dateWatchlist = dateWatchlist;
	}
    
    public MovieUser(Builder builder){
    	this.movieID = builder.movieID;
    	this.userID = builder.userID;
    	this.seen = builder.seen;
    	this.dateSeen = builder.dateSeen;
    	this.watchlist = builder.watchlist;
    	this.dateWatchlist = builder.dateWatchlist;
    }

	public MovieUser() {}


	public long getMovieID() {
		return movieID;
	}

	public void setMovieID(long movieID) {
		this.movieID = movieID;
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	public Date getDateSeen() {
		return dateSeen;
	}

	public void setDateSeen(Date dateSeen) {
		this.dateSeen = dateSeen;
	}

	public boolean isWatchlist() {
		return watchlist;
	}

	public void setWatchlist(boolean watchlist) {
		this.watchlist = watchlist;
	}

	public Date getDateWatchlist() {
		return dateWatchlist;
	}

	public void setDateWatchlist(Date dateWatchlist) {
		this.dateWatchlist = dateWatchlist;
	}
	
	public static Builder newBuilder(){
		return new Builder();	
	}
	
	public static class Builder{
		private long movieID;	
		private long userID;
		private boolean seen;
		private Date dateSeen;
		private boolean watchlist;
		private Date dateWatchlist;	
		
		private Builder(){
			
		}

		public Builder withMovieID(long movieID){
			this.movieID = movieID;
			return this;
		}
		public Builder withUserID(long userID){
			this.userID= userID;
			return this;
		}
		public Builder withSeen(boolean seen){
			this.seen = seen;
			return this;
		}
		public Builder withDateSeen(Date dateSeen){
			this.dateSeen = dateSeen;
			return this;
		}
		public Builder withWatchlist(boolean watchlist){
			this.watchlist = watchlist;
			return this;
		}		
		public Builder withDateWatchlist(Date dateWatchlist){
			this.dateWatchlist = dateWatchlist;
			return this;
		}
		public MovieUser build(){
			return new MovieUser(this);
		}
	}
	

	 @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (movieID ^ (movieID >>> 32));
		result = prime * result + (int) (userID ^ (userID >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovieUser other = (MovieUser) obj;
		if (movieID != other.movieID)
			return false;
		if (userID != other.userID)
			return false;
		return true;
	}

	@Override
	  public String toString() {
	    return "movieUser [ userID=" + userID + ", movieID=" + movieID + ", seen=" + seen + ", dateSeen=" + dateSeen +
	    		"watchlist=" + watchlist + ", dateWatchlist=" + dateWatchlist +"]";
	  }
}
