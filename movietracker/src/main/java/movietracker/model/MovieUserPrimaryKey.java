package movietracker.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MovieUserPrimaryKey implements Serializable{

	protected long movieID;

	protected long userID;
	
	
	public MovieUserPrimaryKey(){}
	
	public MovieUserPrimaryKey(long movieID, long userID) {
		super();
		this.movieID = movieID;
		this.userID = userID;
	}

	public long getMovieID() {
		return movieID;
	}

	public void setMovieID(long movieID) {
		this.movieID = movieID;
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovieUserPrimaryKey other = (MovieUserPrimaryKey) obj;
		if (movieID != other.movieID)
			return false;
		if (userID != other.userID)
			return false;
		return true;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (movieID ^ (movieID >>> 32));
		result = prime * result + (int) (userID ^ (userID >>> 32));
		return result;
	}

}
