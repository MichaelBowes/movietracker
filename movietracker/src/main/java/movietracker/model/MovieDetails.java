package movietracker.model;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
/**
* @author Michael Bowes
* 
*/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "movieDetails")
public class MovieDetails {

	/**
	 * 
	 * Creates a new {@code MovieDetails}.
	 * 
	 * @param DetailsID
	 *            the {@code MovieDetails}'s DetailsID. Mustn't be null or empty.
	 * 
	 * @param MovieID
	 *            the {@code MovieDetails}'s MovieID. Mustn't be null or empty.
	 * 
	 * @param description
	 *            the {@code MovieDetails}'s description. Mustn't be null or empty.
	 * 
	 * @param title
	 *            the {@code MovieDetails}'s title. Mustn't be null or empty.
	 * 
	 * @param ageLimitation
	 *            the {@code MovieDetails}'s ageLimitation. Mustn't be null or empty.
	 * 
	 * @param releaseDate
	 *            the {@code MovieDetails}'s releaseDate. Mustn't be null or empty.
	 * 
	 * @param directorID
	 *            the {@code MovieDetails}'s directorID. Mustn't be null or empty.
	 * 
	 * @param createdBy
	 *            the {@code MovieDetails}'s createdBy. Mustn't be null or empty.
	 * 
	 * @param status
	 *            the {@code MovieDetails}'s status. Mustn't be null or empty.
	 * 
	 */
	
	@Id
	@Column(name="detailsID", nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long DetailsID;
	@Column(name="movieID")
	private long MovieID;
	@Column(name="directorID")
	private long directorID;
	@Column(name="description")
	private String description;
	@Column(name="title")
	private String title;
	@Column(name="ageLimitation")
	private int ageLimitation;
	@Column(name="releaseDate")
	private String releaseDate;
	@Column(name="directorName")
	private String directorName;
	@Column(name="createdBy")
	private String createdBy;
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private Status status;
	@Column(name="duration")	
	private String duration;
	
	@Fetch(FetchMode.SUBSELECT)
	@ManyToMany(fetch = FetchType.EAGER, cascade = {
                CascadeType.DETACH,
                CascadeType.MERGE,
                CascadeType.REFRESH,
                CascadeType.PERSIST
        })
	@JoinTable(name = "moviedetailscategories", 
			joinColumns =  @JoinColumn(name = "detailsID") ,
			inverseJoinColumns =  @JoinColumn(name = "categoryID"))
	private List<Category> categories = new ArrayList<Category>();
	
	@Fetch(FetchMode.SUBSELECT)
	@ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST
    })
	@JoinTable(name = "moviedetailsactors", 
		joinColumns =  @JoinColumn(name = "detailsID") ,
		inverseJoinColumns =  @JoinColumn(name = "actorID"))
	private List<Actor> actors = new ArrayList<Actor>();			
	
	public MovieDetails(long detailsID, long movieID, String description, String title, int ageLimitation,
			String releaseDate, String directorName, String createdBy, Status status, String duration) {
		super();
		DetailsID = detailsID;
		MovieID = movieID;
		this.description = description;
		this.title = title;
		this.ageLimitation = ageLimitation;
		this.releaseDate = releaseDate;
		this.directorName = directorName;
		this.createdBy = createdBy;
		this.status = status;
		this.duration = duration;
	}

	public MovieDetails(){}

	public long getDetailsID() {
		return DetailsID;
	}

	public void setDetailsID(long detailsID) {
		DetailsID = detailsID;
	}

	public long getMovieID() {
		return MovieID;
	}

	public void setMovieID(long movieID) {
		MovieID = movieID;
	}

	public long getDirectorID() {
		return directorID;
	}

	public void setDirectorID(long directorID) {
		this.directorID = directorID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getAgeLimitation() {
		return ageLimitation;
	}

	public void setAgeLimitation(int ageLimitation) {
		this.ageLimitation = ageLimitation;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getDirectorName() {
		return directorName;
	}

	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public String getDuration() {
		return duration;
	}
	
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public List<Category> getCategories(){
		return categories;
	}
	
	public void setCategories(List<Category> categories){
		this.categories = categories;
	}
	
	public List<Actor> getActors(){
		return actors;
	}
	
	public void setActors(List<Actor> actors){
		this.actors = actors;
	}
	

}
