package movietracker.model;
/**
* @author Michael Bowes
* 
*/
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@SuppressWarnings("serial")
@Entity
@Table(name="user")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class User implements UserDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private  long userID;	
	private  String userName;
	private  String userEmail;
	private  Date userLastLogin;
	private  boolean isAdmin;
	private  String password;


	/**
	 * 
	 * Creates a new {@code User}.
	 * 
	 * @param userID
	 *            the {@code User}'s ID. Mustn't be null or empty.
	 * 
	 * @param userName
	 *            the {@code User}'s Name. Mustn't be null or empty.
	 * 
	 * @param userEmail
	 *            the {@code User}'s Email. Mustn't be null or empty.
	 * 
	 * @param userLastLogin
	 *            the {@code User}'s last login date in the form
	 *            "2016-09-21T13:37:43.7071946+02:00".
	 * 
	 * @param isAdmin
	 *            the {@code User}'s administrator rights verification. Mustn't
	 *            be null or empty.
	 * 
	 * @param password
	 *            the {@code User}'s password. Mustn't be null or empty.
	 * 
	 * @param description
	 *            the {@code User}'s description. Mustn't be null or empty.
	 * 
	 * @param watchlist
	 *            the {@code User}'s watch list. Mustn't be null or empty.
	 * 
	 */

	/**
	 * Constructor for the {@code User}.
	 * 
	 * creates a new {@code User} and sets the attributes therein.
	 */
	public User(long userID, String userName, String userEmail, Date userLastLogin, boolean isAdmin,
			String password) {
		super();
		this.userID = userID;
		this.userName = userName;
		this.userEmail = userEmail;
		try {
			this.userLastLogin = userLastLogin;
		} catch (DateTimeException e) {
			throw new DateTimeException("DateTime could not be read from the User class.");
		}
		this.isAdmin = isAdmin;
		this.password = password;
	}
	
	public User(){}

	/**
	 * Getter for the {@code User}'s ID.
	 * 
	 * @return the {@code User}'s ID.
	 */
	public long getUserID() {
		return userID;
	}
	
	/**
	 * Getter for the {@code User}'s Name.
	 * 
	 * @return the {@code User}'s Name.
	 */
	
	public String getUserName() {
		return userName;
	}

	/**
	 * Getter for the {@code User}'s Email.
	 * 
	 * @return the {@code User}'s Email.
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * Getter for the {@code User}'s last login.
	 * 
	 * @return the {@code User}'s last login.
	 */
	public Date getUserLastLogin() {
		return userLastLogin;
	}

	/**
	 * Getter for the {@code User}'s administrator rights verification.
	 * 
	 * @return the {@code User}'s administrator rights verification.
	 */
	public boolean isAdmin() {
		return isAdmin;
	}

	/**
	 * Getter for the {@code User}'s password.
	 * 
	 * @return the {@code User}'s password.
	 */
	public String getPassword() {
		return password;
	}


	public void setUserID(long userID) {
		this.userID = userID;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public void setUserLastLogin(Date userLastLogin) {
		this.userLastLogin = userLastLogin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isAdmin ? 1231 : 1237);
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((userEmail == null) ? 0 : userEmail.hashCode());
		result = prime * result + (int) (userID ^ (userID >>> 32));
		result = prime * result + ((userLastLogin == null) ? 0 : userLastLogin.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (isAdmin != other.isAdmin)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userEmail == null) {
			if (other.userEmail != null)
				return false;
		} else if (!userEmail.equals(other.userEmail))
			return false;
		if (userID != other.userID)
			return false;
		if (userLastLogin == null) {
			if (other.userLastLogin != null)
				return false;
		} else if (!userLastLogin.equals(other.userLastLogin))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", userName=" + userName + ", userEmail=" + userEmail + ", userLastLogin="
				+ userLastLogin + ", isAdmin=" + isAdmin + ", password=" + password + "]";
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
	     List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
	     if (this.isAdmin())
	         list.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
	     else 
	    	 list.add(new SimpleGrantedAuthority("ROLE_USER"));
		return list;
	}
	
	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
	
	

}
