package movietracker.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author Nicolas Bruch
 *
 */

@Entity
@Table(name="category")
public class Category {
	

	private long id;
	private String categoryName;
	private boolean isApproved;
	
	private List<Movie> movies = new ArrayList<Movie>();

	public Category(String categoryName, boolean isApproved){
		this.isApproved = isApproved;
		this.categoryName = categoryName;

	}
	
	public Category(){}
	
	@Id
	@Column(name="categoryID", nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public long getId(){
		return this.id;
	}

	@Column(name="categoryname")
	public String getCategoryName(){
		return this.categoryName;
	}
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST
    })
	@JoinTable(name = "moviecategories", 
		joinColumns =  @JoinColumn(name = "categoryID") ,
		inverseJoinColumns =  @JoinColumn(name = "movieID"))
	@JsonBackReference
    public List<Movie> getMovies() {
        return movies;
    }

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public void setId(long categoryID) {
		this.id = categoryID;
	}
	
    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
    @Column(name="isApproved")
	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}	
	
}