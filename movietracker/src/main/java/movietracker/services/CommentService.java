package movietracker.services;

import java.util.List;

import movietracker.model.Comment;

public interface CommentService {

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Comment findCommentById(long id) throws IllegalArgumentException;
	
	/**
	 * This method inserts a given comment to the database.
	 * 
	 * @param comment
	 */
	public void addComment(Comment comment) throws IllegalArgumentException;

	/**
	 * This method retrieves all comments from the CommentDAO.
	 * 
	 * @return 
	 */
	public List<Comment> findAllComments();

	/**
	 * This method deletes a comment from the database.
	 * 
	 * @param id
	 */
	public void deleteComment(long id) throws IllegalArgumentException;

	/**
	 * This method updates a given comment in the database.
	 * 
	 * @param comment
	 */
	public void updateComment(Comment comment) throws IllegalArgumentException;

}
