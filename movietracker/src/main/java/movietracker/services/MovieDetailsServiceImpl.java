package movietracker.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movietracker.database.MovieDetailsDAO;
import movietracker.model.MovieDetails;

/**
 * 
 * @author Nicolas Bruch
 *
 */
@Service("movieDetailsService")
public class MovieDetailsServiceImpl implements MovieDetailsService {

	@Autowired
	MovieDetailsDAO movieDetailsDAO;

	
	@Override
	public void addMovieDetails(MovieDetails movieDetails) throws IllegalArgumentException {
		if (movieDetails != null)
		    movieDetailsDAO.insert(movieDetails);
		else 
			throw new IllegalArgumentException("The given movieDetails must not be null!");
		
	}

	@Override
	public MovieDetails findMovieDetailsById(long id) throws IllegalArgumentException {
		return movieDetailsDAO.select(id);
	}

	@Override
	public List<MovieDetails> findAllMoviesDetails() {
		return movieDetailsDAO.selectAll();
	}
	

	@Override
	public void deleteMovieDetails(long id) throws IllegalArgumentException {		
		movieDetailsDAO.delete(id);
		
		
	}

	@Override
	public void updateMovieDetails(MovieDetails movieDetails) throws IllegalArgumentException {
		if(movieDetails != null){
			movieDetailsDAO.update(movieDetails);
		}else{
			throw new IllegalArgumentException("The given movie must not be null!");
		}
		
	}	
}
