package movietracker.services;
/**
 * @author Maximilian Kostial 
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movietracker.database.ActorDAO;
import movietracker.model.Actor;


@Service("actorService")
public class ActorServiceImpl implements ActorService {
	
	@Autowired
	private ActorDAO actorDAO;

	@Override
	public void addActor(Actor actor) throws IllegalArgumentException {
        if (actor == null)
        	throw new IllegalArgumentException("The Actor Object must not be null!");
		this.actorDAO.insert(actor);
	}

	@Override
	public Actor findActorById(long id) {
		return this.actorDAO.select(id);
	}

	@Override
	public List<Actor> findAllActors() {
		return this.actorDAO.selectAll();
	}

	@Override
	public void deleteActorById(long id) {
		this.actorDAO.delete(id);		
	}

	@Override
	public void updateActor(Actor actor) throws IllegalArgumentException {
        if (actor == null)
        	throw new IllegalArgumentException("The Actor Object must not be null!");
		this.actorDAO.update(actor);		
	}

	@Override
	public Actor findActorByName(String firstName, String lastName) {
		// TODO Auto-generated method stub
		return actorDAO.selectByName(firstName, lastName);
	}

}
