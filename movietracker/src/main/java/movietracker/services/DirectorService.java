package movietracker.services;
/**
 * @author Maximilian Kostial 
 */
import java.util.List;

import movietracker.model.Director;

public interface DirectorService {
	
	/**
	 * 
	 * @param Director
	 */
	public void addDirector(Director director)throws IllegalArgumentException ;

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Director findDirectorById(long id);

	/**
	 * 
	 * @return
	 */
	public List<Director> findAllDirectors();
   
	/**
     * @param id
     */
	public void deleteDirectorById(long id);
    
	/**
	 * @param Director
	 */
	public void updateDirector(Director director)throws IllegalArgumentException ;
}


