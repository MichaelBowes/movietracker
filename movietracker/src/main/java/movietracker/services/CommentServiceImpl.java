package movietracker.services;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movietracker.database.CommentDAO;
import movietracker.model.Comment;

/**
 * 
 * @author Stanislav Lokhtin
 *
 */
//@Component //("commentService")
//@Transactional
@Service("commentService")
public class CommentServiceImpl implements CommentService {

	@Autowired
	CommentDAO commentDAO;
	
	@Override
	public Comment findCommentById(long id) throws IllegalArgumentException {
		return commentDAO.select(id);
	}

	@Override
	public void addComment(Comment comment) throws IllegalArgumentException  {
		if (comment != null)
			commentDAO.insert(comment);
		else 
			throw new IllegalArgumentException("The given Comment must not be null!");
	}
	
	@Override
	public List<Comment> findAllComments() {
		return commentDAO.selectAll();
	}

	@Override
	public void deleteComment(long id) throws IllegalArgumentException {
		commentDAO.delete(id);
		
	}

	@Override
	public void updateComment(Comment comment) throws IllegalArgumentException {
		if (comment != null)
		    commentDAO.update(comment);
		else 
			throw new IllegalArgumentException("The given Comment must not be null!");	
	}

}
