package movietracker.services;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import movietracker.model.Movie;
import movietracker.model.MovieDetails;
import movietracker.model.User;

/**
 * 
 * @author Nicolas Bruch
 *
 */
public interface MovieService {

	/**
	 * 
	 * @param movie
	 * @return TODO
	 */
	public Movie addMovie(Movie movie)throws IllegalArgumentException ;

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Movie findMovieById(long id)throws IllegalArgumentException ;

	/**
	 * This method retrieves all movies from the MovieDAO. 
	 * 
	 * @return
	 */
	public List<Movie> findAllMovies();
   
	/**
     * This method deletes an movie from the database.
     * @param id
     */
	public void deleteMovie(long id)throws IllegalArgumentException ;
    
	/**
	 * This method updates a given movie in the database.
	 * @param movie
	 */
	public void updateMovie(Movie movie)throws IllegalArgumentException ;

	List<MovieDetails> getAllDetailsByID(long id) throws IllegalArgumentException;

	public void updateRanks();

	public List<Movie> findAllMoviesSeenByUser(User user, boolean seen);

	Movie findMovieByTitle(String title) throws IllegalArgumentException;

}


