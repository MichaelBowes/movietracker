package movietracker.services;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import movietracker.model.MovieDetails;

/**
 * 
 * @author Nicolas Bruch
 *
 */
public interface MovieDetailsService {

	/**
	 * 
	 * @param movie
	 */
	public void addMovieDetails(MovieDetails movieDetails)throws IllegalArgumentException ;

	/**
	 * 
	 * @param id
	 * @return
	 */
	public MovieDetails findMovieDetailsById(long id)throws IllegalArgumentException ;

	/**
	 * This method retrieves all movies from the MovieDAO. 
	 * 
	 * @return
	 */
	public List<MovieDetails> findAllMoviesDetails();
   
	/**
     * This method deletes an movie from the database.
     * @param id
     */
	public void deleteMovieDetails(long id)throws IllegalArgumentException ;
    
	/**
	 * This method updates a given movie in the database.
	 * @param movie
	 */
	public void updateMovieDetails(MovieDetails movieDetails)throws IllegalArgumentException ;

}


