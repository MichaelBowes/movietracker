package movietracker.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movietracker.database.MovieDAO;
import movietracker.database.MovieDetailsDAO;
import movietracker.database.VoteDAO;
import movietracker.model.Movie;
import movietracker.model.MovieDetails;
import movietracker.model.MovieUser;
import movietracker.model.User;
import movietracker.model.Vote;

/**
 * 
 * @author Nicolas Bruch
 *
 */
@Service("movieService")
public class MovieServiceImpl implements MovieService {

	@Autowired
	MovieDAO movieDAO;

	@Autowired
	MovieDetailsDAO movieDetailsDAO;

	@Autowired
	VoteDAO voteDAO;

	@Autowired
	MovieUserService movieUserService;

	@Override
	public Movie addMovie(Movie movie) throws IllegalArgumentException {
		if (movie != null)
			return movieDAO.insert(movie);
		else
			throw new IllegalArgumentException("The given movie must not be null!");

	}

	@Override
	public Movie findMovieById(long id) throws IllegalArgumentException {
		return movieDAO.select(id);
	}

	@Override
	public Movie findMovieByTitle(String title) throws IllegalArgumentException {
		List<Movie> all = this.findAllMovies();
		Movie result = null;
		for (Movie m : all) {
			if (m.getTitle().equals(title))
				result = m;
		}
		return result;
	}

	@Override
	public List<Movie> findAllMovies() {
		return movieDAO.selectAll();
	}

	@Override
	public void deleteMovie(long id) throws IllegalArgumentException {
		movieDAO.delete(id);

	}

	@Override
	public void updateMovie(Movie movie) throws IllegalArgumentException {
		if (movie != null) {
			movieDAO.update(movie);
		} else {
			throw new IllegalArgumentException("The given movie must not be null!");
		}

	}

	@Override
	public List<MovieDetails> getAllDetailsByID(long id) throws IllegalArgumentException {
		return movieDetailsDAO.selectAllByMovieID(id);
	}

	@Override
	public void updateRanks() {
		List<Movie> allMovies = movieDAO.selectAll();
		float movieRank;
		for (Movie m : allMovies) {
			movieRank = 0;
			List<Vote> movieVotes = voteDAO.selectAllVotesOfAMovie(m);
			if (movieVotes.size() > 0) {
				for (Vote v : movieVotes) {
					movieRank += v.getRank();
				}
				movieRank = movieRank / movieVotes.size();
				m.setRank(movieRank);
				m.setNumberOfVotes(movieVotes.size());
				movieDAO.update(m);
			}
		}
	}
//  if seen is false, then find all movies not seen by user instead
	@Override
	public List<Movie> findAllMoviesSeenByUser(User user, boolean seen) {
		List<Movie> allMovies = movieDAO.selectAll();
		List<Movie> result = new ArrayList<Movie>();
		List<MovieUser> movieUsers = new ArrayList<MovieUser>();
		for (Movie m : allMovies) {
			MovieUser movieUser = movieUserService.findMovieUserById(user.getUserID(), m.getMovieID());
			if (movieUser != null) {
				if (seen) {
					if (movieUser.isSeen()) {
						movieUsers.add(movieUser);
						result.add(m);
					}
				} else if (!movieUser.isSeen()) {
					movieUsers.add(movieUser);
					result.add(m);
				}
			} else if (!seen) {
			movieUsers.add(movieUser);
			result.add(m);
		}
		}
		if (seen) Collections.sort(result, new Comparator<Movie>() {
			MovieUser mu1;
			MovieUser mu2;

			public int compare(Movie m1, Movie m2) {
				for (MovieUser mu : movieUsers) {
					if (mu.getMovieID() == m1.getMovieID())
						mu1 = mu;
					if (mu.getMovieID() == m2.getMovieID())
						mu2 = mu;
				}
				if (mu1.getDateSeen() == mu2.getDateSeen())
					return 0;
				return mu1.getDateSeen().after(mu2.getDateSeen()) ? -1 : 1;
			}
		});
		return result;
	}

}
