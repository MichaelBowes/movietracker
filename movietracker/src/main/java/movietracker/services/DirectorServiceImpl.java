package movietracker.services;

/**
 * @author Maximilian Kostial 
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movietracker.database.DirectorDAO;
import movietracker.model.Director;


@Service("directorService")
public class DirectorServiceImpl implements DirectorService {

     @Autowired
     private DirectorDAO directorDAO;

	@Override
	public void addDirector(Director director) throws IllegalArgumentException {
		if (director == null)
			throw new IllegalArgumentException("The given Director Object must not be null!");
         directorDAO.insert(director);		
	}

	@Override
	public Director findDirectorById(long id) {
		return this.directorDAO.select(id);
	}

	@Override
	public List<Director> findAllDirectors() {
		return this.directorDAO.selectAll();
	}

	@Override
	public void deleteDirectorById(long id) {
		this.directorDAO.delete(id);
		
	}

	@Override
	public void updateDirector(Director director) throws IllegalArgumentException {
		if (director == null)
			throw new IllegalArgumentException("The given Director Object must not be null!");
		this.directorDAO.update(director);
	}
	
}
