package movietracker.services;
/**
 * @author Maximilian Kostial 
 */
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import movietracker.database.UserDAO;
import movietracker.model.User;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private User user;
	
	@Override
	public void addUser(User user)throws IllegalArgumentException  {
		if (user != null)
		    userDAO.insert(user);
		else 
			throw new IllegalArgumentException("The given User must not be null!");
	}

	@Override
	public User findUserById(long id){
		return userDAO.selectUserById(id);
	}

	@Override
	public List<User> findAllUsers() {
		return userDAO.selectAll();
	}

	@Override
	public void deleteUserById(long id) {
		userDAO.delete(id);
		
	}

	@Override
	public void updateUser(User user)throws IllegalArgumentException {
		if (user != null)
		    userDAO.update(user);
		else 
			throw new IllegalArgumentException("The given User must not be null!");	
	}

	@Override
	public User findUserByUserName(String name) {
		User user = null;
		 try {
			user = userDAO.selectUserByName(name);
		 } catch (NoResultException e) {
			user = new User();			
		 }
		 return user;
	}
	
	@Override
	public void setUserForSession(){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken) && auth != null) {
			User user =
					 (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		     this.user.setUserID(user.getUserID());
		     this.user.setUserName(user.getUsername());
		     this.user.setAdmin(user.isAdmin());
    }
  }

	@Override
	public User getUserForSession() {
		return this.user;
	}
 

}
