package movietracker.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import movietracker.database.CategoryDAO;
import movietracker.model.Category;
import movietracker.model.Movie;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryDAO categoryDAO;

	
	@Override
	public void addCategory(Category category)throws IllegalArgumentException  {
		if (category != null)
		    categoryDAO.insert(category);
		else 
			throw new IllegalArgumentException("The given Category must not be null!");
	}

	@Override
	public Category findCategoryById(long id)throws IllegalArgumentException {
		return categoryDAO.select(id);
	}

	@Override
	public List<Category> findAllCategories() {
		return categoryDAO.selectAll();
	}

	@Override
	public void deleteCategory(long id)throws IllegalArgumentException {
		categoryDAO.delete(id);
		
	}

	@Override
	public void updateCategory(Category category)throws IllegalArgumentException {
		if (category != null)
		    categoryDAO.update(category);
		else 
			throw new IllegalArgumentException("The given Category must not be null!");	
	}

	@Override
	public List<Movie> findAllMoviesByCategoryID(long id) {
		return categoryDAO.select(id).getMovies();
	}
}
