package movietracker.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movietracker.database.VoteDAO;
import movietracker.model.Movie;
import movietracker.model.User;
import movietracker.model.Vote;

/**
 * 
 * @author Nicolas Bruch
 *
 */

@Service("voteService")
public class VoteServiceImpl implements VoteService {

	@Autowired
	VoteDAO voteDAO;
	
	@Override
	public void addVote(Vote vote) throws IllegalArgumentException {
		if(vote != null){
			voteDAO.insert(vote);
		}else{
			throw new IllegalArgumentException("The given vote must not be null!");
		}
		
	}

	@Override
	public Vote findVoteById(Long id) throws IllegalArgumentException {
		if(id != null){
			return voteDAO.select(id);
		}else{
			throw new IllegalArgumentException("The given ID must not be null!");
		}
	}

	@Override
	public List<Vote> findAllVotes() {
		return voteDAO.selectAll();
	}

	@Override
	public void deleteVote(Long id) throws IllegalArgumentException {
		if(id!=null){
			voteDAO.delete(id);
		}else{
			throw new IllegalArgumentException("The given ID must not be null!");
		}
		
	}

	@Override
	public void updateVote(Vote vote) throws IllegalArgumentException {
		if(vote != null){
			voteDAO.update(vote);
		}else{
			throw new IllegalArgumentException("The given vote must not be null!");
		}
	}	
	
	@Override
	public List<Vote> selectAllVotesOfAUser(User user) {
		return voteDAO.selectAllVotesOfAUser(user);
	}	
	
	@Override
	public List<Vote> selectAllVotesOfAMovie(Movie movie) {
		return voteDAO.selectAllVotesOfAMovie(movie);
	}	

	@Override
	public float getCalculatedMovieRank(Movie movie) {
		List<Vote> votes =  voteDAO.selectAllVotesOfAMovie(movie);
		int voteSum = votes.parallelStream().mapToInt(v -> v.getRank()).sum();
		return (float) voteSum / votes.size();	
	}	

}
