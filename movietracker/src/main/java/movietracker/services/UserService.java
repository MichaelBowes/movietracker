package movietracker.services;
/**
 * @author Maximilian Kostial 
 */
import java.util.List;

import movietracker.model.User;

public interface UserService {

	/**
	 * This method inserts a given user to the database.
	 * 
	 * @param user
	 */
	public void addUser(User user)throws IllegalArgumentException ;

	/**
	 * 
	 * @param id
	 * @return
	 */
	public User findUserById(long id);
    /**
     * 
     * @param name
     * @return
     */
    public User findUserByUserName(String name);
	/**
	 * This method retrieves all users from the UserDAO. 
	 * 
	 * @return
	 */
	public List<User> findAllUsers();
   
	/**
     * This method deletes an user from the database.
     * @param id
     */
	public void deleteUserById(long id);
    
	/**
	 * This method updates a given user in the database.
	 * @param user
	 */
	public void updateUser(User user)throws IllegalArgumentException ;
  
	/**
	 * This method sets the user object for the session.
	 */
	public void setUserForSession();
	
	public User getUserForSession();
}
