package movietracker.services;
/**
 * @author Maximilian Kostial 
 */
import java.util.List;

import movietracker.model.Actor;

public interface ActorService {
	
	/**
	 * @param user
	 */
	public void addActor(Actor actor)throws IllegalArgumentException ;

	/**
	 * @param id
	 * @return
	 */
	public Actor findActorById(long id);
	
	public Actor findActorByName(String firstName, String lastName);

	/**
	 * @return
	 */
	public List<Actor> findAllActors();
   
	/**
     * @param id
     */
	public void deleteActorById(long id);
    
	/**
	 * @param Actor
	 */
	public void updateActor(Actor actor)throws IllegalArgumentException ;
}


