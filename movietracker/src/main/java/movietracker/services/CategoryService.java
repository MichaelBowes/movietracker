package movietracker.services;
/**
 * @author Michael Bowes 
 */
import java.util.List;

import movietracker.model.Category;
import movietracker.model.Movie;

public interface CategoryService {
	/**
	 * This method inserts a given category to the database.
	 * 
	 * @param category
	 */
	public void addCategory(Category category)throws IllegalArgumentException ;

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Category findCategoryById(long id)throws IllegalArgumentException ;

	/**
	 * This method retrieves all categories from the CategoryDAO. 
	 * 
	 * @return
	 */
	public List<Category> findAllCategories();
   
	/**
     * This method deletes a category from the database.
     * @param id
     */
	public void deleteCategory(long id)throws IllegalArgumentException ;
    
	/**
	 * This method updates a given category in the database.
	 * @param category
	 */
	public void updateCategory(Category category)throws IllegalArgumentException ;

	List<Movie> findAllMoviesByCategoryID(long id);
}
