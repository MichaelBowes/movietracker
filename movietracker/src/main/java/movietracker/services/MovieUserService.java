package movietracker.services;
/**
 * @author Maximilian Kostial
 */
import java.util.List;

import movietracker.model.MovieUser;

public interface MovieUserService {
	
	public MovieUser create(MovieUser movieUser);
	public MovieUser findMovieUserById(long userID, long movieID);
	public List<MovieUser> findMovieUserByUserId(long id);
	public List<MovieUser> findAll();
	public void deleteById(long userID, long movieID);
	public void updateMovieUser(MovieUser movieUser);
	
}
