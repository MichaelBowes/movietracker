package movietracker.services;
/**
 * @author Maximilian Kostial
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movietracker.database.MovieUserDAO;
import movietracker.database.UserDAO;
import movietracker.model.MovieUser;
import movietracker.model.User;

@Service("movieUserService")
public class MovieUserServiceImpl implements MovieUserService {
    @Autowired
	MovieUserDAO movieUserDAO;
    
	@Override
	public MovieUser create(MovieUser movieUser) {
		return this.movieUserDAO.insert(movieUser);
		
	}

	@Override
	public MovieUser findMovieUserById(long userID, long movieID) {
		return this.movieUserDAO.select(userID, movieID);
	}

	@Override
	public List<MovieUser> findAll() {
		return this.movieUserDAO.selectAll();
	}

	@Override
	public void deleteById(long userID, long movieID) {
		this.movieUserDAO.delete(userID, movieID);
		
	}

	@Override
	public void updateMovieUser(MovieUser movieUser) {
		this.movieUserDAO.update(movieUser);
		
	}

	@Override
	public List<MovieUser> findMovieUserByUserId(long id) {
		return this.movieUserDAO.selectByUserId(id);
	}


}
