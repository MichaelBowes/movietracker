package movietracker.services;
/**
 * @author Maximilian Kostial
 */

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import movietracker.database.UserDAO;
import movietracker.model.User;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
  
   @Autowired
   private UserDAO userDAO; 

	@Override
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
		User user = null;
		try {
		  user = userDAO.selectUserByName(name);
		} catch (NoResultException e ){
			throw new UsernameNotFoundException("User " + name + " not found!");
		}
		      return user;
	}

	
 }

