package movietracker.services;

import java.util.List;

import movietracker.model.Movie;
import movietracker.model.User;
import movietracker.model.Vote;

/**
 * 
 * @author Nicolas Bruch
 *
 */
public interface VoteService {

	/**
	 * 
	 * @param vote
	 */
	public void addVote(Vote vote)throws IllegalArgumentException ;

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Vote findVoteById(Long id)throws IllegalArgumentException ;

	/**
	 * This method retrieves all votes from the VoteDAO. 
	 * 
	 * @return
	 */
	public List<Vote> findAllVotes();
   
	/**
     * This method deletes an vote from the database.
     * @param id
     */
	public void deleteVote(Long id)throws IllegalArgumentException ;
    
	/**
	 * This method updates a given vote in the database.
	 * @param vote
	 */
	public void updateVote(Vote vote)throws IllegalArgumentException ;

	public List<Vote> selectAllVotesOfAMovie(Movie movie);

	public List<Vote> selectAllVotesOfAUser(User user);

	public float getCalculatedMovieRank(Movie movie);
}


