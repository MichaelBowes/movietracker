INSERT INTO `director` (`directorID`, `firstName`, `lastName`) VALUES
(1, 'Robert', 'Rodriguez'),
(2, 'Alfonso', 'Cuar�n'),
(3, 'Mike', 'Newell'),
(4, 'James', 'Watkins'),
(5, 'J.J.', 'Abrams'),
(6, 'George', 'Lucas'),
(7, 'Irvin', 'Kershner'),
(8, 'Richard', 'Marquand'),
(9, 'Duke', 'Johnson'),
(10, 'Franklin', 'Schaffner'),
(11, 'Christopher', 'Nolan');

INSERT INTO `actor` (`actorID`, `firstName`, `lastName`) VALUES
(1, 'George', 'Clooney'),
(2, 'Harvey', 'Keitel'),
(3, 'Juliette', 'Lewis'),
(4, 'Sandra', 'Bullock'),
(5, 'Daniel', 'Radcliffe'),
(6, 'Emma', 'Watson'),
(7, 'Rupert', 'Grint'),
(8, 'Janet', 'McTeer'),
(9, 'Daisy', 'Ridley'),
(10, 'John', 'Boyega'),
(11, 'Oscar', 'Isaac'),
(12, 'Mark', 'Hamill'),
(13, 'Harrison', 'Ford'),
(14, 'Carrie', 'Fisher'),
(15, 'Ewan', 'McGregor'),
(16, 'Liam', 'Neeson'),
(17, 'Natalie', 'Portman'),
(18, 'Hayden', 'Christensen'),
(19, 'Jennifer', 'Aniston'),
(20, 'Charlton', 'Heston'),
(21, 'Kim', 'Hunter'),
(22, 'Christian', 'Bale'),
(23, 'Michael', 'Caine'),
(24, 'Ken', 'Watanabe'),
(25, 'Heath', 'Ledger'),
(26, 'Aaron', 'Eckhart'),
(27, 'Maggie', 'Gyllenhaal'),
(28, 'Tom', 'Hardy'),
(29, 'Anne', 'Hathaway');

INSERT INTO `user` (`userID`, `userName`, `userEmail`, `userLastLogin`, `isAdmin`, `password`) VALUES
(1, 'Michael', 'michael.bowes@stud.uni-bamberg.de', '2016-12-03', true, '123'),
(2, 'Peter', 'peterDummy@web.de', '2016-12-07', false, '12345'),
(3, 'test', 'test@test.de', '2016-12-08', true, 'test');

INSERT INTO `movieactors` VALUES (1, 1);
INSERT INTO `movieactors` VALUES (2, 1);
INSERT INTO `movieactors` VALUES (3, 1);
INSERT INTO `movieactors` VALUES (1, 2);
INSERT INTO `movieactors` VALUES (4, 2);
INSERT INTO `movieactors` VALUES (5, 3);
INSERT INTO `movieactors` VALUES (6, 3);
INSERT INTO `movieactors` VALUES (7, 3);
INSERT INTO `movieactors` VALUES (5, 4);
INSERT INTO `movieactors` VALUES (6, 4);
INSERT INTO `movieactors` VALUES (7, 4);
INSERT INTO `movieactors` VALUES (5, 5);
INSERT INTO `movieactors` VALUES (8, 5);
INSERT INTO `movieactors` VALUES (9, 6);
INSERT INTO `movieactors` VALUES (10, 6);
INSERT INTO `movieactors` VALUES (11, 6);
INSERT INTO `movieactors` VALUES (12, 7);
INSERT INTO `movieactors` VALUES (13, 7);
INSERT INTO `movieactors` VALUES (14, 7);
INSERT INTO `movieactors` VALUES (12, 8);
INSERT INTO `movieactors` VALUES (13, 8);
INSERT INTO `movieactors` VALUES (14, 8);
INSERT INTO `movieactors` VALUES (12, 9);
INSERT INTO `movieactors` VALUES (13, 9);
INSERT INTO `movieactors` VALUES (14, 9);
INSERT INTO `movieactors` VALUES (15, 10);
INSERT INTO `movieactors` VALUES (16, 10);
INSERT INTO `movieactors` VALUES (17, 10);
INSERT INTO `movieactors` VALUES (17, 11);
INSERT INTO `movieactors` VALUES (18, 11);
INSERT INTO `movieactors` VALUES (20, 13);
INSERT INTO `movieactors` VALUES (21, 13);
INSERT INTO `movieactors` VALUES (22, 14);
INSERT INTO `movieactors` VALUES (23, 14);
INSERT INTO `movieactors` VALUES (24, 14);
INSERT INTO `movieactors` VALUES (22, 15);
INSERT INTO `movieactors` VALUES (25, 15);
INSERT INTO `movieactors` VALUES (26, 15);
INSERT INTO `movieactors` VALUES (27, 15);
INSERT INTO `movieactors` VALUES (23, 15);
INSERT INTO `movieactors` VALUES (22, 16);
INSERT INTO `movieactors` VALUES (28, 16);
INSERT INTO `movieactors` VALUES (29, 16);

INSERT INTO `category` VALUES (1, 'Action', true);
INSERT INTO `category` VALUES (2, 'Crime', true);
INSERT INTO `category` VALUES (3, 'Fantasy', true);
INSERT INTO `category` VALUES (4, 'Adventure', true);
INSERT INTO `category` VALUES (5, 'Drama', true);
INSERT INTO `category` VALUES (6, 'Sci-Fi', true);
INSERT INTO `category` VALUES (7, 'Family', true);
INSERT INTO `category` VALUES (8, 'Horror', true);
INSERT INTO `category` VALUES (9, 'Animation', true);
INSERT INTO `category` VALUES (10, 'Documentary', true);
INSERT INTO `category` VALUES (11, 'Thriller', true);
INSERT INTO `category` VALUES (12, 'Comedy', true);
INSERT INTO `category` VALUES (13, 'Anime', false);

INSERT INTO `movie` VALUES('1', 'From Dusk Till Dawn', '<p>In this action-horror flick from director Robert Rodriguez and screenwriter Quentin Tarantino, Tarantino stars with George Clooney as a pair of bad-to-the-bone brothers named Seth and Richie Gecko.</p><p> After a string of robberies that left a river of blood in the Geckos'' wake, the sadistic siblings head to Mexico to live the good life. To get over the border, they kidnap Jacob Fuller, a widowed preacher played by Harvey Keitel, and his two children, Kate (Juliette Lewis) and Scott (Ernest Liu). Once south of the border, the quintet park their RV at a rough-and-tumble trucker bar called The <em>Titty Twister</em>, where Seth and Richie are supposed to meet a local thug.</p><p> After a couple of drinks, they realize that they''re not in a typical bar, as the entire place begins to teem with vicious, blood-sucking vampires. With the odds stacked greatly against them, the Fullers and Geckos team together in hopes of defeating the creatures of the night. Makeup artist Tom Savini and blaxploitation star Fred Williamson appear as allies against the vampires, and Cheech Marin fills three different roles.</p>', '108', '16', '4. Juli 1996', 1, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('2', 'Gravity', '<p>Director Alfonso Cuaron''s Gravity stars Sandra Bullock as Dr. Ryan Stone, a scientist on a space shuttle mission headed by astronaut Matt Kowalsky (George Clooney), a talkative, charismatic leader full of colorful stories that he shares with his crewmates as well as mission control.</p><p> As the two are on a space walk, debris hits the area where they are working, and soon the pair finds themselves detached from their ship and stranded in space. While figuring out what steps they can take to save themselves, Stone grapples with a painful past that makes her consider giving up altogether.</p> <p>Gravity screened at the 2013 Toronto International Film Festival.</p>', '91', '12', '03.10.2013', 2, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('3', 'Harry Potter and the Prisoner of Azkaban', '<p>After directing the first two movies in the Harry Potter franchise, Chris Columbus opted to serve as producer for Harry Potter and the Prisoner of Azkaban, and passed the baton to Y Tu Mam� Tambi�n director Alfonso Cuar�n. Though "immensely popular" is an understatement when it comes to Harry Potter, Azkaban is somewhat of a departure from its predecessors, and particularly beloved among fans for its surprise ending. </p> <p>Prisoner of Azkaban also marks the introduction of Sirius Black (Gary Oldman), who has escaped from the title prison after 12 years of incarceration. Believed to have been the right-hand-man of the dark wizard Voldemort, whom Harry (Daniel Radcliffe) mysteriously rendered powerless during his infancy, some of those closest to Harry suspect Black has returned to exact revenge on the boy who defeated his master. </p> <p>Upon his return to school, however, Harry is relatively unconcerned with Black. Run by Albus Dumbledore (Michael Gambon) -- who is widely regarded as the most powerful wizard of the age -- Hogwarts is renowned for its safety. Harry''s nonchalance eventually turns to blind rage after accidentally learning the first of Black''s many secrets during a field trip to a neighboring village. Of course, a loose serial killer is only one of the problems plaguing the bespectacled wizard''s third year back at school -- the soul-sucking guards of Azkaban prison have been employed at Hogwarts to protect the students, but their mere presence sends Harry into crippling fainting spells. With the help of his friends Ron (Rupert Grint) and Hermione (Emma Watson), and Defense Against the Dark Arts professor Remus Lupin (David Thewlis), Harry struggles to thwart the Dementors, find Sirius Black, and uncover the mysteries of the night that left him orphaned.</p>', '142', '12', '03.06.2004', '2', 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('4', 'Harry Potter and the Goblet of Fire', '<p>Directed by Mike Newell, the fourth installment to the Harry Potter series finds Harry (Daniel Radcliffe) wondering why his legendary scar -- the famous result of a death curse gone wrong -- is aching in pain, and perhaps even causing mysterious visions. </p> <p> Before he can think too much about it, however, Harry boards the train to Hogwarts School of Witchcraft and Wizardry, where he will attend his fourth year of magical education. Shortly after his reunion with his best friends, Ron (Rupert Grint) and Hermione (Emma Watson), Harry is introduced to yet another Defense Against the Dark Arts teacher: the grizzled Mad-Eye Moody (Brendan Gleeson), a former dark wizard catcher who agreed to take on the infamous <em>DADA</em> professorship as a personal favor to Headmaster Dumbledore (Michael Gambon). </p><p>Of course, Harry''s wishes for an uneventful school year are almost immediately shattered when he is unexpectedly chosen, along with fellow student Cedric Diggory (Robert Pattinson), as Hogwarts'' representative in the Tri-Wizard Tournament, which awards whoever completes three magical tasks the most skillfully with a thousand-galleon purse and the admiration of the international wizard community. </p><p>As difficult as it is to deal with his schoolwork, friendships, and the tournament at the same time (not to mention his feelings toward the ever unfathomable Professor Snape (Alan Rickman), Harry doesn''t realize that the most feared wizard in the world, Lord Voldemort, is anticipating the tournament, as well.</p>', '157', '12', '16.11.2005', 3, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('5', 'The Woman in Black', '<p>A widowed lawyer travels to a secluded village on an important assignment, and encounters a vengeful ghost with mysterious motives. After losing his beloved wife in childbirth, young barrister Arthur Kipps (Daniel Radcliffe) was nearly consumed by grief. A haunted widower father, he raises his young son with the help of his devoted nanny. </p> <p>Arthur is on the verge of losing his job when an important client of the firm dies, and his boss offers him one last opportunity to prove his worth by settling the woman''s affairs. Determined to succeed, Arthur travels to the remote village and receives a chilly welcome. Something horrible once happened here, and it seems that the locals are determined to ensure Arthur never finds out what it was. Now, the more time Arthur spends in his client''s crumbling estate, the more aware he becomes of a presence that isn''t quite human. In this house dwells a woman''s ghost. In life she lost something precious, and now in death she''ll do whatever it takes to get it back. Until she does, her spectral presence will serve as a harbinger of doom, always to be followed by the death of an innocent.</p>', '95', '16', '29. M�rz 2012', 4, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('6', 'Star Wars: Episode VII - The Force Awakens', '<p>In this thrilling continuation of the epic space opera, ex-stormtrooper Finn (John Boyega), scrappy desert dweller Rey (Daisy Ridley), and droid companion <em>BB-8</em> get caught up in a galactic war when they come across a map containing the whereabouts of the vanished Luke Skywalker (Mark Hamill). They soon embark on an epic adventure that brings them face-to-face with heroes from the past, as well as new villains such as Kylo Ren (Adam Driver) and the First Order, a fascist regime that has emerged from the ashes of the old Empire. </p><p>Carrie Fisher, Harrison Ford, Peter Mayhew, and Anthony Daniels return to reprise their roles as, respectively, Leia Organa, Han Solo, Chewbacca, and <em>C-3PO</em>. Oscar Isaac, Lupita Nyong''o, Andy Serkis, Domhnall Gleeson, and Gwendoline Christie co-star. J.J. Abrams directed from a script he co-wrote with Lawrence Kasdan and Michael Arndt. The film also features new contributions from composer John Williams and sound designer Ben Burtt, whose familiar palette of sounds have helped define the Star Wars universe.</p>', '138', '12', '17.12.2015', 5, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('7', 'Star Wars', '<p>George Lucas'' mythological popcorn movie is a two-hour roller-coaster ride that has passed into movie legend. The story, for the tiny number of people not familiar with it, concerns a farm boy named Luke Skywalker (Mark Hamill) who discovers that the used robot recently purchased by his family plays back a message from one Princess Leia (Carrie Fisher), begging for help from Obi-Wan Kenobi. Luke asks his father''s friend Ben Kenobi (Alec Guinness) about this, and he discovers that Ben and Obi-Wan are one and the same. </p>   <p>Kenobi tells Luke of the battle of the rebels against the ruling Empire and the spiritual energy called "The Force." Soon Luke, Kenobi, and a mercenary named Han Solo (Harrison Ford) join forces to rescue Princess Leia from the Empire''s mammoth warship, the Death Star, controlled by evil genius Darth Vader (David Prowse, with the voice of James Earl Jones).</p> <p>George Lucas has frequently cited the influence of several films on Star Wars, particularly Akira Kurosawa''s The Hidden Fortress and Yojimbo and John Ford''s The Searchers, as well as the original Flash Gordon serials. After Star Wars became a success, Lucas announced his intention to turn the film into a series, originally totalling nine films (later pared back to six). Consequently, most reissue prints now feature the title Star Wars: Episode IV -- A New Hope, with The Empire Strikes Back (1980) and Return of the Jedi (1983) serving as Episodes Five and Six in the serial, and Star Wars: Episode I -- The Phantom Menace (1999) going back to the myth''s beginnings.</p>', '121', '12', '09.02.1978', 6, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('8', 'Star Wars: Episode V - The Empire Strikes Back', '<p>The second entry in George Lucas'' Star Wars trilogy finds Luke Skywalker (Mark Hamill), the green-as-grass hero from the first film, now a seasoned space warrior. Luke''s Star Wars cohorts Han Solo (Harrison Ford) and Princess Leia (Carrie Fisher) are likewise more experienced in the ways and means of battling the insidious Empire, as represented by the brooding Darth Vader (body of David Prowse, voice of James Earl Jones). And, of course, "The Force," personified by the ghost of Luke''s mentor Ben Kenobi (Alec Guinness), is with them all. </p> <p>Retreating from Vader''s minions, Luke ends up, at first, on the Ice Planet Hoth, and then the tropical Dagobah. Here he makes the acquaintance of the gnomish Yoda (voice of Frank Oz), whose all-encompassing wisdom comes in handy during the serial-like perils of the rest of the film. Before the film''s open-ended climax, we are introduced to the apparently duplicitous Lando Calrissian (Billy Dee Williams) and are let in on a secret that profoundly affects both Luke and his arch-enemy, Vader. </p>   <p>Many viewers consider this award-winning film the best of the Star Wars movies, and its special-effects bonanza was pure gold at the box office.</p>', '124', '6', '11.12.1980', 7, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('9', 'Star Wars: Episode VI - Return of the Jedi', '<p>In the final episode of the Star Wars saga, Han Solo (Harrison Ford) emerges intact from the carbonite casing in which he''d been sealed in The Empire Strikes Back. The bad news is that Solo, together with Luke Skywalker (Mark Hamill) and Princess Leia (Carrie Fisher), is prisoner to the grotesque Jabba the Hutt. But with the help of the charismatic Lando Calrissian (Billy Dee Williams), our heroes and our heroine manage to escape. </p>    <p>The next task is to rid the galaxy of Darth Vader (body by David Prowse, voice by James Earl Jones) and the Emperor (Ian McDiarmid), now in command of a new, under-construction Death Star. On the forest moon Endor, the good guys enlist the help of a feisty bunch of bear-like creatures called the Ewoks in their battle against the Empire.</p>', '131', '12', '09.12.1983', 8, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('10', 'Star Wars: Episode I - The Phantom Menace', '<p>In 1977, George Lucas released Star Wars, the ultimate sci-fi popcorn flick-turned-pop-culture myth machine. It quickly became the biggest money-making film of all time and changed the shape of the film industry. After two successful sequels (1980''s The Empire Strikes Back and 1983''s Return Of The Jedi) that extended the story of the first film, Lucas took some time off to produce movies for others, with mixed success. </p> <p>In 1999, Lucas returned to the Star Wars saga with a new approach -- instead of picking up where Return Of The Jedi left off, Star Wars: Episode I -- The Phantom Menace would be the first of a trilogy of stories to trace what happened in the intergalactic saga before the first film began. </p>    <p>Here, Obi-Wan Kenobi (Ewan McGregor) is a young apprentice Jedi knight under the tutelage of Qui-Gon Jinn (Liam Neeson); Anakin Skywalker (Jake Lloyd), who will later father Luke Skywalker and become known as Darth Vader, is just a nine-year-old boy. When the Trade Federation cuts off all routes to the planet Naboo, Qui-Gon and Obi-Wan are assigned to settle the matter, but when they arrive on Naboo they are brought to Amidala (Natalie Portman), the Naboo queen, by a friendly but opportunistic Gungan named Jar Jar. </p> <p>Qui-Gon and Obi-Wan plan to escort Amidala to a meeting of Republic leaders in Coruscant, but trouble with their spacecraft strands them on the planet Tatooine, where Qui-Gon meets Anakin, the slave of a scrap dealer. Qui-Gon is soon convinced that the boy could be the leader the Jedis have been searching for, and he begins bargaining for his freedom and teaching the boy the lessons of the Force. </p>  <p>The supporting cast includes Pernilla August as Anakin''s mother, Terence Stamp as Chancellor Valorum, and Samuel L. Jackson as Jedi master Mace Windu. Jackson told a reporter before The Phantom Menace''s release that the best part about doing the film was that he got to say "May the Force be with you" onscreen.</p>', '136', '6', '19. August 1999', 6, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('11', 'Star Wars: Episode II - Attack of the Clones', '<p>The second prequel to the original Star Wars trilogy takes place ten years after the events depicted in Star Wars: Episode I -- The Phantom Menace. Now 20, young Anakin Skywalker (Hayden Christensen) is an apprentice to respected Jedi knight Obi-Wan Kenobi (Ewan McGregor). Unusually powerful in the Force, Anakin is also impatient, arrogant, and headstrong -- causing his mentor a great deal of concern. </p> <p>The pair are ordered to protect Padme Amidala (Natalie Portman), the former queen of the planet Naboo, now representing her world in the Galactic Senate. Someone is trying to assassinate her on the eve of a vote enabling Supreme Chancellor Palpatine (Ian McDiarmid) to build a military force that will safeguard against a growing separatist movement led by mysterious former Jedi Count Dooku (Christopher Lee). After another attempt on Padme''s life, Obi-Wan and Anakin separate.</p> <p>The young Jedi and Padme fall in love as he escorts her first to the security of Naboo and then to his home world of Tatooine, where the fate of his mother leads him to commit an ominous atrocity. </p><p>Meanwhile, Obi-Wan travels to the secretive planet Kamino and the asteroid-ringed world of Geonosis, following bounty hunter Jango Fett (Temuera Morrison) and his son, Boba (Daniel Logan), who are involved in an operation to create a massive army of clones.</p><p>A vicious battle ensues between the clones and Jedi on one side and Dooku''s droids on the other, but who is really pulling the strings in this galactic conflict? </p>   <p>In late 2002, the movie was released in IMAX theaters as Star Wars: Episode II -- Attack of the Clones: The IMAX Experience, with a pared-down running time of 120 minutes in order to meet the technical requirements of the large-screen format.</p>', '142', '12', '16.05.2002', 6, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('12', 'Anomalisa', '<p>A lonely motivational speaker goes on a business trip to Cincinnati, where he meets an extraordinary stranger who might be able to change his negative view of life.</p><p> This stop-motion-animation feature was penned by lauded screenwriter Charlie Kaufman, who also co-directs alongside Duke Johnson. David Thewlis, Tom Noonan, and Jennifer Jason Leigh provide their voices.</p>', '90', '12', '21. Januar 2016', 9, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('13', 'Planet of the Apes', '<p> Originally intended as a project for Blake Edwards, the film version of Pierre Boule''s semisatiric sci-fi novel came to the screen in 1968 under the directorial guidance of Franklin J. Schaffner. Charlton Heston is George Taylor, one of several astronauts on a long space mission whose spaceship crash-lands on a remote planet, seemingly devoid of intelligent life. Soon the astronaut learns that this planet is ruled by a race of talking, thinking, reasoning apes who hold court over a complex, multilayered civilization. </p><p>In this topsy-turvy society, the human beings are grunting, inarticulate primates, penned-up like animals. When ape leader Dr. Zaius (Maurice Evans) discovers that the captive Taylor has the power of speech, he reacts in horror and insists that the astronaut be killed. But sympathetic ape scientists Cornelius (Roddy McDowell) and Dr. Zira (Kim Hunter) risk their lives to protect Taylor -- and to discover the secret of their planet''s history that Dr. Zaius and his minions guard so jealously. </p> <p>In the end, it is Taylor who stumbles on the truth about the Planet of the Apes: "Damn you! Damn you! Goddamn you all to hell!" </p> <p>Scripted by Rod Serling and Michael Wilson (a former blacklistee who previously adapted another Pierre Boule novel, Bridge on the River Kwai), Planet of the Apes has gone on to be an all-time sci-fi (and/or camp) classic. It won a special Academy Award for John Chambers''s convincing (and, from all accounts, excruciatingly uncomfortable) simian makeup. It spawned four successful sequels, as well as two TV series, one live-action and one animated.</p>', '112', '12', '03.05.1968', 10, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('14', 'Batman Begins', '<p> The origins of the Caped Crusader of Gotham City are finally brought to the big screen in this new adaptation of the perennially popular comic-book series. The young Bruce Wayne (Gus Lewis) leads a privileged life as the son of wealthy, philanthropist parents, both of whom stress their commitment to improving the lives of the citizens of crime-ridden Gotham City.</p><p> After his mother and father are murdered by a mugger, however, Wayne grows into an impudent young man (Christian Bale), full of rage and bent on retribution until encouraged by his childhood sweetheart, Rachel Dawes (Katie Holmes), to search for answers beyond his own personal vendettas. </p><p> Wayne eventually finds discipline in the Far East under the tutelage of Henri Ducard (Liam Neeson), a member of the mysterious <em>League of Shadows</em> who guides him in the study of martial arts -- and the ways in which an ordinary man can hone his senses to an almost superhuman acuity. </p><p> After seven years away from Gotham, Wayne returns, determined to bring peace and safety back to the city. With the help of his faithful manservant, Alfred Pennyworth (Michael Caine), and Lucius Fox (Morgan Freeman), a scientist at his late father''s corporation, Wayne develops a secret identity as Batman, a masked fighter for justice. But when a shady psychiatrist (Cillian Murphy) joins forces with the criminal underworld, Wayne realizes that putting an end to their nefarious plans will be very difficult indeed. Batman Begins also features Gary Oldman as Lt. James Gordon and Tom Wilkinson as the crime boss Carmine Falcone.</p>', '140', '12', '16. Juni 2005', 11, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('15', 'The Dark Knight', '<p>Christopher Nolan steps back into the director''s chair for this sequel to Batman Begins, which finds the titular superhero coming face to face with his greatest nemesis -- the dreaded Joker. Christian Bale returns to the role of Batman, Maggie Gyllenhaal takes over the role of Rachel Dawes (played by Katie Holmes in Batman Begins), and Brokeback Mountain star Heath Ledger dons the ghoulishly gleeful Joker makeup previously worn by Jack Nicholson and Cesar Romero. </p> <p>Just as it begins to appear as if Batman, Lt. James Gordon (Gary Oldman), and District Attorney Harvey Dent (Aaron Eckhart) are making headway in their tireless battle against the criminal element, a maniacal, wisecracking fiend plunges the streets of Gotham City into complete chaos.</p>', '152', '16', '21.08.2009', 11, 'Andreas Henrich', 'APPROVED', 4, 2);
INSERT INTO `movie` VALUES('16', 'The Dark Knight Rises', '<p>Christopher Nolan''s Batman trilogy concludes with this Warner Brothers release that finds The Dark Knight pitted against Bane, an unstoppable foe possessed of tremendous physical and intellectual strength. Nearly a decade after taking the fall for Harvey Dent''s death and disappearing into the darkness, a fugitive Batman (Christian Bale) watches from the shadows as the Dent Act keeps the streets of Gotham City free of crime. </p><p>Meanwhile, an elusive cat burglar seizes the chance to strike, and a masked anarchist plots a devastating series of attacks designed to lure Bruce Wayne out of the shadows. Determined not to abandon the people who he once risked his life to protect, The Dark Knight emerges from his self-imposed exile ready to fight. But Bane (Tom Hardy) is ready, too, and once Batman is within his grasp, he will do everything in his power to break Gotham City''s shadowy savior. </p><p> Oscar-winner Michael Caine and Gary Oldman return in a sequel also starring Anne Hathaway, Marion Cotillard, and Joseph Gordon-Levitt.</p>', '164', '12', '21.07.2012', 11, 'Andreas Henrich', 'APPROVED', 4, 2);

INSERT INTO `moviecategories` VALUES (1, 1);
INSERT INTO `moviecategories` VALUES (1, 2);
INSERT INTO `moviecategories` VALUES (1, 8);
INSERT INTO `moviecategories` VALUES (2, 5);
INSERT INTO `moviecategories` VALUES (2, 4);
INSERT INTO `moviecategories` VALUES (2, 6);
INSERT INTO `moviecategories` VALUES (3, 4);
INSERT INTO `moviecategories` VALUES (3, 7);
INSERT INTO `moviecategories` VALUES (3, 3);
INSERT INTO `moviecategories` VALUES (4, 4);
INSERT INTO `moviecategories` VALUES (4, 7);
INSERT INTO `moviecategories` VALUES (4, 3);
INSERT INTO `moviecategories` VALUES (5, 8);
INSERT INTO `moviecategories` VALUES (5, 5);
INSERT INTO `moviecategories` VALUES (5, 3);
INSERT INTO `moviecategories` VALUES (6, 1);
INSERT INTO `moviecategories` VALUES (6, 4);
INSERT INTO `moviecategories` VALUES (6, 3);
INSERT INTO `moviecategories` VALUES (7, 1);
INSERT INTO `moviecategories` VALUES (7, 4);
INSERT INTO `moviecategories` VALUES (7, 3);
INSERT INTO `moviecategories` VALUES (8, 1);
INSERT INTO `moviecategories` VALUES (8, 4);
INSERT INTO `moviecategories` VALUES (8, 3);
INSERT INTO `moviecategories` VALUES (9, 1);
INSERT INTO `moviecategories` VALUES (9, 4);
INSERT INTO `moviecategories` VALUES (9, 3);
INSERT INTO `moviecategories` VALUES (10, 1);
INSERT INTO `moviecategories` VALUES (10, 4);
INSERT INTO `moviecategories` VALUES (10, 3);
INSERT INTO `moviecategories` VALUES (11, 1);
INSERT INTO `moviecategories` VALUES (11, 4);
INSERT INTO `moviecategories` VALUES (11, 3);
INSERT INTO `moviecategories` VALUES (12, 9);
INSERT INTO `moviecategories` VALUES (12, 5);
INSERT INTO `moviecategories` VALUES (12, 12);
INSERT INTO `moviecategories` VALUES (13, 1);
INSERT INTO `moviecategories` VALUES (13, 4);
INSERT INTO `moviecategories` VALUES (13, 6);
INSERT INTO `moviecategories` VALUES (14, 1);
INSERT INTO `moviecategories` VALUES (14, 4);
INSERT INTO `moviecategories` VALUES (15, 1);
INSERT INTO `moviecategories` VALUES (15, 2);
INSERT INTO `moviecategories` VALUES (15, 5);
INSERT INTO `moviecategories` VALUES (16, 1);
INSERT INTO `moviecategories` VALUES (16, 11);


INSERT INTO `moviecomments` VALUES (1, 1);

INSERT INTO `movieUser` VALUES (1, 1, true, '2016-12-12 13:00:00', true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (2, 1, true, '2016-12-12 13:00:00', true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (3, 1, true, '2016-12-12 13:00:00', true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (4, 1, false, NULL, true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (5, 1, true, '2016-12-10 13:00:00', false, NULL );
INSERT INTO `movieUser` VALUES (6, 1, true, '2016-12-12 13:00:00', true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (7, 1, true, '2016-12-12 13:00:00', true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (1, 2, false, '2016-12-10 13:00:00', true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (2, 2, true, '2016-12-12 13:00:00', true, '2016-11-10 14:00:00');
INSERT INTO `movieUser` VALUES (8, 2, true, '2016-12-10 13:00:00', false, NULL );
INSERT INTO `movieUser` VALUES (2, 3, true, '2016-12-12 13:00:00', true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (3, 3, true, '2016-12-12 13:00:00', true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (4, 3, false, NULL, true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (5, 3, true, '2016-12-10 13:00:00', false, NULL );
INSERT INTO `movieUser` VALUES (6, 3, true, '2016-05-12 13:00:00', true, '2016-11-11 14:00:00');
INSERT INTO `movieUser` VALUES (7, 3, true, '2016-09-12 13:00:00', true, '2016-11-11 14:00:00');

INSERT INTO `moviedetails` VALUES (1, 1, 1,'From Dusk To Dusk', '<p>In this action-horror flick from director Robert Rodriguez and screenwriter Quentin Tarantino, Tarantino stars with George Clooney as a pair of bad-to-the-bone brothers named Seth and Richie Gecko.</p><p> After a string of robberies that left a river of blood in the Geckos'' wake, the sadistic siblings head to Mexico to live the good life. To get over the border, they kidnap Jacob Fuller, a widowed preacher played by Harvey Keitel, and his two children, Kate (Juliette Lewis) and Scott (Ernest Liu). Once south of the border, the quintet park their RV at a rough-and-tumble trucker bar called The <em>Titty Twister</em>, where Seth and Richie are supposed to meet a local thug.</p><p> After a couple of drinks, they realize that they''re not in a typical bar, as the entire place begins to teem with vicious, blood-sucking vampires. With the odds stacked greatly against them, the Fullers and Geckos team together in hopes of defeating the creatures of the night. Makeup artist Tom Savini and blaxploitation star Fred Williamson appear as allies against the vampires, and Cheech Marin fills three different roles.</p>', '16', '4. Juli 1996','Udo Wild', 'Andreas Henrich', 'PENDING',30);
INSERT INTO `moviedetails` VALUES (2, 1, 1,'From Dusk To Tusk', '<p>In this action-horror flick they kidnap Jacob Fuller, a widowed preacher played by Harvey Keitel, and his two children, Kate (Juliette Lewis) and Scott (Ernest Liu). Once south of the border, the quintet park their RV at a rough-and-tumble trucker bar called The <em>Titty Twister</em>, where Seth and Richie are supposed to meet a local thug.</p><p> After a couple of drinks, they realize that they''re not in a typical bar, as the entire place begins to teem with vicious, blood-sucking vampires. With the odds stacked greatly against them, the Fullers and Geckos team together in hopes of defeating the creatures of the night. Makeup artist Tom Savini and blaxploitation star Fred Williamson appear as allies against the vampires, and Cheech Marin fills three different roles.</p>', '16', '4. Juli 1996', 'Hans Peter', 'Andreas Henrich', 'PENDING',30);
INSERT INTO `moviedetails` VALUES (3, 2, 1,'From Dusk Till Dawn 2', '<p>In this action-horror flick from director Robert Rodriguez and screenwriter Quentin Tarantino, Tarantino stars with George Clooney as a pair of bad-to-the-bone brothers named Seth and Richie Gecko.</p><p> After a string of robberies that left a river of blood in the Geckos'' wake, the sadistic siblings head to Mexico to live the good life. To get over the border, they kidnap Jacob Fuller, a widowed preacher played by Harvey Keitel, and his two children, Kate (Juliette Lewis) and Scott (Ernest Liu). Once south of the border, the quintet park their RV at a rough-and-tumble trucker bar called The <em>Titty Twister</em>, where Seth and Richie are supposed to meet a local thug.</p><p> After a couple of drinks, they realize that they''re not in a typical bar, as the entire place begins to teem with vicious, blood-sucking vampires. With the odds stacked greatly against them, the Fullers and Geckos team together in hopes of defeating the creatures of the night. Makeup artist Tom Savini and blaxploitation star Fred Williamson appear as allies against the vampires, and Cheech Marin fills three different roles.</p>', '16', '4. Juli 1996','Udo Wild', 'Andreas Henrich','PENDING',50);
INSERT INTO `moviedetails` VALUES (4, 3, 1, 'From Dusk Till Dawn 3', '<p>In this action-horror flick from director Robert Rodriguez and screenwriter Quentin Tarantino, Tarantino stars with George Clooney as a pair of bad-to-the-bone brothers named Seth and Richie Gecko.</p><p> After a string of robberies that left a river of blood in the Geckos'' wake, the sadistic siblings head to Mexico to live the good life. To get over the border, they kidnap Jacob Fuller, a widowed preacher played by Harvey Keitel, and his two children, Kate (Juliette Lewis) and Scott (Ernest Liu). Once south of the border, the quintet park their RV at a rough-and-tumble trucker bar called The <em>Titty Twister</em>, where Seth and Richie are supposed to meet a local thug.</p><p> After a couple of drinks, they realize that they''re not in a typical bar, as the entire place begins to teem with vicious, blood-sucking vampires. With the odds stacked greatly against them, the Fullers and Geckos team together in hopes of defeating the creatures of the night. Makeup artist Tom Savini and blaxploitation star Fred Williamson appear as allies against the vampires, and Cheech Marin fills three different roles.</p>', '16', '4. Juli 1996','Udo Wild', 'Andreas Henrich', 'PENDING',40);

INSERT INTO `moviedetailscategories` VALUES (1, 1);
INSERT INTO `moviedetailscategories` VALUES (1, 2);
INSERT INTO `moviedetailscategories` VALUES (1, 8);
INSERT INTO `moviedetailscategories` VALUES (2, 5);
INSERT INTO `moviedetailscategories` VALUES (2, 4);
INSERT INTO `moviedetailscategories` VALUES (2, 6);
INSERT INTO `moviedetailscategories` VALUES (3, 4);
INSERT INTO `moviedetailscategories` VALUES (3, 7);
INSERT INTO `moviedetailscategories` VALUES (3, 3);
INSERT INTO `moviedetailscategories` VALUES (4, 4);
INSERT INTO `moviedetailscategories` VALUES (4, 7);
INSERT INTO `moviedetailscategories` VALUES (4, 3);

INSERT INTO `moviedetailsactors` VALUES (1, 1);
INSERT INTO `moviedetailsactors` VALUES (2, 1);
INSERT INTO `moviedetailsactors` VALUES (3, 1);
INSERT INTO `moviedetailsactors` VALUES (1, 2);
INSERT INTO `moviedetailsactors` VALUES (4, 2);
INSERT INTO `moviedetailsactors` VALUES (5, 3);
INSERT INTO `moviedetailsactors` VALUES (6, 3);
INSERT INTO `moviedetailsactors` VALUES (7, 3);
INSERT INTO `moviedetailsactors` VALUES (5, 4);
INSERT INTO `moviedetailsactors` VALUES (6, 4);
INSERT INTO `moviedetailsactors` VALUES (7, 4);

INSERT INTO `vote` VALUES('1', '1', '1', 1);
INSERT INTO `vote` VALUES('2', '1', '2', 1);
INSERT INTO `vote` VALUES('3', '2', '1', 4);
INSERT INTO `vote` VALUES('4', '2', '2', 3);
INSERT INTO `vote` VALUES('5', '3', '1', 5);
INSERT INTO `vote` VALUES('6', '3', '2', 5);
INSERT INTO `vote` VALUES('7', '4', '1', 1);
INSERT INTO `vote` VALUES('8', '4', '2', 3);
INSERT INTO `vote` VALUES('9', '5', '1', 5);
INSERT INTO `vote` VALUES('10', '5', '2', 2);
INSERT INTO `vote` VALUES('11', '6', '1', 5);
INSERT INTO `vote` VALUES('12', '6', '2', 3);
INSERT INTO `vote` VALUES('13', '7', '3', 5);
INSERT INTO `vote` VALUES('14', '7', '2', 5);
INSERT INTO `vote` VALUES('15', '8', '1', 5);
INSERT INTO `vote` VALUES('16', '8', '2', 3);
INSERT INTO `vote` VALUES('17', '9', '1', 5);
INSERT INTO `vote` VALUES('18', '9', '3', 3);
INSERT INTO `vote` VALUES('19', '10', '1', 1);
INSERT INTO `vote` VALUES('20', '10', '2', 3);
INSERT INTO `vote` VALUES('21', '11', '1', 1);
INSERT INTO `vote` VALUES('22', '11', '3', 3);
INSERT INTO `vote` VALUES('23', '12', '1', 2);
INSERT INTO `vote` VALUES('24', '12', '2', 4);
INSERT INTO `vote` VALUES('25', '13', '1', 5);
INSERT INTO `vote` VALUES('26', '13', '3', 1);
INSERT INTO `vote` VALUES('27', '14', '1', 1);
INSERT INTO `vote` VALUES('28', '14', '2', 3);
INSERT INTO `vote` VALUES('29', '15', '1', 5);
INSERT INTO `vote` VALUES('30', '15', '2', 3);
INSERT INTO `vote` VALUES('31', '16', '1', 1);
INSERT INTO `vote` VALUES('32', '16', '2', 2);

INSERT INTO `comment` VALUES (1,1,1,'DEUS IO VULT! IN AETERNA! DEUS IO VULT!','2016-12-08');





