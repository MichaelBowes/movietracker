CREATE TABLE `director` (
	`directorID` int NOT NULL AUTO_INCREMENT,
	`firstName` varchar(100) NOT NULL,
	`lastName` varchar(100) NOT NULL, 
	PRIMARY KEY (`directorID`)
);

CREATE TABLE `actor` (
	`actorID` int NOT NULL AUTO_INCREMENT,
	`firstName` varchar(100) NOT NULL,
	`lastName` varchar(100) NOT NULL, 
	`birthdate` varchar(100), 
	`biography` varchar(8000), 
	PRIMARY KEY (`actorID`)
);

CREATE TABLE `movieactors` (
	`actorID`	INTEGER NOT NULL,
	`movieID`	INTEGER NOT NULL,
	PRIMARY KEY(actorID, movieID)
);

CREATE TABLE `moviedetailsactors` (
	`actorID`	INTEGER NOT NULL,
	`detailsID`	INTEGER NOT NULL,
	PRIMARY KEY(actorID, detailsID)
);

CREATE TABLE `category` (
  `categoryID` INTEGER NOT NULL AUTO_INCREMENT,
  `categoryname` TEXT NOT NULL,
  `isApproved` boolean NOT NULL,
	PRIMARY KEY (`categoryID`)
);

CREATE TABLE `movie` (
  `movieID` INTEGER NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `description` varchar(8000) NOT NULL,
  `duration` INTEGER NOT NULL,
  `ageLimitation` INTEGER NOT NULL,
  `releaseDate` varchar(20) NOT NULL,
  `directorID` INTEGER,
  `createdBy` varchar(200) NOT NULL,
  `status` varchar(25) NOT NULL,
  `rank` FLOAT,
  `numberOfVotes` INT,
	PRIMARY KEY (`movieID`), 
	FOREIGN KEY (`directorID`) REFERENCES director(directorID)
);


CREATE TABLE `moviecategories` (
  `movieID` INTEGER NOT NULL,
  `categoryID` INTEGER NOT NULL,
  PRIMARY KEY (`movieID`, `categoryID`),
  CONSTRAINT `FK_MOVIE` FOREIGN KEY (`movieID`) REFERENCES `movie` (`movieID`),
  CONSTRAINT `FK_CATEGORY` FOREIGN KEY (`categoryID`) REFERENCES `category` (`categoryID`)
);

CREATE TABLE `user` (
  `userID` INTEGER NOT NULL AUTO_INCREMENT,
  `userName` varchar(50) NOT NULL,
  `userEmail` varchar(200) NOT NULL,
  `userLastLogin` TIMESTAMP,
  `isAdmin` boolean NOT NULL,
  `password` varchar(200) NOT NULL,
	PRIMARY KEY (`userID`)
);

CREATE TABLE `movieUser` (
  `movieID` INTEGER NOT NULL,
  `userID` INTEGER NOT NULL,
  `seen` boolean NOT NULL,
  `dateSeen` TIMESTAMP,
  `watchlist` boolean NOT NULL,
  `dateWatchlist` TIMESTAMP,
	PRIMARY KEY (`movieID`,`userID`),
  FOREIGN KEY (`movieID`) REFERENCES movie(movieID),
	FOREIGN KEY (`userID`) REFERENCES user(userID)
);

CREATE TABLE `movieDetails` (
  `detailsID` INTEGER NOT NULL AUTO_INCREMENT,
  `movieID` INTEGER,
  `directorID` INTEGER,
  `title` varchar(200) NOT NULL,
  `description` varchar(8000) NOT NULL,
  `ageLimitation` INTEGER NOT NULL,
  `releaseDate` varchar(20) NOT NULL,
  `directorName` varchar(200) NOT NULL,
  `createdBy` varchar(200) NOT NULL,
  `status` varchar(25) NOT NULL,
  `duration` INTEGER NOT NULL,
	PRIMARY KEY (`detailsID`), 
	FOREIGN KEY (`movieID`) REFERENCES movie(movieID)
);

CREATE TABLE `moviedetailscategories` (
  `detailsID` INTEGER NOT NULL,
  `categoryID` INTEGER NOT NULL,
  PRIMARY KEY (`detailsID`, `categoryID`)
);

CREATE TABLE `comment` (
  `commentID` INTEGER NOT NULL AUTO_INCREMENT,
  `userID` INTEGER NOT NULL,
  `movieID` INTEGER NOT NULL,
  `text` varchar(1000) NOT NULL,
  `createdAt` TIMESTAMP NOT NULL,
	PRIMARY KEY (`commentID`),
	FOREIGN KEY (`movieID`) REFERENCES movie(movieID),
	FOREIGN KEY (`userID`) REFERENCES user(userID)
);

CREATE TABLE `moviecomments` (
  `movieID` INTEGER NOT NULL,
  `commentID` INTEGER NOT NULL,
  PRIMARY KEY (`movieID`, `commentID`)  
);

CREATE TABLE `vote` (
  `voteID` INTEGER NOT NULL AUTO_INCREMENT,
  `movieID` INTEGER NOT NULL,
  `userID` INTEGER NOT NULL,
  `rating` INTEGER NOT NULL,
	PRIMARY KEY (`voteID`)
);