package movietracker.test.service;
/**
 * @author Maximilian Kostial 
 */

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.configuration.ModelConfiguration;
import movietracker.configuration.ServiceConfiguration;
import movietracker.database.UserDAO;
import movietracker.model.User;
import movietracker.services.UserService;
@DirtiesContext  // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ServiceConfiguration.class, DatabaseConfiguration.class, ModelConfiguration.class})
@ActiveProfiles("development")
public class UserServiceTest {
	
	@Autowired
    UserService userService;
	@Autowired
	UserDAO userDAO;
	
	@Test
	public void testFindUser(){
		long id = 1;
		userService.findUserById(id);
	}
	@Test
	public void testAddUser(){
		userService.addUser(buildUser());
	}
	
	@Test
	public void testFindAllUser(){
		String email = "marcus.aurelius@antonius-augustus.com";
		
		userService.addUser(buildUser());
		List<User> userList= userService.findAllUsers();
		
		for (User user : userList)
			if (user.getUserID() == userList.size())
				Assert.assertEquals(email, user.getUserEmail());
	}
	
	@Test
	public void testUpdateUser(){
		String username = "Hans";
		List<User> userList = userService.findAllUsers();
		User user = userList.get(0);
		user.setUserName(username);
		
		userService.updateUser(user);
		
		Assert.assertEquals(user, userService.findAllUsers().get(0));
	}
	
	private User buildUser(){
		User user = new User();
		  user.setAdmin(true);
		  user.setPassword("invictusCaesar");
		  user.setUserEmail("marcus.aurelius@antonius-augustus.com");
		  user.setUserLastLogin(new Date());
		  user.setUserName("Mark Aurel");
		  return user;
	}
}
