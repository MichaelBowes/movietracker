package movietracker.test.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
/**
 * @author Maximilian Kostial
 */
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.configuration.ModelConfiguration;
import movietracker.configuration.ServiceConfiguration;
import movietracker.database.DirectorDAO;
import movietracker.model.Director;
import movietracker.services.DirectorService;

@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ServiceConfiguration.class, DatabaseConfiguration.class, ModelConfiguration.class})
@ActiveProfiles("development")
public class DirectorServiceTest {

    @Autowired
    private DirectorDAO directorDAO;
    
    @Autowired
    private DirectorService directorService;
    
	@Test
	public void testAutowired(){
		Assert.assertNotNull(directorDAO);
		Assert.assertNotNull(directorService);
	}
	@Test
	public void testFindDirector(){
		long id = 2;
		Assert.assertNotNull(directorService.findDirectorById(id));
	}
	@Test
	public void testAddDirector(){
		Director director = this.buildDirector();
		directorService.addDirector(director);
	}
	@Test
	public void testFindAllDirectors(){
		Director newDirector = this.buildDirector();
		directorService.addDirector(newDirector);
		String firstName = newDirector.getFirstName();
		
		List<Director> directorList = directorService.findAllDirectors();
		Map<String, Director> directorMap = new HashMap<String, Director>();
		
		for (Director director : directorList)
			directorMap.put(director.getFirstName(), director);
		
		Assert.assertTrue(directorMap.containsKey(firstName));
		Director director = directorMap.get(firstName);
		String firstNameFromDirectorInDB = director.getFirstName();
		
		Assert.assertEquals(firstNameFromDirectorInDB, firstName);
	}
	@Test
	public void testUpdateDirector(){		
		String newLastName = "Salty";
		directorService.addDirector(this.buildDirector());
	   
		List<Director> directorList = directorService.findAllDirectors();
		Director director = directorList.get(0);
		director.setLastName(newLastName);
		
		directorService.updateDirector(director);
		
		long id = director.getDirectorID();
		String lastNameOfDirectorInDB = directorService.findDirectorById(id).getLastName();
		Assert.assertEquals(newLastName, lastNameOfDirectorInDB);
	}
	
	private Director buildDirector(){
		return Director.newBuilder()
				.withFirstName("J.J.")
				.withLastName("Abrams")
				.build();
	}
}
