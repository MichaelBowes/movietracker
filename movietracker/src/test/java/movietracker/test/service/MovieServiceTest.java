package movietracker.test.service;
/**
 * @author Michael Bowes
 */
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.configuration.ModelConfiguration;
import movietracker.configuration.ServiceConfiguration;
import movietracker.database.MovieDAO;
import movietracker.model.Movie;
import movietracker.model.MovieDetails;
import movietracker.model.Status;
import movietracker.model.Vote;
import movietracker.services.MovieService;
import movietracker.services.VoteService;
@DirtiesContext  // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ServiceConfiguration.class, DatabaseConfiguration.class, ModelConfiguration.class})
@ActiveProfiles("development")
public class MovieServiceTest {

	@Autowired
    MovieService movieService;
	@Autowired
	MovieDAO movieDAO;
	@Autowired
	VoteService voteService;
	
	@Test
	public void testFindMovieById(){
		Movie movie = movieService.findMovieById(1);
		Assert.assertEquals("From Dusk Till Dawn", movie.getTitle());
	}
	
	@Test
	public void testAddMovie(){
		Movie movie = buildMovie();
		movieService.addMovie(movie);
		Movie foundMovie = movieService.findMovieById(movie.getMovieID());
		Assert.assertNotNull(foundMovie);
		Assert.assertEquals("Batman Begins", foundMovie.getTitle());
	}
	
	@Test
	public void testDeleteMovie(){
		long id = 1;
		movieService.deleteMovie(id);
	}
	
	@Test
	public void testFindAllMovies(){
		String title = "Batman Begins";
		
		movieService.addMovie(buildMovie());
		List<Movie> movieList= movieService.findAllMovies();
		
		for (Movie movie : movieList)
			if (movie.getMovieID() == 14)
				Assert.assertEquals(title, movie.getTitle());
	}

	@Test
	public void testUpdateMovie(){
		String moviename = "Batman Ends";
		List<Movie> movieList = movieService.findAllMovies();
		Movie movie = movieList.get(0);
		movie.setTitle(moviename);
		
		movieService.updateMovie(movie);
		
		Assert.assertEquals(moviename, movieService.findAllMovies().get(0).getTitle());
	}
	
	@Test
	public void testUpdateRanks(){
		movieService.updateRanks();
		List<Movie> movieList = movieService.findAllMovies();
		Assert.assertEquals(1.0, movieList.get(0).getRank(), 0.0001);
	}
	
	@Test
	public void testUpdateRanksWithNewVote(){
		movieService.updateRanks();
		List<Movie> movieList = movieService.findAllMovies();
		Movie movie = movieList.get(0);
		float rank = voteService.getCalculatedMovieRank(movie);
		Assert.assertEquals(1.0, rank, 0.0001);
		voteService.addVote(new Vote(4, 1, movie.getMovieID()));
		movieService.updateRanks();
		Assert.assertEquals(2.0, movie.getRank(), 0.0001);
	}
	
	@Test
	public void testGetAllDetailsByID(){
		List<MovieDetails> movieDetailsList = movieService.getAllDetailsByID(1);
		
		Assert.assertNotNull(movieDetailsList);
    	Assert.assertEquals(2, movieDetailsList.size());
    	Assert.assertEquals("From Dusk To Dusk", movieDetailsList.get(0).getTitle());
    	Assert.assertEquals("From Dusk To Tusk", movieDetailsList.get(1).getTitle());
	}
	
	private Movie buildMovie(){
		  Movie movie = new Movie();
		  movie.setDescription("Batman came and beat the bad guys. Happy End!");
		  movie.setTitle("Batman Begins");
		  movie.setDuration("60");
		  movie.setAgeLimitation(12);
		  movie.setReleaseDate("11.01.2016");
		  movie.setDirectorID(1);
		  movie.setCreatedBy("Cowboy Bobby");
		  movie.setStatus(Status.APPROVED);
		  movie.setRank(2);
		  movie.setNumberOfVotes(3);
		  
		  return movie;
	}
	
}
