package movietracker.test.service;
/**
 * @author Nicolas Bruch
 */

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.configuration.ModelConfiguration;
import movietracker.configuration.ServiceConfiguration;
import movietracker.database.CategoryDAO;
import movietracker.model.Category;
import movietracker.model.Movie;
import movietracker.services.CategoryService;
@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ServiceConfiguration.class, DatabaseConfiguration.class, ModelConfiguration.class})
@ActiveProfiles("development")
public class CategoryServiceTest {
	
	@Autowired
    CategoryService categoryService;
	@Autowired
	CategoryDAO categoryDAO;
	
	private Category addCategory() {
		Category category = new Category();
		category.setCategoryName("TEST");
    	return category;
	}
	
	@Test
	public void testFindCommentNullFound(){
		long id = 111;
		Category category = categoryService.findCategoryById(id);
		Assert.assertNull(category);
	}
	
	@Test
	public void testAddCategory(){
		Category category = addCategory();
		categoryService.addCategory(category);
		Category foundCategory = categoryService.findCategoryById(category.getId());
		Assert.assertNotNull(foundCategory);
		Assert.assertEquals("TEST", foundCategory.getCategoryName());
	}
	
	@Test
	public void testFindAllCategories(){
		List<Category> categories = categoryService.findAllCategories();
		Assert.assertNotNull(categories);
		int initialLength = categories.size();
		Category category1 = addCategory();
		Category category2 = addCategory();
		Category category3 = addCategory();
		categoryService.addCategory(category1);
		categoryService.addCategory(category2);
		categoryService.addCategory(category3);
		List<Category> foundCategory = categoryService.findAllCategories();
		Assert.assertNotNull(foundCategory);
		Assert.assertEquals(initialLength + 3, foundCategory.size());
	}
    
    @Test
    public void testFindAllMoviesByCategoryID(){
    	List<Movie> movies = categoryService.findAllMoviesByCategoryID(1);
    	
    	Assert.assertNotNull(movies);
    }
	
	@Test
	public void testDeleteCategory(){
		List<Category> categories = categoryService.findAllCategories();
		Assert.assertNotNull(categories);
		int initialLength = categories.size();		
		Category foundCategory = categoryService.findCategoryById(categories.get(0).getId());
		categoryService.deleteCategory(foundCategory.getId());
		List<Category> foundCategories = categoryService.findAllCategories();
		Assert.assertEquals(initialLength - 1, foundCategories.size());
	}
	
	@Test
	public void testUpdateCategory(){
		Category category = addCategory();
		category.setCategoryName("TEST1");
		categoryService.addCategory(category);
		category.setCategoryName("TEST2");
		categoryService.updateCategory(category);
		long id = category.getId();
		Category foundFoundCategory = categoryService.findCategoryById(id);
		Assert.assertEquals("TEST2", foundFoundCategory.getCategoryName());
	}
}
