package movietracker.test.service;

import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.configuration.ModelConfiguration;
import movietracker.configuration.ServiceConfiguration;
import movietracker.model.Comment;
import movietracker.services.CommentService;

/**
 * 
 * @author Stanislav Lokhtin
 *
 */
@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ServiceConfiguration.class, DatabaseConfiguration.class, ModelConfiguration.class})
@ActiveProfiles("development")
public class CommentServiceTest {
	
	@Autowired
	CommentService commentService;
	
	private Comment addComment() {
		Comment comment = new Comment();
    	comment.setText("Hello World");
    	comment.setUserID(1);
    	comment.setMovieID(1);
    	Calendar cal = Calendar.getInstance();
    	comment.setCreatedAt(cal);
    	return comment;
	}
	
	@Test
	public void testFindCommentNullFound(){
		long id = 111;
		Comment comment = commentService.findCommentById(id);
		Assert.assertNull(comment);
	}
	
	@Test
	public void testAddComment(){
		Comment comment = addComment();
		commentService.addComment(comment);
		Comment foundComment = commentService.findCommentById(comment.getId());
		Assert.assertNotNull(foundComment);
		Assert.assertEquals("Hello World", foundComment.getText());
	}
	
	@Test
	public void testFindAllComments(){
		List<Comment> comments = commentService.findAllComments();
		Assert.assertNotNull(comments);
		int initialLength = comments.size();
		Comment comment = addComment();
		Comment comment2 = addComment();
		Comment comment3 = addComment();
		commentService.addComment(comment);
		commentService.addComment(comment2);
		commentService.addComment(comment3);
		List<Comment> foundComment = commentService.findAllComments();
		Assert.assertNotNull(foundComment);
		Assert.assertEquals(initialLength + 3, foundComment.size());
	}
	
	@Test
	public void testDeleteComment(){
		Comment comment = addComment();
		Comment comment2 = addComment();
		commentService.addComment(comment);
		commentService.addComment(comment2);
		List<Comment> comments = commentService.findAllComments();
		Assert.assertNotNull(comments);
		int initialLength = comments.size();		
		Comment foundComment = commentService.findCommentById(comments.get(0).getId());
		commentService.deleteComment(foundComment.getId());
		List<Comment> foundComments = commentService.findAllComments();
		Assert.assertEquals(initialLength - 1, foundComments.size());
	}
	
	@Test
	public void testUpdateComment(){
		Comment comment = addComment();
		comment.setText("Text 1");
		commentService.addComment(comment);
		comment.setText("New Hello World");
		commentService.updateComment(comment);
		long id = comment.getId();
		Comment foundFoundComment = commentService.findCommentById(id);
		Assert.assertEquals("New Hello World", foundFoundComment.getText());
	}
}
