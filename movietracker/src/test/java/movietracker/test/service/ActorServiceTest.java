package movietracker.test.service;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
/**
 * @author Maximilian Kostial
 */
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.configuration.ModelConfiguration;
import movietracker.configuration.ServiceConfiguration;
import movietracker.database.ActorDAO;
import movietracker.model.Actor;
import movietracker.services.ActorService;

@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ServiceConfiguration.class, DatabaseConfiguration.class, ModelConfiguration.class })
@ActiveProfiles("development")
public class ActorServiceTest {
	
	@Autowired
	ActorDAO actorDAO;
	
	@Autowired
	ActorService actorService;
    
	@Test
	public void testAutowired(){
		Assert.assertNotNull(actorDAO);
		Assert.assertNotNull(actorService);
	}
	
	@Test
	public void testFindActor(){
		long id = 200;
		long id2 = 1;
		
		Assert.assertNull(actorService.findActorById(id));			
		Assert.assertNotNull(actorService.findActorById(id2));
	}
	
	@Test
	public void testAddActor(){
		actorService.addActor(this.buildActor());
		
		for (Actor actor : actorService.findAllActors()){	
			String firstNameOfActorInDB  = actor.getFirstName();
			String firstNameOfAddedActor = buildActor().getFirstName();
			
			  if (firstNameOfActorInDB  == firstNameOfAddedActor)
		           Assert.assertEquals(firstNameOfAddedActor, firstNameOfActorInDB);
		}
	}
	
	@Test
	public void testFindAllActors(){
		String lastName = "Der Zauberer";
		
		actorService.addActor(buildActor());
		List<Actor> actorList= actorService.findAllActors();
		Map<String,Actor> actorMap = new HashMap<String,Actor>();
		
		for (Actor actor : actorList)
			actorMap.put(actor.getLastName(), actor);
		
		Assert.assertTrue(actorMap.containsKey(lastName));
		
        Actor actor = actorMap.get(lastName);
        String lastNameFromActorInDB = actor.getLastName();
		Assert.assertEquals(lastNameFromActorInDB, lastName);
	}
	
	@Test
	public void testUpdateActor(){
		String firstName = "Hans";
		List<Actor> actorList = actorService.findAllActors();
		Actor actor = actorList.get(0);
		actor.setFirstName(firstName);
		
		actorService.updateActor(actor);
		
		long id = actor.getActorID();
		
		Assert.assertEquals(actor, actorService.findActorById(id));
	}
	
	
	private Actor buildActor(){
		return Actor.newBuilder()
				.withFirstName("Tim")
				.withLastName("Der Zauberer")
				.withBirthDate(new Date())
				.withBiography("Was ist deine Lieblingsfarbe?")
				.build();
	}

}
