package movietracker.test.service;
/**
 * @author Nicolas Bruch
 */

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.configuration.ModelConfiguration;
import movietracker.configuration.ServiceConfiguration;
import movietracker.database.VoteDAO;
import movietracker.model.Vote;
import movietracker.services.VoteService;
@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ServiceConfiguration.class, DatabaseConfiguration.class, ModelConfiguration.class})
@ActiveProfiles("development")
public class VoteServiceTest {
	
	@Autowired
    VoteService voteService;
	@Autowired
	VoteDAO voteDAO;
	
	private Vote addVote() {
		Vote vote = new Vote();
		vote.setRank(4);
		vote.setUserID(1);
		vote.setMovieID(1);
    	return vote;
	}
	
	@Test
	public void testFindCommentNullFound(){
		long id = 111;
		Vote vote = voteService.findVoteById(id);
		Assert.assertNull(vote);
	}
	
	@Test
	public void testAddComment(){
		Vote vote = addVote();
		voteService.addVote(vote);
		Vote foundVote = voteService.findVoteById(vote.getId());
		Assert.assertNotNull(foundVote);
		Assert.assertEquals(4, foundVote.getRank());
	}
	
	@Test
	public void testFindAllComments(){
		List<Vote> votes = voteService.findAllVotes();
		Assert.assertNotNull(votes);
		int initialLength = votes.size();
		Vote vote1 = addVote();
		Vote vote2 = addVote();
		Vote vote3 = addVote();
		voteService.addVote(vote1);
		voteService.addVote(vote2);
		voteService.addVote(vote3);
		List<Vote> foundVote = voteService.findAllVotes();
		Assert.assertNotNull(foundVote);
		Assert.assertEquals(initialLength + 3, foundVote.size());
	}
	
	@Test
	public void testDeleteComment(){
		Vote vote1 = addVote();
		Vote vote2 = addVote();
		voteService.addVote(vote1);
		voteService.addVote(vote2);
		List<Vote> votes = voteService.findAllVotes();
		Assert.assertNotNull(votes);
		int initialLength = votes.size();		
		Vote foundVote = voteService.findVoteById(votes.get(0).getId());
		voteService.deleteVote(foundVote.getId());
		List<Vote> foundComments = voteService.findAllVotes();
		Assert.assertEquals(initialLength - 1, foundComments.size());
	}
	
	@Test
	public void testUpdateComment(){
		Vote vote = addVote();
		vote.setRank(10);
		voteService.addVote(vote);
		vote.setRank(4);
		voteService.updateVote(vote);
		long id = vote.getId();
		Vote foundFoundVote = voteService.findVoteById(id);
		Assert.assertEquals(4, foundFoundVote.getRank());
	}
}
