package movietracker.test.database;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.database.CategoryDAO;
import movietracker.model.Category;
import movietracker.model.Movie;
/**
 * 
 * @author Nicolas Bruch
 *
 */

@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DatabaseConfiguration.class)
@ActiveProfiles("development")
public class CategoryDAOTest{

	
	@Autowired
	private CategoryDAO categoryDAO;
	
	private void addCategory(){
		Category category = new Category();
		category.setCategoryName("TEST");
		categoryDAO.insert(category);	
	}

	@Test
    public void testSelectIsNull(){
		Category category = categoryDAO.select(245);
    	Assert.assertNull(category);
    }
	
	@Test
	public void testSelect(){
		System.out.println("Start");
		addCategory();
		List<Category> categories = categoryDAO.selectAll();
	  	System.out.println("ID: " + categories.get(categories.size()-1).getId());
		Category category = categoryDAO.select(categories.get(categories.size()-1).getId());
		System.out.println("Firstok");
		Assert.assertNotNull(category);
		
		Assert.assertEquals("TEST", category.getCategoryName());
	}
	
	@Test
	public void testDeleteCategory(){
		List<Category> categories = categoryDAO.selectAll();
    	Assert.assertNotNull(categories);
    	int initialSize = categories.size();
    	categoryDAO.delete(categories.get(0).getId());
    	System.out.println("TEst2");
    	List<Category> newCategories = categoryDAO.selectAll();
    	System.out.println("TEst3");
    	Assert.assertEquals(initialSize - 1, newCategories.size());
	}
    
    @Test
    public void testCategoryDAOImplNotNull(){
    	Assert.assertNotNull(categoryDAO);
    }
    
    @Test
    public void testGetCategories() {
    	List<Category> categories = categoryDAO.selectAll();
    	Assert.assertNotNull(categories);
    }

    @Test 
    public void testInsertCategory(){
    	List<Category> categories = categoryDAO.selectAll();
    	Assert.assertNotNull(categories);
    	int initialSize = categories.size();
    	addCategory();
    	categories = categoryDAO.selectAll();
    	Assert.assertNotNull(categories);
    	Assert.assertEquals(initialSize + 1, categories.size());
    	Assert.assertEquals("TEST", categories.get(categories.size()-1).getCategoryName());
    }
    @Test
    public void testGetMoviesForCategory(){
    	List<Movie> movieList= categoryDAO.select(1).getMovies();
    	Movie movie = movieList.get(0);
    	Assert.assertNotNull(movie);
    }
    @Test
    public void testUpdateCategory(){
    	addCategory();
    	List<Category> categories = categoryDAO.selectAll(); 
    	Assert.assertNotNull(categories);
    	int initialSize = categories.size();
    	Category category = categories.get(categories.size() - 1);
    	category.setCategoryName("TEST2");
    	categoryDAO.update(categories.get(categories.size() - 1));
    	categories = categoryDAO.selectAll();
    	Assert.assertEquals(initialSize, categories.size());
    	Assert.assertEquals("TEST2", categories.get(categories.size() - 1).getCategoryName());

    }
	
}
