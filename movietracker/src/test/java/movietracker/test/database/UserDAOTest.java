package movietracker.test.database;
/**
 * @author Maximilian Kostial
 */
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.database.UserDAO;
import movietracker.model.User;
@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DatabaseConfiguration.class)
@ActiveProfiles("development")
public class UserDAOTest {

	
	@Autowired
	private UserDAO userDAO;

    @Test
    public void testSelect(){
    	User user = userDAO.selectUserById(1);   	
    	Assert.assertNotNull(user);
    	Assert.assertNotNull(user.getUserLastLogin());
    }
    
    @Test
    public void testSelectUserByName(){
    	User user = userDAO.selectUserByName("test");
    	Assert.assertNotNull(user);
    	Assert.assertEquals("test", user.getUsername());
    }
    
    @Test
    public void testUserDAOImplNotNull(){
    	Assert.assertNotNull(userDAO);
    }
    
    @Test
    public void testGetUsers() {
    	List<User> users = userDAO.selectAll();
  
    	Assert.assertNotNull(users);
    	Assert.assertNotEquals(0, users.size());
    	Assert.assertEquals(1 ,users.get(0).getUserID());
    }

    @Test 
    public void testInsertUser(){   	
	    User user = buildUser();
		userDAO.insert(user);
		for (User userDB : userDAO.selectAll()){
			
			long dbUserId = userDB.getUserID();
		    long userID = user.getUserID();
		    
			  if (dbUserId == userID)
       	         Assert.assertEquals(user.getUsername(), userDB.getUsername());
		}
    }
    
    @Test
    public void testDeleteUser(){
    	userDAO.delete(2);
    	Assert.assertNull(userDAO.selectUserById(2));
    }
    
    @Test
    public void testUpdateUser(){
    	long id = 2;
    	
    	User user = userDAO.selectUserById(id);
    	
    	String newPassword = "newPassword";
    	user.setPassword(newPassword);   
    	
    	userDAO.update(user);
    	
    	Assert.assertEquals(newPassword, userDAO.selectUserById(id).getPassword());
    	Assert.assertNotNull(userDAO.selectUserById(2).getUsername());
    }
    
    
    private User buildUser(){
    	User user = new User();
    	  user.setUserName("Henry Loiterie");
    	  user.setUserEmail("Henry@web.de");
    	  user.setUserLastLogin(new Date());
          user.setAdmin(false);
    	  user.setPassword("bongo");
    	
        return user;
    }
	
}
