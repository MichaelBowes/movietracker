package movietracker.test.database;
/**
 * @author Michael Bowes
 */


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import movietracker.model.Category;
import movietracker.model.Movie;
import movietracker.model.Status;
import movietracker.configuration.DatabaseConfiguration;
import movietracker.database.MovieDAO;
    @DirtiesContext // Resets the Configuration after every Test
    @Transactional
    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(classes=DatabaseConfiguration.class)
    @ActiveProfiles("development")
    public class MovieDAOTest {
    	
    	@Autowired
    	private MovieDAO movieDAO;

    	private void addMovie() {
    		Movie movie = new Movie();
  		  	movie.setDescription("Batman came and beat the bad guys. Happy End!");
  		  	movie.setTitle("Batman Begins");
  		  	movie.setDuration("60");
  		  	movie.setAgeLimitation(12);
  		  	movie.setReleaseDate("11.01.2016");
  		  	movie.setDirectorID(1);
  		  	movie.setCreatedBy("Cowboy Bobby");
  		  	movie.setStatus(Status.APPROVED);
  		  	movie.setRank(2);
  		  	movie.setNumberOfVotes(3);
        	movieDAO.insert(movie);
    	}
    	@Test
    	public void testStatus(){
    		long id = 1;
    		Movie movie = movieDAO.select(id);
    		Assert.assertEquals(Status.APPROVED, movie.getStatus());
    		
    		Movie pendingMovie = new Movie();
    		pendingMovie.setDescription("Batman came and beat the bad guys. Happy End!");
    		pendingMovie.setTitle("Batman Begins");
    		pendingMovie.setDuration("60");
    		pendingMovie.setAgeLimitation(12);
    		pendingMovie.setReleaseDate("11.01.2016");
    		pendingMovie.setDirectorID(1);
    		pendingMovie.setCreatedBy("Cowboy Bobby");
    		pendingMovie.setStatus(Status.PENDING);
    		pendingMovie.setRank(2);
    		pendingMovie.setNumberOfVotes(3);
        	movieDAO.insert(pendingMovie);
        	
        	Assert.assertEquals(Status.PENDING, movieDAO.select(pendingMovie.getMovieID()).getStatus());
    	}
    	
        @Test
        public void testSelectMovie(){
        	Movie movie = movieDAO.select(2);   	
        	Assert.assertNotNull(movie);
        }
        
        @Test
        public void testSelectMovieFoundNull(){
        	Movie movie = movieDAO.select(220);   	
        	Assert.assertNull(movie);
        }
        
        @Test
        public void testSelectAll(){
        	
        	List<Movie> movies = movieDAO.selectAll();
        	Movie movie = movies.get(1);
            List<Category> categoryList = movie.getCategories();
            Assert.assertNotNull(categoryList.get(0));
            
             
        	Assert.assertNotNull(movies);
        	Assert.assertEquals(16, movies.size());
        	
     	     Movie movie2 = movieDAO.select(1);
     	     Assert.assertNotNull(movie2.getComments().get(0));
        }
        

        @Test
        public void testSelectMovieCategories(){
            Movie movie = movieDAO.selectAll().get(0);
        	Assert.assertNotNull(movie);
        	Assert.assertNotNull(movie.getCategories());
        	Assert.assertEquals(3, movie.getCategories().size());
            movie = movieDAO.selectAll().get(1);
        	Assert.assertNotNull(movie);
        	Assert.assertNotNull(movie.getCategories());
        	Assert.assertEquals(3, movie.getCategories().size());
        	Assert.assertEquals("Adventure", movie.getCategories().get(0).getCategoryName());     	
        }
        
    	@Test
        public void testMovieDAOImplNotNull(){
        	Assert.assertNotNull(movieDAO);
        }
        
        @Test
        public void testDeleteMovie() {
        	List<Movie> movies = movieDAO.selectAll(); 
        	movies = movieDAO.selectAll(); 
        	Assert.assertNotNull(movies);
        	Assert.assertEquals(16, movies.size());
        	
        	movieDAO.delete(movies.get(0).getMovieID());
        	movies = movieDAO.selectAll(); 
        	Assert.assertEquals(15, movies.size());
        }
        
        @Test
        public void testDeleteAllMovies() {
        	List<Movie> movies = movieDAO.selectAll(); 
        	Assert.assertNotNull(movies);
        	Assert.assertEquals(16, movies.size());
        	
        	movieDAO.delete(movies.get(0).getMovieID());
        	movieDAO.delete(movies.get(1).getMovieID());
        	movieDAO.delete(movies.get(2).getMovieID());
        	movieDAO.delete(movies.get(3).getMovieID());
        	movieDAO.delete(movies.get(4).getMovieID());
        	movieDAO.delete(movies.get(5).getMovieID());
        	movieDAO.delete(movies.get(6).getMovieID());
        	movieDAO.delete(movies.get(7).getMovieID());
        	movieDAO.delete(movies.get(8).getMovieID());
        	movieDAO.delete(movies.get(9).getMovieID());
        	movieDAO.delete(movies.get(10).getMovieID());
        	movieDAO.delete(movies.get(11).getMovieID());
        	movieDAO.delete(movies.get(12).getMovieID());
        	movieDAO.delete(movies.get(13).getMovieID());
        	movieDAO.delete(movies.get(14).getMovieID());
        	movieDAO.delete(movies.get(15).getMovieID());
        	movies = movieDAO.selectAll();
        	Assert.assertEquals(0, movies.size());
        }
        
        //@Test
    	public void testFindAll(){}
        
        //@Test
        public void testSearchByTitle(){}

		@Test 
        public void testSelectAllMovies(){   	
        	addMovie();
        	List<Movie> movies = movieDAO.selectAll();
        	
        	Assert.assertNotNull(movies);
        	Assert.assertEquals(17, movies.size());
        	Assert.assertEquals("Batman came and beat the bad guys. Happy End!", movies.get(16).getDescription());
        	Assert.assertEquals("Batman Begins", movies.get(16).getTitle());
        	Assert.assertEquals("60", movies.get(16).getDuration());
        	Assert.assertEquals(12, movies.get(16).getAgeLimitation());
        	Assert.assertEquals("11.01.2016", movies.get(16).getReleaseDate());
        	Assert.assertEquals(1, movies.get(16).getDirectorID());
        	Assert.assertEquals("Cowboy Bobby", movies.get(16).getCreatedBy());
        	Assert.assertEquals(Status.APPROVED, movies.get(16).getStatus());
        	Assert.assertEquals(2, movies.get(16).getRank(), 0);
        	Assert.assertEquals(3, movies.get(16).getNumberOfVotes());
        }
        
		@Test
        public void testUpdateMovie() {
        	addMovie();
        	List<Movie> movies = movieDAO.selectAll(); 
        	Assert.assertNotNull(movies);
        	Assert.assertEquals(17, movies.size());
        	Movie movie = movies.get(16);
  		  	movie.setDescription("Nope!");
  		  	movie.setTitle("Batman Ends");
  		  	movie.setDuration("50");
  		  	movie.setAgeLimitation(18);
  		  	movie.setReleaseDate("12.02.2017");
  		  	movie.setDirectorID(1);
  		  	movie.setCreatedBy("Bob Dilan");
  		  	movie.setStatus(Status.APPROVED);
  		  	movie.setRank(2);
  		  	movie.setNumberOfVotes(3);
        	movieDAO.update(movie);
        	
        	movies = movieDAO.selectAll();
        	Assert.assertEquals(17, movies.size());
        	Assert.assertEquals("Nope!", movies.get(16).getDescription());
        	Assert.assertEquals("Batman Ends", movies.get(16).getTitle());
        	Assert.assertEquals("50", movies.get(16).getDuration());
        	Assert.assertEquals(18, movies.get(16).getAgeLimitation());
        	Assert.assertEquals("12.02.2017", movies.get(16).getReleaseDate());
        	Assert.assertEquals(1, movies.get(16).getDirectorID());
        	Assert.assertEquals("Bob Dilan", movies.get(16).getCreatedBy());
        	Assert.assertEquals(Status.APPROVED, movies.get(16).getStatus());
        	Assert.assertEquals(2, movies.get(16).getRank(), 0);
        	Assert.assertEquals(3, movies.get(16).getNumberOfVotes());
        }  
}