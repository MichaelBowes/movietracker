package movietracker.test.database;
/**
 * @author Michael Bowes
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import movietracker.model.MovieDetails;
import movietracker.model.Status;
import movietracker.configuration.DatabaseConfiguration;
import movietracker.database.MovieDetailsDAO;

    @DirtiesContext // Resets the Configuration after every Test
    @Transactional
    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(classes=DatabaseConfiguration.class)
    @ActiveProfiles("development")
    public class MovieDetailsDAOTest {
    	
    	@Autowired
    	private MovieDetailsDAO movieDetailsDAO;

    	private void addMovieDetails() {
    		MovieDetails movieDetails = new MovieDetails();
    		movieDetails.setDescription("Batman came and beat the bad guys. Happy End!");
    		movieDetails.setTitle("Batman Begins");
    		movieDetails.setAgeLimitation(12);
    		movieDetails.setReleaseDate("11.01.2016");
    		movieDetails.setDirectorName("None");
    		movieDetails.setDirectorName("hans");
    		movieDetails.setCreatedBy("Cowboy Bobby");
    		movieDetails.setStatus(Status.APPROVED);
    		movieDetails.setMovieID(1);
    		movieDetails.setDuration("120");
    		movieDetailsDAO.insert(movieDetails);
    	}
    	
        @Test
        public void testSelectMovieDetails(){
        	MovieDetails movieDetails = movieDetailsDAO.select(2);   	
        	Assert.assertNotNull(movieDetails);
        }
    
	    @Test
	    public void testSelectAllByMovieID(){
	    	List<MovieDetails> movieDetails = movieDetailsDAO.selectAllByMovieID(1);   	
	    	Assert.assertNotNull(movieDetails);
	    	Assert.assertEquals(2, movieDetails.size());
	    	Assert.assertEquals("From Dusk To Dusk", movieDetails.get(0).getTitle());
	    	Assert.assertEquals("From Dusk To Tusk", movieDetails.get(1).getTitle());
	    }
        
        @Test
        public void testSelectMovieDetailsFoundNull(){
        	MovieDetails movieDetails = movieDetailsDAO.select(220);   	
        	Assert.assertNull(movieDetails);
        }
        
        @Test
        public void testSelectAll(){
        	List<MovieDetails> movieDetails = movieDetailsDAO.selectAll();
            
        	Assert.assertNotNull(movieDetails);
        	Assert.assertEquals(4, movieDetails.size());
        }
        
    	@Test
        public void testMovieDetailsDAOImplNotNull(){
        	Assert.assertNotNull(movieDetailsDAO);
        }
        
        @Test
        public void testDeleteMovieDetails() {
        	List<MovieDetails> movieDetails = movieDetailsDAO.selectAll(); 
        	Assert.assertNotNull(movieDetails);
        	Assert.assertEquals(4, movieDetails.size());
        	
        	movieDetailsDAO.delete(movieDetails.get(0).getDetailsID());
        	List<MovieDetails> movieDetailsAgain = movieDetailsDAO.selectAll();
        	Assert.assertEquals(3, movieDetailsAgain.size());
        }
        
        @Test
        public void testDeleteAllMovieDetails() {
        	List<MovieDetails> movieDetails = movieDetailsDAO.selectAll(); 
        	Assert.assertNotNull(movieDetails);
        	Assert.assertEquals(4, movieDetails.size());
        	
        	movieDetailsDAO.delete(movieDetails.get(0).getDetailsID());
        	movieDetailsDAO.delete(movieDetails.get(1).getDetailsID());
        	movieDetailsDAO.delete(movieDetails.get(2).getDetailsID());
        	movieDetailsDAO.delete(movieDetails.get(3).getDetailsID());
        	List<MovieDetails> movieDetailsAgain = movieDetailsDAO.selectAll();
        	Assert.assertEquals(0, movieDetailsAgain.size());
        }

		@Test 
        public void testSelectAllMovieDetails(){   	
        	addMovieDetails();
        	List<MovieDetails> movieDetails = movieDetailsDAO.selectAll();
        	
        	Assert.assertNotNull(movieDetails);
        	Assert.assertEquals(5, movieDetails.size());
        	Assert.assertEquals("Batman came and beat the bad guys. Happy End!", movieDetails.get(4).getDescription());
        	Assert.assertEquals("Batman Begins", movieDetails.get(4).getTitle());
        	Assert.assertEquals(12, movieDetails.get(4).getAgeLimitation());
        	Assert.assertEquals("11.01.2016", movieDetails.get(4).getReleaseDate());
        	Assert.assertEquals("hans", movieDetails.get(4).getDirectorName());
        	Assert.assertEquals("Cowboy Bobby", movieDetails.get(4).getCreatedBy());
        	Assert.assertEquals(Status.APPROVED, movieDetails.get(4).getStatus());
        }
        
		@Test
        public void testUpdateMovieDetails() {
        	addMovieDetails();
        	List<MovieDetails> movieDetails = movieDetailsDAO.selectAll(); 
        	Assert.assertNotNull(movieDetails);
        	Assert.assertEquals(5, movieDetails.size());
        	MovieDetails movieDetailss = movieDetails.get(3);
        	movieDetailss.setDescription("Nope!");
        	movieDetailss.setTitle("Batman Ends");
        	movieDetailss.setAgeLimitation(18);
        	movieDetailss.setReleaseDate("12.02.2017");
        	movieDetailss.setDirectorName("None");
        	movieDetailss.setDirectorName("hans");
        	movieDetailss.setCreatedBy("Bob Dilan");
        	movieDetailss.setStatus(Status.APPROVED);
        	movieDetailsDAO.update(movieDetailss);
        	
        	movieDetails = movieDetailsDAO.selectAll();
        	Assert.assertEquals(5, movieDetails.size());
        	Assert.assertEquals("Nope!", movieDetails.get(3).getDescription());
        	Assert.assertEquals("Batman Ends", movieDetails.get(3).getTitle());
        	Assert.assertEquals(18, movieDetails.get(3).getAgeLimitation());
        	Assert.assertEquals("12.02.2017", movieDetails.get(3).getReleaseDate());
        	Assert.assertEquals("hans", movieDetails.get(3).getDirectorName());
        	Assert.assertEquals("Bob Dilan", movieDetails.get(3).getCreatedBy());
        	Assert.assertEquals(Status.APPROVED, movieDetails.get(3).getStatus());
        }  
}
