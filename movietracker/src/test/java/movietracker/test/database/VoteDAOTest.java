package movietracker.test.database;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import junit.framework.TestCase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.database.VoteDAO;
import movietracker.model.Vote;
/**
 * 
 * @author Nicolas Bruch
 *
 */
@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DatabaseConfiguration.class)
@ActiveProfiles("development")
public class VoteDAOTest extends TestCase {
	
	@Autowired
	private VoteDAO voteDAO;
	
	private void addVote() {
		Vote vote = new Vote();
    	vote.setMovieID(1);
    	vote.setRank(1);
    	vote.setUserID(1);
    	voteDAO.insert(vote);
	}
	
	 	@Test
	    public void testSelectFoundNull(){
	    	Vote vote = voteDAO.select(423);   	
	    	Assert.assertNull(vote);
	    }
	    
	    @Test
	    public void testVoteDAOImplNotNull(){
	    	Assert.assertNotNull(voteDAO);
	    }
	    
	    @Test
	    public void testGetVotes() {
	    	List<Vote> votes = voteDAO.selectAll();
	    	Assert.assertNotNull(votes);
	    	Assert.assertEquals(32, votes.size());
	    }
	    
	    
	    @Test
	    public void testSelectFoundNotNull(){
	    	addVote();
	    	List<Vote> votes = voteDAO.selectAll();
	    	Vote vote = voteDAO.select(votes.get(32).getId());   	
	    	Assert.assertNotNull(vote);
	    	Assert.assertEquals(1, vote.getRank());
	    }

	    @Test 
	    public void testInsertVote(){   	
	    	addVote();
	    	List<Vote> votes = voteDAO.selectAll();
	    	  
	    	Assert.assertNotNull(votes);
	    	Assert.assertEquals(33, votes.size());
	    	Assert.assertEquals(1, votes.get(32).getRank());
	    	Assert.assertEquals(1, votes.get(32).getUserID());
	    	Assert.assertEquals(1, votes.get(32).getMovieID());
	    }
	    
	    @Test
	    public void testDeleteVote() {
	    	addVote();
	    	List<Vote> votes = voteDAO.selectAll(); 
	    	Assert.assertNotNull(votes);
	    	Assert.assertEquals(33, votes.size());
	    	voteDAO.delete(votes.get(votes.size()-1).getId());
	    	List<Vote> newVote = voteDAO.selectAll();
	    	Assert.assertEquals(32, newVote.size());
	    }  
	    
	    @Test
	    public void testDeleteOneOfTheThreeVotes() {
	    	addVote();
	    	addVote();
	    	addVote();
	    	List<Vote> votes = voteDAO.selectAll(); 
	    	Assert.assertNotNull(votes);
	    	Assert.assertEquals(35, votes.size());
	    	voteDAO.delete(votes.get(33).getId());
	    	votes = voteDAO.selectAll();
	    	Assert.assertEquals(34, votes.size());
	    }  
	    
	    @Test
	    public void testUpdateVotes() {
	    	addVote();
	    	List<Vote> votes = voteDAO.selectAll(); 
	    	Assert.assertNotNull(votes);
	    	Assert.assertEquals(33, votes.size());
	    	Vote vote = votes.get(32);
	    	vote.setRank(3);
	    	vote.setMovieID(10);
	    	voteDAO.update(votes.get(0));
	    	votes = voteDAO.selectAll();
	    	Assert.assertEquals(33, votes.size());
	    	Assert.assertEquals(3, votes.get(32).getRank());
	    	Assert.assertEquals(10, votes.get(32).getMovieID());
	    	Assert.assertEquals(1, votes.get(32).getUserID());
	    }  
	
	

}
