package movietracker.test.database;

import junit.framework.TestCase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import movietracker.model.MovieUser;
import movietracker.configuration.DatabaseConfiguration;
import movietracker.database.MovieUserDAO;

/**
 * 
 * @author Stanislav Lokhtin
 *
 */
@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DatabaseConfiguration.class)
@ActiveProfiles("development")
public class MovieUserDAOTest extends TestCase {
	
	@Autowired
	private MovieUserDAO movieUserDAO;

	private void addMovieUser() {
		MovieUser movieUser = new MovieUser();
    	movieUser.setUserID(3);
    	movieUser.setMovieID(9);
    	movieUser.setSeen(true);
    	movieUser.setDateSeen(new Date());
    	movieUser.setWatchlist(false);
    	movieUser.setDateWatchlist(new Date());
    	movieUserDAO.insert(movieUser);
	}
	
    @Test
    public void testSelectFoundNull(){
    	MovieUser movieUser = movieUserDAO.select(222,111);   	
    	Assert.assertNull(movieUser);
    }
    
    @Test
    public void testMovieUserDAOImplNotNull(){
    	Assert.assertNotNull(movieUserDAO);
    }
    
    @Test
    public void testGetMovieUsers16Found() {
    	
    	List<MovieUser> movieUsers = movieUserDAO.selectAll();
  
    	Assert.assertNotNull(movieUsers);
    	Assert.assertEquals(16, movieUsers.size());
    }
    

    @Test
    public void testSelectFoundNotNull(){
    	MovieUser movieUser = movieUserDAO.select(1,1);   	
    	Assert.assertNotNull(movieUser);
    	Assert.assertEquals(true, movieUser.isSeen());
    }
    
    @Test 
    public void testInsertMovieUser(){   
    	List<MovieUser> movieUsers = movieUserDAO.selectAll();
    	int initialSize = movieUsers.size();
    	addMovieUser();
    	movieUsers = movieUserDAO.selectAll();
    	  
    	Assert.assertNotNull(movieUsers);
    	Assert.assertEquals(initialSize + 1, movieUsers.size());
    	Assert.assertEquals(true, movieUsers.get(0).isSeen());
    	Assert.assertEquals(1, movieUsers.get(0).getUserID());
    	Assert.assertEquals(1, movieUsers.get(0).getMovieID());
    }
    
    @Test
    public void testDeleteMovieUser() {
    	List<MovieUser> movieUsers = movieUserDAO.selectAll(); 
    	int initialSize = movieUsers.size();
    	Assert.assertNotNull(movieUsers);
    	movieUserDAO.delete(movieUsers.get(0).getUserID(), movieUsers.get(0).getMovieID());
    	movieUsers = movieUserDAO.selectAll();
    	Assert.assertEquals(initialSize - 1, movieUsers.size());
    }   
    
    @Test
    public void testUpdateMovieUser() {
    	addMovieUser();
    	List<MovieUser> movieUsers = movieUserDAO.selectAll(); 
    	Assert.assertNotNull(movieUsers);
    	int initialSize = movieUsers.size();
    	MovieUser movieUser = movieUsers.get(0);
    	movieUser.setWatchlist(true);
    	movieUser.setSeen(true);
    	movieUserDAO.update(movieUsers.get(0));
    	movieUsers = movieUserDAO.selectAll();
    	Assert.assertEquals(initialSize, movieUsers.size());
    	Assert.assertEquals(true, movieUsers.get(0).isWatchlist());
    	Assert.assertEquals(true, movieUsers.get(0).isSeen());
    	Assert.assertEquals(1, movieUsers.get(0).getUserID());
    }  
    
    @Test
    public void findMovieUserByUserId(){
    	List<MovieUser> movieUsers = movieUserDAO.selectByUserId(3);
    	  
    	Assert.assertNotNull(movieUsers);

    }
}
