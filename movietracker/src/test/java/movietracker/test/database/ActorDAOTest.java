package movietracker.test.database;
/**
 * @author Maximilian Kostial
 */
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.database.ActorDAO;
import movietracker.model.Actor;

@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DatabaseConfiguration.class)
@ActiveProfiles("development")
public class ActorDAOTest {
	
	@Autowired
	ActorDAO actorDAO;
	
    @Test
    public void testSelect(){
    	Actor actor = actorDAO.select(1);
    	Assert.assertNotNull(actor);
    }
	
    @Test
    public void testActorDAOImplNotNull(){
    	Assert.assertNotNull(actorDAO);
    }
    @Test
    public void testGetActors() {
    	List<Actor> list = actorDAO.selectAll();

    	Assert.assertNotNull(list);
    	Assert.assertNotEquals(0, list.size());
    	Assert.assertEquals(1 ,list.get(0).getActorID());
    }
    @Test 
    public void testInsertActor(){   	
	    Actor actor = this.buildActor();
		actorDAO.insert(actor);
		for (Actor actorDB : actorDAO.selectAll()){
			
			long dbActorId = actorDB.getActorID();
		    long actorID = actor.getActorID();
		    
			  if (dbActorId == actorID)
       	         Assert.assertEquals(actor.getFirstName(), actorDB.getFirstName());
		}
    }
    
    @Test
    public void testDeleteActor(){
    	actorDAO.delete(1);
    	Assert.assertNull(actorDAO.select(1));
    }
    
    @Test
    public void testUpdateActor(){
    	Actor actor = actorDAO.select(1);
    	
    	String lastName= "Vin Diesel";
    	actor.setLastName(lastName);  
    	
    	actorDAO.update(actor);
    	
    	Assert.assertEquals(lastName, actorDAO.select(1).getLastName());
    	Assert.assertNotNull(actorDAO.select(1).getLastName());
    }
    
	
	private Actor buildActor(){
		return Actor.newBuilder()
				.withFirstName("Hugh")
				.withLastName("Jackman")
				.withBirthDate(new Date())
				.withBiography("Test Biography: Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum")
				.build();
	}

}
