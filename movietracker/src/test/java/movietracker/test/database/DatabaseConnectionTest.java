package movietracker.test.database;
/**
 * @author Maximilian Kostial 
 */
import movietracker.configuration.*;
import movietracker.model.Actor;
import movietracker.model.Category;
import movietracker.model.Comment;
import movietracker.model.Director;
import movietracker.model.Movie;
import movietracker.model.User;
import movietracker.model.Vote;
import movietracker.model.MovieUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
 
@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DatabaseConfiguration.class)
@ActiveProfiles("development")
public class DatabaseConnectionTest {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Test
	public void testSessionFactory() {
		Assert.notNull(sessionFactory);
	}
	@Test
	public void testEntities(){
		
		//This Map contains all Model classes which are annotated with @Entity, their Class Name to String as Key
		Map<String,ClassMetadata> classMetaDataMap = sessionFactory.getAllClassMetadata();
		Assert.notEmpty(classMetaDataMap);
		
		//This List contains all Model classes which are annotated with @Entity as String
		List<String> entityList = new ArrayList<String>();
		entityList.add(Vote.class.getName());
		entityList.add(User.class.getName());
		entityList.add(Comment.class.getName());
		entityList.add(Director.class.getName());
		entityList.add(Category.class.getName());
		entityList.add(Actor.class.getName());
		entityList.add(Movie.class.getName());
		entityList.add(MovieUser.class.getName());

		for (String entity : entityList)
			Assert.isTrue(classMetaDataMap.containsKey(entity));
	}
	
}
