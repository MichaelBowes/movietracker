package movietracker.test.database;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
/**
 * @author Maximilian Kostial
 */
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import movietracker.configuration.DatabaseConfiguration;
import movietracker.database.DirectorDAO;
import movietracker.model.Director;

@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DatabaseConfiguration.class)
@ActiveProfiles("development")
public class DirectorDAOTest {
	
	@Autowired
	DirectorDAO directorDAO;
	
    @Test
    public void testSelect(){
    	Director director = directorDAO.select(1);
    	Assert.assertNotNull(director);
    }
    
    @Test
    public void testdirectorDAOImplNotNull(){
    	Assert.assertNotNull(directorDAO);
    }
    
    @Test
    public void testGetDirectors() {
    	List<Director> list = directorDAO.selectAll();

    	Assert.assertNotNull(list);
    	Assert.assertNotEquals(0, list.size());
    	Assert.assertEquals(1 ,list.get(0).getDirectorID());
    }

    @Test 
    public void testInsertDirector(){   	
	    Director director = this.buildDirector();
		directorDAO.insert(director);
		for (Director directorDB : directorDAO.selectAll()){
			
			long dbDirectorId = directorDB.getDirectorID();
		    long directorID = director.getDirectorID();
		    
			  if (dbDirectorId == directorID)
       	         Assert.assertEquals(director.getFirstName(), directorDB.getFirstName());
		}
    }
    
    @Test
    public void testDeletector(){
    	directorDAO.delete(1);
    	Assert.assertNull(directorDAO.select(1));
    }
    
    @Test
    public void testUpdateDirector(){
    	Director director = directorDAO.select(1);
    	
    	String lastName= "Spielberg";
    	director.setLastName(lastName);  
    	
    	directorDAO.update(director);
    	  	
    	Assert.assertEquals(lastName, directorDAO.select(1).getLastName());
    	Assert.assertNotNull(directorDAO.select(1).getLastName());
    }

    @Test
    public void testDeleteDirector(){
    	List<Director> list = directorDAO.selectAll();
    	int initialSize = list.size();
    	Director director = directorDAO.select(1);
    	
    	directorDAO.delete(director.getDirectorID());
    	list = directorDAO.selectAll();
    	Director directorAgain = directorDAO.select(director.getDirectorID());
    	Assert.assertNull(directorAgain);
    	Assert.assertEquals(initialSize - 1, list.size());
    }
	
	

	
	private Director buildDirector(){
		return Director.newBuilder()
				.withFirstName("Scott")
				.withLastName("Ridley")
				.build();
	}

}
