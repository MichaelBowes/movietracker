package movietracker.test.database;

import junit.framework.TestCase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import movietracker.model.Comment;
import movietracker.configuration.DatabaseConfiguration;
import movietracker.database.CommentDAO;

/**
 * 
 * @author Stanislav Lokhtin
 *
 */
@DirtiesContext // Resets the Configuration after every Test
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DatabaseConfiguration.class)
@ActiveProfiles("development")
public class CommentDAOTest extends TestCase {
	
	@Autowired
	private CommentDAO commentDAO;

	private void addComment() {
		Comment comment = new Comment();
    	comment.setText("Hello World");
    	comment.setUserID(1);
    	comment.setMovieID(1);
    	Calendar cal = Calendar.getInstance();
    	comment.setCreatedAt(cal);
    	commentDAO.insert(comment);
	}
	
    @Test
    public void testSelectFoundNull(){
    	Comment comment = commentDAO.select(222);   	
    	Assert.assertNull(comment);
    }
    
    @Test
    public void testCommentDAOImplNotNull(){
    	Assert.assertNotNull(commentDAO);
    }
    
    @Test
    public void testSelectFoundNotNull(){
    	addComment();
    	List<Comment> comments = commentDAO.selectAll();
    	Comment comment = commentDAO.select(comments.get(comments.size() - 1).getId());   	
    	Assert.assertNotNull(comment);
    	Assert.assertEquals("Hello World", comment.getText());
    }

    @Test 
    public void testInsertComment(){   	
    	List<Comment> comments = commentDAO.selectAll();
    	int initialSize = comments.size();
    	addComment();
    	comments = commentDAO.selectAll();
    	  
    	Assert.assertNotNull(comments);
    	Assert.assertEquals(initialSize + 1, comments.size());
    }
    
    @Test
    public void testDeleteComment() {
    	List<Comment> comments = commentDAO.selectAll();
    	int initialSize = comments.size();
    	addComment();
    	comments = commentDAO.selectAll(); 
    	Assert.assertNotNull(comments);
    	Assert.assertEquals(initialSize + 1, comments.size());
    	commentDAO.delete(comments.get(0).getId());
    	List<Comment> commentsAgain = commentDAO.selectAll();
    	Assert.assertEquals(initialSize, commentsAgain.size());
    }  
    
    @Test
    public void testDeleteOneOfTheThreeComments() {
    	List<Comment> comments = commentDAO.selectAll();
    	int initialSize = comments.size();
    	addComment();
    	addComment();
    	addComment();
    	comments = commentDAO.selectAll(); 
    	Assert.assertNotNull(comments);
    	Assert.assertEquals(initialSize + 3, comments.size());
    	commentDAO.delete(comments.get(0).getId());
    	comments = commentDAO.selectAll();
    	Assert.assertEquals(initialSize + 2, comments.size());
    }  
    
    @Test
    public void testUpdateComment() {
    	List<Comment> comments = commentDAO.selectAll();
    	int initialSize = comments.size();
    	addComment();
    	comments = commentDAO.selectAll(); 
    	Assert.assertNotNull(comments);
    	Assert.assertEquals(initialSize + 1, comments.size());
    	Comment comment = comments.get(comments.size() - 1);
    	comment.setText("New Hello World");
    	comment.setMovieID(10);
    	commentDAO.update(comment);
    	comments = commentDAO.selectAll();
    	Assert.assertEquals(initialSize + 1, comments.size());
    	Assert.assertEquals("New Hello World", comments.get(comments.size() - 1).getText());
    	Assert.assertEquals(10, comments.get(comments.size() - 1).getMovieID());
    	Assert.assertEquals(1, comments.get(comments.size() - 1).getUserID());
    }  

}
